'''
some util function to generate lua lex symbol

maybe can read from a antlr file and make this more general

'''

import random
import string

from luaDict import LUA_DICT

def randomPyString():
    allchar = string.ascii_letters + string.punctuation + string.digits
    return "".join(random.choice(allchar) for x in range(random.randint(4, 8)))

class TokenGenerator:
    def __init__(self):
        self.usedID = []

    def genBool(self):
        coin = random.randint(0, 1)
        if coin == 0:
            return 'true'
        else:
            return 'false'

    def genID(self):
        '''
        1/3 random string
        1/3 used string
        1/3 from dict
        '''
        roll = random.randint(0, 2)
        if len(self.usedID) == 0:
            if roll > 0 :
                return randomPyString()
            else:
                return random.choice(self.usedID)
        else:
            if roll == 0:
                return randomPyString()
            elif roll == 1:
                return random.choice(self.usedID)
            else:
                return random.choice(LUA_DICT['ID'].split('\n'))
    
    def genString(self):
        return random.choice(LUA_DICT['STRING'])

    