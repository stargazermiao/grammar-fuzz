// Generated from /home/stargazermiao/workspace/PL/afl++-grammar/lua-mutator/antlr/lua.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class luaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		RESERVED=53, NIL=54, BOOL=55, LONGSTRING=56, ESACPE_CHAR=57, BYTE=58, 
		NUMBERAL=59, ID=60, LETTER=61, DIGIT=62, HEX_DIGIT=63, NORMAL_STRING=64, 
		COMMENT=65, LINE_COMMENT=66, WS=67, SHEBANG=68;
	public static final int
		RULE_string = 0, RULE_binop = 1, RULE_unop = 2, RULE_chunk = 3, RULE_block = 4, 
		RULE_stat = 5, RULE_retstat = 6, RULE_label = 7, RULE_funcname = 8, RULE_varlist = 9, 
		RULE_var = 10, RULE_varsuffix = 11, RULE_namelist = 12, RULE_explist = 13, 
		RULE_exp = 14, RULE_functioncall = 15, RULE_functioncallsuffix = 16, RULE_args = 17, 
		RULE_functiondef = 18, RULE_funcbody = 19, RULE_parlist = 20, RULE_fieldsep = 21, 
		RULE_tableconstructor = 22, RULE_field = 23;
	private static String[] makeRuleNames() {
		return new String[] {
			"string", "binop", "unop", "chunk", "block", "stat", "retstat", "label", 
			"funcname", "varlist", "var", "varsuffix", "namelist", "explist", "exp", 
			"functioncall", "functioncallsuffix", "args", "functiondef", "funcbody", 
			"parlist", "fieldsep", "tableconstructor", "field"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'+'", "'-'", "'*'", "'/'", "'//'", "'^'", "'%'", "'&'", "'~'", 
			"'|'", "'>>'", "'<<'", "'..'", "'<'", "'<='", "'>'", "'>='", "'=='", 
			"'~='", "'and'", "'or'", "'not'", "'#'", "';'", "'='", "'break'", "'goto'", 
			"'do'", "'end'", "'while'", "'repeat'", "'until'", "'if'", "'then'", 
			"'elseif'", "'else'", "'for'", "','", "'in'", "'function'", "'local'", 
			"'return'", "'::'", "'.'", "':'", "'('", "')'", "'['", "']'", "'...'", 
			"'{'", "'}'", null, "'nil'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, "RESERVED", "NIL", "BOOL", "LONGSTRING", 
			"ESACPE_CHAR", "BYTE", "NUMBERAL", "ID", "LETTER", "DIGIT", "HEX_DIGIT", 
			"NORMAL_STRING", "COMMENT", "LINE_COMMENT", "WS", "SHEBANG"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "lua.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public luaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StringContext extends ParserRuleContext {
		public TerminalNode NORMAL_STRING() { return getToken(luaParser.NORMAL_STRING, 0); }
		public TerminalNode LONGSTRING() { return getToken(luaParser.LONGSTRING, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_string);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			_la = _input.LA(1);
			if ( !(_la==LONGSTRING || _la==NORMAL_STRING) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinopContext extends ParserRuleContext {
		public BinopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binop; }
	}

	public final BinopContext binop() throws RecognitionException {
		BinopContext _localctx = new BinopContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_binop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnopContext extends ParserRuleContext {
		public UnopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unop; }
	}

	public final UnopContext unop() throws RecognitionException {
		UnopContext _localctx = new UnopContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_unop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(52);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__8) | (1L << T__21) | (1L << T__22))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChunkContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode EOF() { return getToken(luaParser.EOF, 0); }
		public ChunkContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chunk; }
	}

	public final ChunkContext chunk() throws RecognitionException {
		ChunkContext _localctx = new ChunkContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_chunk);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			block();
			setState(55);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public RetstatContext retstat() {
			return getRuleContext(RetstatContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__23) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__29) | (1L << T__30) | (1L << T__32) | (1L << T__36) | (1L << T__39) | (1L << T__40) | (1L << T__42) | (1L << T__45) | (1L << ID))) != 0)) {
				{
				{
				setState(57);
				stat();
				}
				}
				setState(62);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(64);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__41) {
				{
				setState(63);
				retstat();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatContext extends ParserRuleContext {
		public VarlistContext varlist() {
			return getRuleContext(VarlistContext.class,0);
		}
		public ExplistContext explist() {
			return getRuleContext(ExplistContext.class,0);
		}
		public FunctioncallContext functioncall() {
			return getRuleContext(FunctioncallContext.class,0);
		}
		public LabelContext label() {
			return getRuleContext(LabelContext.class,0);
		}
		public TerminalNode ID() { return getToken(luaParser.ID, 0); }
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public NamelistContext namelist() {
			return getRuleContext(NamelistContext.class,0);
		}
		public FuncnameContext funcname() {
			return getRuleContext(FuncnameContext.class,0);
		}
		public FuncbodyContext funcbody() {
			return getRuleContext(FuncbodyContext.class,0);
		}
		public StatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stat; }
	}

	public final StatContext stat() throws RecognitionException {
		StatContext _localctx = new StatContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_stat);
		int _la;
		try {
			setState(147);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(66);
				match(T__23);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(67);
				varlist();
				setState(68);
				match(T__24);
				setState(69);
				explist();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(71);
				functioncall();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(72);
				label();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(73);
				match(T__25);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(74);
				match(T__26);
				setState(75);
				match(ID);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(76);
				match(T__27);
				setState(77);
				block();
				setState(78);
				match(T__28);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(80);
				match(T__29);
				setState(81);
				exp(0);
				setState(82);
				match(T__27);
				setState(83);
				block();
				setState(84);
				match(T__28);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(86);
				match(T__30);
				setState(87);
				block();
				setState(88);
				match(T__31);
				setState(89);
				exp(0);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(91);
				match(T__32);
				setState(92);
				exp(0);
				setState(93);
				match(T__33);
				setState(94);
				block();
				setState(102);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__34) {
					{
					{
					setState(95);
					match(T__34);
					setState(96);
					exp(0);
					setState(97);
					match(T__33);
					setState(98);
					block();
					}
					}
					setState(104);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(107);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__35) {
					{
					setState(105);
					match(T__35);
					setState(106);
					block();
					}
				}

				setState(109);
				match(T__28);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(111);
				match(T__36);
				setState(112);
				match(ID);
				setState(113);
				match(T__24);
				setState(114);
				exp(0);
				setState(115);
				match(T__37);
				setState(116);
				exp(0);
				setState(119);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__37) {
					{
					setState(117);
					match(T__37);
					setState(118);
					exp(0);
					}
				}

				setState(121);
				match(T__27);
				setState(122);
				block();
				setState(123);
				match(T__28);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(125);
				match(T__36);
				setState(126);
				namelist();
				setState(127);
				match(T__38);
				setState(128);
				explist();
				setState(129);
				match(T__27);
				setState(130);
				block();
				setState(131);
				match(T__28);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(133);
				match(T__39);
				setState(134);
				funcname();
				setState(135);
				funcbody();
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(137);
				match(T__40);
				setState(138);
				match(T__39);
				setState(139);
				match(ID);
				setState(140);
				funcbody();
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(141);
				match(T__40);
				setState(142);
				namelist();
				setState(145);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__24) {
					{
					setState(143);
					match(T__24);
					setState(144);
					explist();
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RetstatContext extends ParserRuleContext {
		public List<ExplistContext> explist() {
			return getRuleContexts(ExplistContext.class);
		}
		public ExplistContext explist(int i) {
			return getRuleContext(ExplistContext.class,i);
		}
		public RetstatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_retstat; }
	}

	public final RetstatContext retstat() throws RecognitionException {
		RetstatContext _localctx = new RetstatContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_retstat);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			match(T__41);
			setState(153);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 2)) & ~0x3f) == 0 && ((1L << (_la - 2)) & ((1L << (T__1 - 2)) | (1L << (T__8 - 2)) | (1L << (T__21 - 2)) | (1L << (T__22 - 2)) | (1L << (T__39 - 2)) | (1L << (T__45 - 2)) | (1L << (T__49 - 2)) | (1L << (T__50 - 2)) | (1L << (NIL - 2)) | (1L << (BOOL - 2)) | (1L << (LONGSTRING - 2)) | (1L << (NUMBERAL - 2)) | (1L << (ID - 2)) | (1L << (NORMAL_STRING - 2)))) != 0)) {
				{
				{
				setState(150);
				explist();
				}
				}
				setState(155);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(157);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__23) {
				{
				setState(156);
				match(T__23);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(luaParser.ID, 0); }
		public LabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_label; }
	}

	public final LabelContext label() throws RecognitionException {
		LabelContext _localctx = new LabelContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_label);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(159);
			match(T__42);
			setState(160);
			match(ID);
			setState(161);
			match(T__42);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncnameContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(luaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(luaParser.ID, i);
		}
		public FuncnameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcname; }
	}

	public final FuncnameContext funcname() throws RecognitionException {
		FuncnameContext _localctx = new FuncnameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_funcname);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(163);
			match(ID);
			setState(168);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__43) {
				{
				{
				setState(164);
				match(T__43);
				setState(165);
				match(ID);
				}
				}
				setState(170);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(173);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__44) {
				{
				setState(171);
				match(T__44);
				setState(172);
				match(ID);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarlistContext extends ParserRuleContext {
		public List<VarContext> var() {
			return getRuleContexts(VarContext.class);
		}
		public VarContext var(int i) {
			return getRuleContext(VarContext.class,i);
		}
		public VarlistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varlist; }
	}

	public final VarlistContext varlist() throws RecognitionException {
		VarlistContext _localctx = new VarlistContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_varlist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(175);
			var();
			setState(180);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__37) {
				{
				{
				setState(176);
				match(T__37);
				setState(177);
				var();
				}
				}
				setState(182);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(luaParser.ID, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public List<VarsuffixContext> varsuffix() {
			return getRuleContexts(VarsuffixContext.class);
		}
		public VarsuffixContext varsuffix(int i) {
			return getRuleContext(VarsuffixContext.class,i);
		}
		public List<FunctioncallsuffixContext> functioncallsuffix() {
			return getRuleContexts(FunctioncallsuffixContext.class);
		}
		public FunctioncallsuffixContext functioncallsuffix(int i) {
			return getRuleContext(FunctioncallsuffixContext.class,i);
		}
		public VarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var; }
	}

	public final VarContext var() throws RecognitionException {
		VarContext _localctx = new VarContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_var);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(189);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(183);
				match(ID);
				}
				break;
			case T__45:
				{
				setState(184);
				match(T__45);
				setState(185);
				exp(0);
				setState(186);
				match(T__46);
				setState(187);
				varsuffix();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(195);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(193);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case T__44:
					case T__45:
					case T__50:
					case LONGSTRING:
					case NORMAL_STRING:
						{
						setState(191);
						functioncallsuffix();
						}
						break;
					case T__43:
					case T__47:
						{
						setState(192);
						varsuffix();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(197);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			setState(201);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(198);
					varsuffix();
					}
					} 
				}
				setState(203);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarsuffixContext extends ParserRuleContext {
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public TerminalNode ID() { return getToken(luaParser.ID, 0); }
		public VarsuffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varsuffix; }
	}

	public final VarsuffixContext varsuffix() throws RecognitionException {
		VarsuffixContext _localctx = new VarsuffixContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_varsuffix);
		try {
			setState(210);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__47:
				enterOuterAlt(_localctx, 1);
				{
				setState(204);
				match(T__47);
				setState(205);
				exp(0);
				setState(206);
				match(T__48);
				}
				break;
			case T__43:
				enterOuterAlt(_localctx, 2);
				{
				setState(208);
				match(T__43);
				setState(209);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NamelistContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(luaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(luaParser.ID, i);
		}
		public NamelistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_namelist; }
	}

	public final NamelistContext namelist() throws RecognitionException {
		NamelistContext _localctx = new NamelistContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_namelist);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			match(ID);
			setState(217);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(213);
					match(T__37);
					setState(214);
					match(ID);
					}
					} 
				}
				setState(219);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExplistContext extends ParserRuleContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public ExplistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_explist; }
	}

	public final ExplistContext explist() throws RecognitionException {
		ExplistContext _localctx = new ExplistContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_explist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(220);
			exp(0);
			setState(225);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__37) {
				{
				{
				setState(221);
				match(T__37);
				setState(222);
				exp(0);
				}
				}
				setState(227);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public TerminalNode NIL() { return getToken(luaParser.NIL, 0); }
		public TerminalNode BOOL() { return getToken(luaParser.BOOL, 0); }
		public TerminalNode NUMBERAL() { return getToken(luaParser.NUMBERAL, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public FunctiondefContext functiondef() {
			return getRuleContext(FunctiondefContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public FunctioncallContext functioncall() {
			return getRuleContext(FunctioncallContext.class,0);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TableconstructorContext tableconstructor() {
			return getRuleContext(TableconstructorContext.class,0);
		}
		public UnopContext unop() {
			return getRuleContext(UnopContext.class,0);
		}
		public BinopContext binop() {
			return getRuleContext(BinopContext.class,0);
		}
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
	}

	public final ExpContext exp() throws RecognitionException {
		return exp(0);
	}

	private ExpContext exp(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpContext _localctx = new ExpContext(_ctx, _parentState);
		ExpContext _prevctx = _localctx;
		int _startState = 28;
		enterRecursionRule(_localctx, 28, RULE_exp, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(245);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(229);
				match(NIL);
				}
				break;
			case 2:
				{
				setState(230);
				match(BOOL);
				}
				break;
			case 3:
				{
				setState(231);
				match(NUMBERAL);
				}
				break;
			case 4:
				{
				setState(232);
				string();
				}
				break;
			case 5:
				{
				setState(233);
				match(T__49);
				}
				break;
			case 6:
				{
				setState(234);
				functiondef();
				}
				break;
			case 7:
				{
				setState(235);
				var();
				}
				break;
			case 8:
				{
				setState(236);
				functioncall();
				}
				break;
			case 9:
				{
				setState(237);
				match(T__45);
				setState(238);
				exp(0);
				setState(239);
				match(T__46);
				}
				break;
			case 10:
				{
				setState(241);
				tableconstructor();
				}
				break;
			case 11:
				{
				setState(242);
				unop();
				setState(243);
				exp(2);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(253);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ExpContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_exp);
					setState(247);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(248);
					binop();
					setState(249);
					exp(2);
					}
					} 
				}
				setState(255);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FunctioncallContext extends ParserRuleContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public List<FunctioncallsuffixContext> functioncallsuffix() {
			return getRuleContexts(FunctioncallsuffixContext.class);
		}
		public FunctioncallsuffixContext functioncallsuffix(int i) {
			return getRuleContext(FunctioncallsuffixContext.class,i);
		}
		public FunctioncallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functioncall; }
	}

	public final FunctioncallContext functioncall() throws RecognitionException {
		FunctioncallContext _localctx = new FunctioncallContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_functioncall);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(256);
				var();
				}
				break;
			case 2:
				{
				setState(257);
				match(T__45);
				setState(258);
				exp(0);
				setState(259);
				match(T__46);
				}
				break;
			}
			setState(264); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(263);
					functioncallsuffix();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(266); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctioncallsuffixContext extends ParserRuleContext {
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public TerminalNode ID() { return getToken(luaParser.ID, 0); }
		public FunctioncallsuffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functioncallsuffix; }
	}

	public final FunctioncallsuffixContext functioncallsuffix() throws RecognitionException {
		FunctioncallsuffixContext _localctx = new FunctioncallsuffixContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_functioncallsuffix);
		try {
			setState(272);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__45:
			case T__50:
			case LONGSTRING:
			case NORMAL_STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(268);
				args();
				}
				break;
			case T__44:
				enterOuterAlt(_localctx, 2);
				{
				setState(269);
				match(T__44);
				setState(270);
				match(ID);
				setState(271);
				args();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public ExplistContext explist() {
			return getRuleContext(ExplistContext.class,0);
		}
		public TableconstructorContext tableconstructor() {
			return getRuleContext(TableconstructorContext.class,0);
		}
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_args);
		int _la;
		try {
			setState(281);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__45:
				enterOuterAlt(_localctx, 1);
				{
				setState(274);
				match(T__45);
				setState(276);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 2)) & ~0x3f) == 0 && ((1L << (_la - 2)) & ((1L << (T__1 - 2)) | (1L << (T__8 - 2)) | (1L << (T__21 - 2)) | (1L << (T__22 - 2)) | (1L << (T__39 - 2)) | (1L << (T__45 - 2)) | (1L << (T__49 - 2)) | (1L << (T__50 - 2)) | (1L << (NIL - 2)) | (1L << (BOOL - 2)) | (1L << (LONGSTRING - 2)) | (1L << (NUMBERAL - 2)) | (1L << (ID - 2)) | (1L << (NORMAL_STRING - 2)))) != 0)) {
					{
					setState(275);
					explist();
					}
				}

				setState(278);
				match(T__46);
				}
				break;
			case T__50:
				enterOuterAlt(_localctx, 2);
				{
				setState(279);
				tableconstructor();
				}
				break;
			case LONGSTRING:
			case NORMAL_STRING:
				enterOuterAlt(_localctx, 3);
				{
				setState(280);
				string();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctiondefContext extends ParserRuleContext {
		public FuncbodyContext funcbody() {
			return getRuleContext(FuncbodyContext.class,0);
		}
		public FunctiondefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functiondef; }
	}

	public final FunctiondefContext functiondef() throws RecognitionException {
		FunctiondefContext _localctx = new FunctiondefContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_functiondef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(283);
			match(T__39);
			setState(284);
			funcbody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncbodyContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ParlistContext parlist() {
			return getRuleContext(ParlistContext.class,0);
		}
		public FuncbodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcbody; }
	}

	public final FuncbodyContext funcbody() throws RecognitionException {
		FuncbodyContext _localctx = new FuncbodyContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_funcbody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(286);
			match(T__45);
			setState(288);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__49 || _la==ID) {
				{
				setState(287);
				parlist();
				}
			}

			setState(290);
			match(T__46);
			setState(291);
			block();
			setState(292);
			match(T__28);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParlistContext extends ParserRuleContext {
		public NamelistContext namelist() {
			return getRuleContext(NamelistContext.class,0);
		}
		public ParlistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parlist; }
	}

	public final ParlistContext parlist() throws RecognitionException {
		ParlistContext _localctx = new ParlistContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_parlist);
		int _la;
		try {
			setState(300);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(294);
				namelist();
				setState(297);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__37) {
					{
					setState(295);
					match(T__37);
					setState(296);
					match(T__49);
					}
				}

				}
				break;
			case T__49:
				enterOuterAlt(_localctx, 2);
				{
				setState(299);
				match(T__49);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldsepContext extends ParserRuleContext {
		public FieldsepContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldsep; }
	}

	public final FieldsepContext fieldsep() throws RecognitionException {
		FieldsepContext _localctx = new FieldsepContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_fieldsep);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(302);
			_la = _input.LA(1);
			if ( !(_la==T__23 || _la==T__37) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableconstructorContext extends ParserRuleContext {
		public List<FieldContext> field() {
			return getRuleContexts(FieldContext.class);
		}
		public FieldContext field(int i) {
			return getRuleContext(FieldContext.class,i);
		}
		public List<FieldsepContext> fieldsep() {
			return getRuleContexts(FieldsepContext.class);
		}
		public FieldsepContext fieldsep(int i) {
			return getRuleContext(FieldsepContext.class,i);
		}
		public TableconstructorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableconstructor; }
	}

	public final TableconstructorContext tableconstructor() throws RecognitionException {
		TableconstructorContext _localctx = new TableconstructorContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_tableconstructor);
		int _la;
		try {
			int _alt;
			setState(321);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(304);
				match(T__50);
				setState(305);
				field();
				setState(311);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(306);
						fieldsep();
						setState(307);
						field();
						}
						} 
					}
					setState(313);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				}
				setState(315);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__23 || _la==T__37) {
					{
					setState(314);
					fieldsep();
					}
				}

				setState(317);
				match(T__51);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(319);
				match(T__50);
				setState(320);
				match(T__51);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldContext extends ParserRuleContext {
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public TerminalNode ID() { return getToken(luaParser.ID, 0); }
		public FieldContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_field; }
	}

	public final FieldContext field() throws RecognitionException {
		FieldContext _localctx = new FieldContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_field);
		try {
			setState(333);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(323);
				match(T__47);
				setState(324);
				exp(0);
				setState(325);
				match(T__48);
				setState(326);
				match(T__24);
				setState(327);
				exp(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(329);
				match(ID);
				setState(330);
				match(T__24);
				setState(331);
				exp(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(332);
				exp(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 14:
			return exp_sempred((ExpContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean exp_sempred(ExpContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3F\u0152\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\6\7\6=\n\6\f\6\16\6@\13\6\3\6\5"+
		"\6C\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\7\7g\n\7\f\7\16\7j\13\7\3\7\3\7\5\7n\n\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\5\7z\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0094\n\7\5\7"+
		"\u0096\n\7\3\b\3\b\7\b\u009a\n\b\f\b\16\b\u009d\13\b\3\b\5\b\u00a0\n\b"+
		"\3\t\3\t\3\t\3\t\3\n\3\n\3\n\7\n\u00a9\n\n\f\n\16\n\u00ac\13\n\3\n\3\n"+
		"\5\n\u00b0\n\n\3\13\3\13\3\13\7\13\u00b5\n\13\f\13\16\13\u00b8\13\13\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\5\f\u00c0\n\f\3\f\3\f\7\f\u00c4\n\f\f\f\16\f\u00c7"+
		"\13\f\3\f\7\f\u00ca\n\f\f\f\16\f\u00cd\13\f\3\r\3\r\3\r\3\r\3\r\3\r\5"+
		"\r\u00d5\n\r\3\16\3\16\3\16\7\16\u00da\n\16\f\16\16\16\u00dd\13\16\3\17"+
		"\3\17\3\17\7\17\u00e2\n\17\f\17\16\17\u00e5\13\17\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20"+
		"\u00f8\n\20\3\20\3\20\3\20\3\20\7\20\u00fe\n\20\f\20\16\20\u0101\13\20"+
		"\3\21\3\21\3\21\3\21\3\21\5\21\u0108\n\21\3\21\6\21\u010b\n\21\r\21\16"+
		"\21\u010c\3\22\3\22\3\22\3\22\5\22\u0113\n\22\3\23\3\23\5\23\u0117\n\23"+
		"\3\23\3\23\3\23\5\23\u011c\n\23\3\24\3\24\3\24\3\25\3\25\5\25\u0123\n"+
		"\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\5\26\u012c\n\26\3\26\5\26\u012f"+
		"\n\26\3\27\3\27\3\30\3\30\3\30\3\30\3\30\7\30\u0138\n\30\f\30\16\30\u013b"+
		"\13\30\3\30\5\30\u013e\n\30\3\30\3\30\3\30\3\30\5\30\u0144\n\30\3\31\3"+
		"\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\5\31\u0150\n\31\3\31\2\3\36"+
		"\32\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\2\6\4\2::BB\3\2"+
		"\3\27\5\2\4\4\13\13\30\31\4\2\32\32((\2\u0172\2\62\3\2\2\2\4\64\3\2\2"+
		"\2\6\66\3\2\2\2\b8\3\2\2\2\n>\3\2\2\2\f\u0095\3\2\2\2\16\u0097\3\2\2\2"+
		"\20\u00a1\3\2\2\2\22\u00a5\3\2\2\2\24\u00b1\3\2\2\2\26\u00bf\3\2\2\2\30"+
		"\u00d4\3\2\2\2\32\u00d6\3\2\2\2\34\u00de\3\2\2\2\36\u00f7\3\2\2\2 \u0107"+
		"\3\2\2\2\"\u0112\3\2\2\2$\u011b\3\2\2\2&\u011d\3\2\2\2(\u0120\3\2\2\2"+
		"*\u012e\3\2\2\2,\u0130\3\2\2\2.\u0143\3\2\2\2\60\u014f\3\2\2\2\62\63\t"+
		"\2\2\2\63\3\3\2\2\2\64\65\t\3\2\2\65\5\3\2\2\2\66\67\t\4\2\2\67\7\3\2"+
		"\2\289\5\n\6\29:\7\2\2\3:\t\3\2\2\2;=\5\f\7\2<;\3\2\2\2=@\3\2\2\2><\3"+
		"\2\2\2>?\3\2\2\2?B\3\2\2\2@>\3\2\2\2AC\5\16\b\2BA\3\2\2\2BC\3\2\2\2C\13"+
		"\3\2\2\2D\u0096\7\32\2\2EF\5\24\13\2FG\7\33\2\2GH\5\34\17\2H\u0096\3\2"+
		"\2\2I\u0096\5 \21\2J\u0096\5\20\t\2K\u0096\7\34\2\2LM\7\35\2\2M\u0096"+
		"\7>\2\2NO\7\36\2\2OP\5\n\6\2PQ\7\37\2\2Q\u0096\3\2\2\2RS\7 \2\2ST\5\36"+
		"\20\2TU\7\36\2\2UV\5\n\6\2VW\7\37\2\2W\u0096\3\2\2\2XY\7!\2\2YZ\5\n\6"+
		"\2Z[\7\"\2\2[\\\5\36\20\2\\\u0096\3\2\2\2]^\7#\2\2^_\5\36\20\2_`\7$\2"+
		"\2`h\5\n\6\2ab\7%\2\2bc\5\36\20\2cd\7$\2\2de\5\n\6\2eg\3\2\2\2fa\3\2\2"+
		"\2gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2im\3\2\2\2jh\3\2\2\2kl\7&\2\2ln\5\n\6"+
		"\2mk\3\2\2\2mn\3\2\2\2no\3\2\2\2op\7\37\2\2p\u0096\3\2\2\2qr\7\'\2\2r"+
		"s\7>\2\2st\7\33\2\2tu\5\36\20\2uv\7(\2\2vy\5\36\20\2wx\7(\2\2xz\5\36\20"+
		"\2yw\3\2\2\2yz\3\2\2\2z{\3\2\2\2{|\7\36\2\2|}\5\n\6\2}~\7\37\2\2~\u0096"+
		"\3\2\2\2\177\u0080\7\'\2\2\u0080\u0081\5\32\16\2\u0081\u0082\7)\2\2\u0082"+
		"\u0083\5\34\17\2\u0083\u0084\7\36\2\2\u0084\u0085\5\n\6\2\u0085\u0086"+
		"\7\37\2\2\u0086\u0096\3\2\2\2\u0087\u0088\7*\2\2\u0088\u0089\5\22\n\2"+
		"\u0089\u008a\5(\25\2\u008a\u0096\3\2\2\2\u008b\u008c\7+\2\2\u008c\u008d"+
		"\7*\2\2\u008d\u008e\7>\2\2\u008e\u0096\5(\25\2\u008f\u0090\7+\2\2\u0090"+
		"\u0093\5\32\16\2\u0091\u0092\7\33\2\2\u0092\u0094\5\34\17\2\u0093\u0091"+
		"\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0096\3\2\2\2\u0095D\3\2\2\2\u0095"+
		"E\3\2\2\2\u0095I\3\2\2\2\u0095J\3\2\2\2\u0095K\3\2\2\2\u0095L\3\2\2\2"+
		"\u0095N\3\2\2\2\u0095R\3\2\2\2\u0095X\3\2\2\2\u0095]\3\2\2\2\u0095q\3"+
		"\2\2\2\u0095\177\3\2\2\2\u0095\u0087\3\2\2\2\u0095\u008b\3\2\2\2\u0095"+
		"\u008f\3\2\2\2\u0096\r\3\2\2\2\u0097\u009b\7,\2\2\u0098\u009a\5\34\17"+
		"\2\u0099\u0098\3\2\2\2\u009a\u009d\3\2\2\2\u009b\u0099\3\2\2\2\u009b\u009c"+
		"\3\2\2\2\u009c\u009f\3\2\2\2\u009d\u009b\3\2\2\2\u009e\u00a0\7\32\2\2"+
		"\u009f\u009e\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\17\3\2\2\2\u00a1\u00a2"+
		"\7-\2\2\u00a2\u00a3\7>\2\2\u00a3\u00a4\7-\2\2\u00a4\21\3\2\2\2\u00a5\u00aa"+
		"\7>\2\2\u00a6\u00a7\7.\2\2\u00a7\u00a9\7>\2\2\u00a8\u00a6\3\2\2\2\u00a9"+
		"\u00ac\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00af\3\2"+
		"\2\2\u00ac\u00aa\3\2\2\2\u00ad\u00ae\7/\2\2\u00ae\u00b0\7>\2\2\u00af\u00ad"+
		"\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0\23\3\2\2\2\u00b1\u00b6\5\26\f\2\u00b2"+
		"\u00b3\7(\2\2\u00b3\u00b5\5\26\f\2\u00b4\u00b2\3\2\2\2\u00b5\u00b8\3\2"+
		"\2\2\u00b6\u00b4\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\25\3\2\2\2\u00b8\u00b6"+
		"\3\2\2\2\u00b9\u00c0\7>\2\2\u00ba\u00bb\7\60\2\2\u00bb\u00bc\5\36\20\2"+
		"\u00bc\u00bd\7\61\2\2\u00bd\u00be\5\30\r\2\u00be\u00c0\3\2\2\2\u00bf\u00b9"+
		"\3\2\2\2\u00bf\u00ba\3\2\2\2\u00c0\u00c5\3\2\2\2\u00c1\u00c4\5\"\22\2"+
		"\u00c2\u00c4\5\30\r\2\u00c3\u00c1\3\2\2\2\u00c3\u00c2\3\2\2\2\u00c4\u00c7"+
		"\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00cb\3\2\2\2\u00c7"+
		"\u00c5\3\2\2\2\u00c8\u00ca\5\30\r\2\u00c9\u00c8\3\2\2\2\u00ca\u00cd\3"+
		"\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\27\3\2\2\2\u00cd"+
		"\u00cb\3\2\2\2\u00ce\u00cf\7\62\2\2\u00cf\u00d0\5\36\20\2\u00d0\u00d1"+
		"\7\63\2\2\u00d1\u00d5\3\2\2\2\u00d2\u00d3\7.\2\2\u00d3\u00d5\7>\2\2\u00d4"+
		"\u00ce\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d5\31\3\2\2\2\u00d6\u00db\7>\2\2"+
		"\u00d7\u00d8\7(\2\2\u00d8\u00da\7>\2\2\u00d9\u00d7\3\2\2\2\u00da\u00dd"+
		"\3\2\2\2\u00db\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\33\3\2\2\2\u00dd"+
		"\u00db\3\2\2\2\u00de\u00e3\5\36\20\2\u00df\u00e0\7(\2\2\u00e0\u00e2\5"+
		"\36\20\2\u00e1\u00df\3\2\2\2\u00e2\u00e5\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e3"+
		"\u00e4\3\2\2\2\u00e4\35\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e6\u00e7\b\20\1"+
		"\2\u00e7\u00f8\78\2\2\u00e8\u00f8\79\2\2\u00e9\u00f8\7=\2\2\u00ea\u00f8"+
		"\5\2\2\2\u00eb\u00f8\7\64\2\2\u00ec\u00f8\5&\24\2\u00ed\u00f8\5\26\f\2"+
		"\u00ee\u00f8\5 \21\2\u00ef\u00f0\7\60\2\2\u00f0\u00f1\5\36\20\2\u00f1"+
		"\u00f2\7\61\2\2\u00f2\u00f8\3\2\2\2\u00f3\u00f8\5.\30\2\u00f4\u00f5\5"+
		"\6\4\2\u00f5\u00f6\5\36\20\4\u00f6\u00f8\3\2\2\2\u00f7\u00e6\3\2\2\2\u00f7"+
		"\u00e8\3\2\2\2\u00f7\u00e9\3\2\2\2\u00f7\u00ea\3\2\2\2\u00f7\u00eb\3\2"+
		"\2\2\u00f7\u00ec\3\2\2\2\u00f7\u00ed\3\2\2\2\u00f7\u00ee\3\2\2\2\u00f7"+
		"\u00ef\3\2\2\2\u00f7\u00f3\3\2\2\2\u00f7\u00f4\3\2\2\2\u00f8\u00ff\3\2"+
		"\2\2\u00f9\u00fa\f\3\2\2\u00fa\u00fb\5\4\3\2\u00fb\u00fc\5\36\20\4\u00fc"+
		"\u00fe\3\2\2\2\u00fd\u00f9\3\2\2\2\u00fe\u0101\3\2\2\2\u00ff\u00fd\3\2"+
		"\2\2\u00ff\u0100\3\2\2\2\u0100\37\3\2\2\2\u0101\u00ff\3\2\2\2\u0102\u0108"+
		"\5\26\f\2\u0103\u0104\7\60\2\2\u0104\u0105\5\36\20\2\u0105\u0106\7\61"+
		"\2\2\u0106\u0108\3\2\2\2\u0107\u0102\3\2\2\2\u0107\u0103\3\2\2\2\u0108"+
		"\u010a\3\2\2\2\u0109\u010b\5\"\22\2\u010a\u0109\3\2\2\2\u010b\u010c\3"+
		"\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d!\3\2\2\2\u010e\u0113"+
		"\5$\23\2\u010f\u0110\7/\2\2\u0110\u0111\7>\2\2\u0111\u0113\5$\23\2\u0112"+
		"\u010e\3\2\2\2\u0112\u010f\3\2\2\2\u0113#\3\2\2\2\u0114\u0116\7\60\2\2"+
		"\u0115\u0117\5\34\17\2\u0116\u0115\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0118"+
		"\3\2\2\2\u0118\u011c\7\61\2\2\u0119\u011c\5.\30\2\u011a\u011c\5\2\2\2"+
		"\u011b\u0114\3\2\2\2\u011b\u0119\3\2\2\2\u011b\u011a\3\2\2\2\u011c%\3"+
		"\2\2\2\u011d\u011e\7*\2\2\u011e\u011f\5(\25\2\u011f\'\3\2\2\2\u0120\u0122"+
		"\7\60\2\2\u0121\u0123\5*\26\2\u0122\u0121\3\2\2\2\u0122\u0123\3\2\2\2"+
		"\u0123\u0124\3\2\2\2\u0124\u0125\7\61\2\2\u0125\u0126\5\n\6\2\u0126\u0127"+
		"\7\37\2\2\u0127)\3\2\2\2\u0128\u012b\5\32\16\2\u0129\u012a\7(\2\2\u012a"+
		"\u012c\7\64\2\2\u012b\u0129\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012f\3"+
		"\2\2\2\u012d\u012f\7\64\2\2\u012e\u0128\3\2\2\2\u012e\u012d\3\2\2\2\u012f"+
		"+\3\2\2\2\u0130\u0131\t\5\2\2\u0131-\3\2\2\2\u0132\u0133\7\65\2\2\u0133"+
		"\u0139\5\60\31\2\u0134\u0135\5,\27\2\u0135\u0136\5\60\31\2\u0136\u0138"+
		"\3\2\2\2\u0137\u0134\3\2\2\2\u0138\u013b\3\2\2\2\u0139\u0137\3\2\2\2\u0139"+
		"\u013a\3\2\2\2\u013a\u013d\3\2\2\2\u013b\u0139\3\2\2\2\u013c\u013e\5,"+
		"\27\2\u013d\u013c\3\2\2\2\u013d\u013e\3\2\2\2\u013e\u013f\3\2\2\2\u013f"+
		"\u0140\7\66\2\2\u0140\u0144\3\2\2\2\u0141\u0142\7\65\2\2\u0142\u0144\7"+
		"\66\2\2\u0143\u0132\3\2\2\2\u0143\u0141\3\2\2\2\u0144/\3\2\2\2\u0145\u0146"+
		"\7\62\2\2\u0146\u0147\5\36\20\2\u0147\u0148\7\63\2\2\u0148\u0149\7\33"+
		"\2\2\u0149\u014a\5\36\20\2\u014a\u0150\3\2\2\2\u014b\u014c\7>\2\2\u014c"+
		"\u014d\7\33\2\2\u014d\u0150\5\36\20\2\u014e\u0150\5\36\20\2\u014f\u0145"+
		"\3\2\2\2\u014f\u014b\3\2\2\2\u014f\u014e\3\2\2\2\u0150\61\3\2\2\2#>Bh"+
		"my\u0093\u0095\u009b\u009f\u00aa\u00af\u00b6\u00bf\u00c3\u00c5\u00cb\u00d4"+
		"\u00db\u00e3\u00f7\u00ff\u0107\u010c\u0112\u0116\u011b\u0122\u012b\u012e"+
		"\u0139\u013d\u0143\u014f";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}