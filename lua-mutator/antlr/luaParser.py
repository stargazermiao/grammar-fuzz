# Generated from lua.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3F")
        buf.write("\u0152\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\6\7\6=\n\6")
        buf.write("\f\6\16\6@\13\6\3\6\5\6C\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\7\7g\n\7\f\7\16\7j\13\7\3\7\3\7\5\7n\n\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7z\n\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0094\n\7\5\7\u0096\n")
        buf.write("\7\3\b\3\b\7\b\u009a\n\b\f\b\16\b\u009d\13\b\3\b\5\b\u00a0")
        buf.write("\n\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\7\n\u00a9\n\n\f\n\16")
        buf.write("\n\u00ac\13\n\3\n\3\n\5\n\u00b0\n\n\3\13\3\13\3\13\7\13")
        buf.write("\u00b5\n\13\f\13\16\13\u00b8\13\13\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\f\5\f\u00c0\n\f\3\f\3\f\7\f\u00c4\n\f\f\f\16\f\u00c7")
        buf.write("\13\f\3\f\7\f\u00ca\n\f\f\f\16\f\u00cd\13\f\3\r\3\r\3")
        buf.write("\r\3\r\3\r\3\r\5\r\u00d5\n\r\3\16\3\16\3\16\7\16\u00da")
        buf.write("\n\16\f\16\16\16\u00dd\13\16\3\17\3\17\3\17\7\17\u00e2")
        buf.write("\n\17\f\17\16\17\u00e5\13\17\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\5\20\u00f8\n\20\3\20\3\20\3\20\3\20\7\20\u00fe\n")
        buf.write("\20\f\20\16\20\u0101\13\20\3\21\3\21\3\21\3\21\3\21\5")
        buf.write("\21\u0108\n\21\3\21\6\21\u010b\n\21\r\21\16\21\u010c\3")
        buf.write("\22\3\22\3\22\3\22\5\22\u0113\n\22\3\23\3\23\5\23\u0117")
        buf.write("\n\23\3\23\3\23\3\23\5\23\u011c\n\23\3\24\3\24\3\24\3")
        buf.write("\25\3\25\5\25\u0123\n\25\3\25\3\25\3\25\3\25\3\26\3\26")
        buf.write("\3\26\5\26\u012c\n\26\3\26\5\26\u012f\n\26\3\27\3\27\3")
        buf.write("\30\3\30\3\30\3\30\3\30\7\30\u0138\n\30\f\30\16\30\u013b")
        buf.write("\13\30\3\30\5\30\u013e\n\30\3\30\3\30\3\30\3\30\5\30\u0144")
        buf.write("\n\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\5\31\u0150\n\31\3\31\2\3\36\32\2\4\6\b\n\f\16\20\22\24")
        buf.write("\26\30\32\34\36 \"$&(*,.\60\2\6\4\2::BB\3\2\3\27\5\2\4")
        buf.write("\4\13\13\30\31\4\2\32\32((\2\u0172\2\62\3\2\2\2\4\64\3")
        buf.write("\2\2\2\6\66\3\2\2\2\b8\3\2\2\2\n>\3\2\2\2\f\u0095\3\2")
        buf.write("\2\2\16\u0097\3\2\2\2\20\u00a1\3\2\2\2\22\u00a5\3\2\2")
        buf.write("\2\24\u00b1\3\2\2\2\26\u00bf\3\2\2\2\30\u00d4\3\2\2\2")
        buf.write("\32\u00d6\3\2\2\2\34\u00de\3\2\2\2\36\u00f7\3\2\2\2 \u0107")
        buf.write("\3\2\2\2\"\u0112\3\2\2\2$\u011b\3\2\2\2&\u011d\3\2\2\2")
        buf.write("(\u0120\3\2\2\2*\u012e\3\2\2\2,\u0130\3\2\2\2.\u0143\3")
        buf.write("\2\2\2\60\u014f\3\2\2\2\62\63\t\2\2\2\63\3\3\2\2\2\64")
        buf.write("\65\t\3\2\2\65\5\3\2\2\2\66\67\t\4\2\2\67\7\3\2\2\289")
        buf.write("\5\n\6\29:\7\2\2\3:\t\3\2\2\2;=\5\f\7\2<;\3\2\2\2=@\3")
        buf.write("\2\2\2><\3\2\2\2>?\3\2\2\2?B\3\2\2\2@>\3\2\2\2AC\5\16")
        buf.write("\b\2BA\3\2\2\2BC\3\2\2\2C\13\3\2\2\2D\u0096\7\32\2\2E")
        buf.write("F\5\24\13\2FG\7\33\2\2GH\5\34\17\2H\u0096\3\2\2\2I\u0096")
        buf.write("\5 \21\2J\u0096\5\20\t\2K\u0096\7\34\2\2LM\7\35\2\2M\u0096")
        buf.write("\7>\2\2NO\7\36\2\2OP\5\n\6\2PQ\7\37\2\2Q\u0096\3\2\2\2")
        buf.write("RS\7 \2\2ST\5\36\20\2TU\7\36\2\2UV\5\n\6\2VW\7\37\2\2")
        buf.write("W\u0096\3\2\2\2XY\7!\2\2YZ\5\n\6\2Z[\7\"\2\2[\\\5\36\20")
        buf.write("\2\\\u0096\3\2\2\2]^\7#\2\2^_\5\36\20\2_`\7$\2\2`h\5\n")
        buf.write("\6\2ab\7%\2\2bc\5\36\20\2cd\7$\2\2de\5\n\6\2eg\3\2\2\2")
        buf.write("fa\3\2\2\2gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2im\3\2\2\2jh\3")
        buf.write("\2\2\2kl\7&\2\2ln\5\n\6\2mk\3\2\2\2mn\3\2\2\2no\3\2\2")
        buf.write("\2op\7\37\2\2p\u0096\3\2\2\2qr\7\'\2\2rs\7>\2\2st\7\33")
        buf.write("\2\2tu\5\36\20\2uv\7(\2\2vy\5\36\20\2wx\7(\2\2xz\5\36")
        buf.write("\20\2yw\3\2\2\2yz\3\2\2\2z{\3\2\2\2{|\7\36\2\2|}\5\n\6")
        buf.write("\2}~\7\37\2\2~\u0096\3\2\2\2\177\u0080\7\'\2\2\u0080\u0081")
        buf.write("\5\32\16\2\u0081\u0082\7)\2\2\u0082\u0083\5\34\17\2\u0083")
        buf.write("\u0084\7\36\2\2\u0084\u0085\5\n\6\2\u0085\u0086\7\37\2")
        buf.write("\2\u0086\u0096\3\2\2\2\u0087\u0088\7*\2\2\u0088\u0089")
        buf.write("\5\22\n\2\u0089\u008a\5(\25\2\u008a\u0096\3\2\2\2\u008b")
        buf.write("\u008c\7+\2\2\u008c\u008d\7*\2\2\u008d\u008e\7>\2\2\u008e")
        buf.write("\u0096\5(\25\2\u008f\u0090\7+\2\2\u0090\u0093\5\32\16")
        buf.write("\2\u0091\u0092\7\33\2\2\u0092\u0094\5\34\17\2\u0093\u0091")
        buf.write("\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0096\3\2\2\2\u0095")
        buf.write("D\3\2\2\2\u0095E\3\2\2\2\u0095I\3\2\2\2\u0095J\3\2\2\2")
        buf.write("\u0095K\3\2\2\2\u0095L\3\2\2\2\u0095N\3\2\2\2\u0095R\3")
        buf.write("\2\2\2\u0095X\3\2\2\2\u0095]\3\2\2\2\u0095q\3\2\2\2\u0095")
        buf.write("\177\3\2\2\2\u0095\u0087\3\2\2\2\u0095\u008b\3\2\2\2\u0095")
        buf.write("\u008f\3\2\2\2\u0096\r\3\2\2\2\u0097\u009b\7,\2\2\u0098")
        buf.write("\u009a\5\34\17\2\u0099\u0098\3\2\2\2\u009a\u009d\3\2\2")
        buf.write("\2\u009b\u0099\3\2\2\2\u009b\u009c\3\2\2\2\u009c\u009f")
        buf.write("\3\2\2\2\u009d\u009b\3\2\2\2\u009e\u00a0\7\32\2\2\u009f")
        buf.write("\u009e\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\17\3\2\2\2\u00a1")
        buf.write("\u00a2\7-\2\2\u00a2\u00a3\7>\2\2\u00a3\u00a4\7-\2\2\u00a4")
        buf.write("\21\3\2\2\2\u00a5\u00aa\7>\2\2\u00a6\u00a7\7.\2\2\u00a7")
        buf.write("\u00a9\7>\2\2\u00a8\u00a6\3\2\2\2\u00a9\u00ac\3\2\2\2")
        buf.write("\u00aa\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00af\3")
        buf.write("\2\2\2\u00ac\u00aa\3\2\2\2\u00ad\u00ae\7/\2\2\u00ae\u00b0")
        buf.write("\7>\2\2\u00af\u00ad\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0")
        buf.write("\23\3\2\2\2\u00b1\u00b6\5\26\f\2\u00b2\u00b3\7(\2\2\u00b3")
        buf.write("\u00b5\5\26\f\2\u00b4\u00b2\3\2\2\2\u00b5\u00b8\3\2\2")
        buf.write("\2\u00b6\u00b4\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\25\3")
        buf.write("\2\2\2\u00b8\u00b6\3\2\2\2\u00b9\u00c0\7>\2\2\u00ba\u00bb")
        buf.write("\7\60\2\2\u00bb\u00bc\5\36\20\2\u00bc\u00bd\7\61\2\2\u00bd")
        buf.write("\u00be\5\30\r\2\u00be\u00c0\3\2\2\2\u00bf\u00b9\3\2\2")
        buf.write("\2\u00bf\u00ba\3\2\2\2\u00c0\u00c5\3\2\2\2\u00c1\u00c4")
        buf.write("\5\"\22\2\u00c2\u00c4\5\30\r\2\u00c3\u00c1\3\2\2\2\u00c3")
        buf.write("\u00c2\3\2\2\2\u00c4\u00c7\3\2\2\2\u00c5\u00c3\3\2\2\2")
        buf.write("\u00c5\u00c6\3\2\2\2\u00c6\u00cb\3\2\2\2\u00c7\u00c5\3")
        buf.write("\2\2\2\u00c8\u00ca\5\30\r\2\u00c9\u00c8\3\2\2\2\u00ca")
        buf.write("\u00cd\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2")
        buf.write("\u00cc\27\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce\u00cf\7\62")
        buf.write("\2\2\u00cf\u00d0\5\36\20\2\u00d0\u00d1\7\63\2\2\u00d1")
        buf.write("\u00d5\3\2\2\2\u00d2\u00d3\7.\2\2\u00d3\u00d5\7>\2\2\u00d4")
        buf.write("\u00ce\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d5\31\3\2\2\2\u00d6")
        buf.write("\u00db\7>\2\2\u00d7\u00d8\7(\2\2\u00d8\u00da\7>\2\2\u00d9")
        buf.write("\u00d7\3\2\2\2\u00da\u00dd\3\2\2\2\u00db\u00d9\3\2\2\2")
        buf.write("\u00db\u00dc\3\2\2\2\u00dc\33\3\2\2\2\u00dd\u00db\3\2")
        buf.write("\2\2\u00de\u00e3\5\36\20\2\u00df\u00e0\7(\2\2\u00e0\u00e2")
        buf.write("\5\36\20\2\u00e1\u00df\3\2\2\2\u00e2\u00e5\3\2\2\2\u00e3")
        buf.write("\u00e1\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4\35\3\2\2\2\u00e5")
        buf.write("\u00e3\3\2\2\2\u00e6\u00e7\b\20\1\2\u00e7\u00f8\78\2\2")
        buf.write("\u00e8\u00f8\79\2\2\u00e9\u00f8\7=\2\2\u00ea\u00f8\5\2")
        buf.write("\2\2\u00eb\u00f8\7\64\2\2\u00ec\u00f8\5&\24\2\u00ed\u00f8")
        buf.write("\5\26\f\2\u00ee\u00f8\5 \21\2\u00ef\u00f0\7\60\2\2\u00f0")
        buf.write("\u00f1\5\36\20\2\u00f1\u00f2\7\61\2\2\u00f2\u00f8\3\2")
        buf.write("\2\2\u00f3\u00f8\5.\30\2\u00f4\u00f5\5\6\4\2\u00f5\u00f6")
        buf.write("\5\36\20\4\u00f6\u00f8\3\2\2\2\u00f7\u00e6\3\2\2\2\u00f7")
        buf.write("\u00e8\3\2\2\2\u00f7\u00e9\3\2\2\2\u00f7\u00ea\3\2\2\2")
        buf.write("\u00f7\u00eb\3\2\2\2\u00f7\u00ec\3\2\2\2\u00f7\u00ed\3")
        buf.write("\2\2\2\u00f7\u00ee\3\2\2\2\u00f7\u00ef\3\2\2\2\u00f7\u00f3")
        buf.write("\3\2\2\2\u00f7\u00f4\3\2\2\2\u00f8\u00ff\3\2\2\2\u00f9")
        buf.write("\u00fa\f\3\2\2\u00fa\u00fb\5\4\3\2\u00fb\u00fc\5\36\20")
        buf.write("\4\u00fc\u00fe\3\2\2\2\u00fd\u00f9\3\2\2\2\u00fe\u0101")
        buf.write("\3\2\2\2\u00ff\u00fd\3\2\2\2\u00ff\u0100\3\2\2\2\u0100")
        buf.write("\37\3\2\2\2\u0101\u00ff\3\2\2\2\u0102\u0108\5\26\f\2\u0103")
        buf.write("\u0104\7\60\2\2\u0104\u0105\5\36\20\2\u0105\u0106\7\61")
        buf.write("\2\2\u0106\u0108\3\2\2\2\u0107\u0102\3\2\2\2\u0107\u0103")
        buf.write("\3\2\2\2\u0108\u010a\3\2\2\2\u0109\u010b\5\"\22\2\u010a")
        buf.write("\u0109\3\2\2\2\u010b\u010c\3\2\2\2\u010c\u010a\3\2\2\2")
        buf.write("\u010c\u010d\3\2\2\2\u010d!\3\2\2\2\u010e\u0113\5$\23")
        buf.write("\2\u010f\u0110\7/\2\2\u0110\u0111\7>\2\2\u0111\u0113\5")
        buf.write("$\23\2\u0112\u010e\3\2\2\2\u0112\u010f\3\2\2\2\u0113#")
        buf.write("\3\2\2\2\u0114\u0116\7\60\2\2\u0115\u0117\5\34\17\2\u0116")
        buf.write("\u0115\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0118\3\2\2\2")
        buf.write("\u0118\u011c\7\61\2\2\u0119\u011c\5.\30\2\u011a\u011c")
        buf.write("\5\2\2\2\u011b\u0114\3\2\2\2\u011b\u0119\3\2\2\2\u011b")
        buf.write("\u011a\3\2\2\2\u011c%\3\2\2\2\u011d\u011e\7*\2\2\u011e")
        buf.write("\u011f\5(\25\2\u011f\'\3\2\2\2\u0120\u0122\7\60\2\2\u0121")
        buf.write("\u0123\5*\26\2\u0122\u0121\3\2\2\2\u0122\u0123\3\2\2\2")
        buf.write("\u0123\u0124\3\2\2\2\u0124\u0125\7\61\2\2\u0125\u0126")
        buf.write("\5\n\6\2\u0126\u0127\7\37\2\2\u0127)\3\2\2\2\u0128\u012b")
        buf.write("\5\32\16\2\u0129\u012a\7(\2\2\u012a\u012c\7\64\2\2\u012b")
        buf.write("\u0129\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012f\3\2\2\2")
        buf.write("\u012d\u012f\7\64\2\2\u012e\u0128\3\2\2\2\u012e\u012d")
        buf.write("\3\2\2\2\u012f+\3\2\2\2\u0130\u0131\t\5\2\2\u0131-\3\2")
        buf.write("\2\2\u0132\u0133\7\65\2\2\u0133\u0139\5\60\31\2\u0134")
        buf.write("\u0135\5,\27\2\u0135\u0136\5\60\31\2\u0136\u0138\3\2\2")
        buf.write("\2\u0137\u0134\3\2\2\2\u0138\u013b\3\2\2\2\u0139\u0137")
        buf.write("\3\2\2\2\u0139\u013a\3\2\2\2\u013a\u013d\3\2\2\2\u013b")
        buf.write("\u0139\3\2\2\2\u013c\u013e\5,\27\2\u013d\u013c\3\2\2\2")
        buf.write("\u013d\u013e\3\2\2\2\u013e\u013f\3\2\2\2\u013f\u0140\7")
        buf.write("\66\2\2\u0140\u0144\3\2\2\2\u0141\u0142\7\65\2\2\u0142")
        buf.write("\u0144\7\66\2\2\u0143\u0132\3\2\2\2\u0143\u0141\3\2\2")
        buf.write("\2\u0144/\3\2\2\2\u0145\u0146\7\62\2\2\u0146\u0147\5\36")
        buf.write("\20\2\u0147\u0148\7\63\2\2\u0148\u0149\7\33\2\2\u0149")
        buf.write("\u014a\5\36\20\2\u014a\u0150\3\2\2\2\u014b\u014c\7>\2")
        buf.write("\2\u014c\u014d\7\33\2\2\u014d\u0150\5\36\20\2\u014e\u0150")
        buf.write("\5\36\20\2\u014f\u0145\3\2\2\2\u014f\u014b\3\2\2\2\u014f")
        buf.write("\u014e\3\2\2\2\u0150\61\3\2\2\2#>Bhmy\u0093\u0095\u009b")
        buf.write("\u009f\u00aa\u00af\u00b6\u00bf\u00c3\u00c5\u00cb\u00d4")
        buf.write("\u00db\u00e3\u00f7\u00ff\u0107\u010c\u0112\u0116\u011b")
        buf.write("\u0122\u012b\u012e\u0139\u013d\u0143\u014f")
        return buf.getvalue()


class luaParser ( Parser ):

    grammarFileName = "lua.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'+'", "'-'", "'*'", "'/'", "'//'", "'^'", 
                     "'%'", "'&'", "'~'", "'|'", "'>>'", "'<<'", "'..'", 
                     "'<'", "'<='", "'>'", "'>='", "'=='", "'~='", "'and'", 
                     "'or'", "'not'", "'#'", "';'", "'='", "'break'", "'goto'", 
                     "'do'", "'end'", "'while'", "'repeat'", "'until'", 
                     "'if'", "'then'", "'elseif'", "'else'", "'for'", "','", 
                     "'in'", "'function'", "'local'", "'return'", "'::'", 
                     "'.'", "':'", "'('", "')'", "'['", "']'", "'...'", 
                     "'{'", "'}'", "<INVALID>", "'nil'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "RESERVED", "NIL", "BOOL", "LONGSTRING", 
                      "ESACPE_CHAR", "BYTE", "NUMBERAL", "ID", "LETTER", 
                      "DIGIT", "HEX_DIGIT", "NORMAL_STRING", "COMMENT", 
                      "LINE_COMMENT", "WS", "SHEBANG" ]

    RULE_string = 0
    RULE_binop = 1
    RULE_unop = 2
    RULE_chunk = 3
    RULE_block = 4
    RULE_stat = 5
    RULE_retstat = 6
    RULE_label = 7
    RULE_funcname = 8
    RULE_varlist = 9
    RULE_var = 10
    RULE_varsuffix = 11
    RULE_namelist = 12
    RULE_explist = 13
    RULE_exp = 14
    RULE_functioncall = 15
    RULE_functioncallsuffix = 16
    RULE_args = 17
    RULE_functiondef = 18
    RULE_funcbody = 19
    RULE_parlist = 20
    RULE_fieldsep = 21
    RULE_tableconstructor = 22
    RULE_field = 23

    ruleNames =  [ "string", "binop", "unop", "chunk", "block", "stat", 
                   "retstat", "label", "funcname", "varlist", "var", "varsuffix", 
                   "namelist", "explist", "exp", "functioncall", "functioncallsuffix", 
                   "args", "functiondef", "funcbody", "parlist", "fieldsep", 
                   "tableconstructor", "field" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    T__34=35
    T__35=36
    T__36=37
    T__37=38
    T__38=39
    T__39=40
    T__40=41
    T__41=42
    T__42=43
    T__43=44
    T__44=45
    T__45=46
    T__46=47
    T__47=48
    T__48=49
    T__49=50
    T__50=51
    T__51=52
    RESERVED=53
    NIL=54
    BOOL=55
    LONGSTRING=56
    ESACPE_CHAR=57
    BYTE=58
    NUMBERAL=59
    ID=60
    LETTER=61
    DIGIT=62
    HEX_DIGIT=63
    NORMAL_STRING=64
    COMMENT=65
    LINE_COMMENT=66
    WS=67
    SHEBANG=68

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class StringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NORMAL_STRING(self):
            return self.getToken(luaParser.NORMAL_STRING, 0)

        def LONGSTRING(self):
            return self.getToken(luaParser.LONGSTRING, 0)

        def getRuleIndex(self):
            return luaParser.RULE_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString" ):
                listener.enterString(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString" ):
                listener.exitString(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString" ):
                return visitor.visitString(self)
            else:
                return visitor.visitChildren(self)




    def string(self):

        localctx = luaParser.StringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_string)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            _la = self._input.LA(1)
            if not(_la==luaParser.LONGSTRING or _la==luaParser.NORMAL_STRING):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BinopContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return luaParser.RULE_binop

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinop" ):
                listener.enterBinop(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinop" ):
                listener.exitBinop(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBinop" ):
                return visitor.visitBinop(self)
            else:
                return visitor.visitChildren(self)




    def binop(self):

        localctx = luaParser.BinopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_binop)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 50
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << luaParser.T__0) | (1 << luaParser.T__1) | (1 << luaParser.T__2) | (1 << luaParser.T__3) | (1 << luaParser.T__4) | (1 << luaParser.T__5) | (1 << luaParser.T__6) | (1 << luaParser.T__7) | (1 << luaParser.T__8) | (1 << luaParser.T__9) | (1 << luaParser.T__10) | (1 << luaParser.T__11) | (1 << luaParser.T__12) | (1 << luaParser.T__13) | (1 << luaParser.T__14) | (1 << luaParser.T__15) | (1 << luaParser.T__16) | (1 << luaParser.T__17) | (1 << luaParser.T__18) | (1 << luaParser.T__19) | (1 << luaParser.T__20))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class UnopContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return luaParser.RULE_unop

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnop" ):
                listener.enterUnop(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnop" ):
                listener.exitUnop(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnop" ):
                return visitor.visitUnop(self)
            else:
                return visitor.visitChildren(self)




    def unop(self):

        localctx = luaParser.UnopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_unop)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << luaParser.T__1) | (1 << luaParser.T__8) | (1 << luaParser.T__21) | (1 << luaParser.T__22))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ChunkContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def block(self):
            return self.getTypedRuleContext(luaParser.BlockContext,0)


        def EOF(self):
            return self.getToken(luaParser.EOF, 0)

        def getRuleIndex(self):
            return luaParser.RULE_chunk

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterChunk" ):
                listener.enterChunk(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitChunk" ):
                listener.exitChunk(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitChunk" ):
                return visitor.visitChunk(self)
            else:
                return visitor.visitChildren(self)




    def chunk(self):

        localctx = luaParser.ChunkContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_chunk)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 54
            self.block()
            self.state = 55
            self.match(luaParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BlockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stat(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.StatContext)
            else:
                return self.getTypedRuleContext(luaParser.StatContext,i)


        def retstat(self):
            return self.getTypedRuleContext(luaParser.RetstatContext,0)


        def getRuleIndex(self):
            return luaParser.RULE_block

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlock" ):
                listener.enterBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlock" ):
                listener.exitBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock" ):
                return visitor.visitBlock(self)
            else:
                return visitor.visitChildren(self)




    def block(self):

        localctx = luaParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 60
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << luaParser.T__23) | (1 << luaParser.T__25) | (1 << luaParser.T__26) | (1 << luaParser.T__27) | (1 << luaParser.T__29) | (1 << luaParser.T__30) | (1 << luaParser.T__32) | (1 << luaParser.T__36) | (1 << luaParser.T__39) | (1 << luaParser.T__40) | (1 << luaParser.T__42) | (1 << luaParser.T__45) | (1 << luaParser.ID))) != 0):
                self.state = 57
                self.stat()
                self.state = 62
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 64
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==luaParser.T__41:
                self.state = 63
                self.retstat()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varlist(self):
            return self.getTypedRuleContext(luaParser.VarlistContext,0)


        def explist(self):
            return self.getTypedRuleContext(luaParser.ExplistContext,0)


        def functioncall(self):
            return self.getTypedRuleContext(luaParser.FunctioncallContext,0)


        def label(self):
            return self.getTypedRuleContext(luaParser.LabelContext,0)


        def ID(self):
            return self.getToken(luaParser.ID, 0)

        def block(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.BlockContext)
            else:
                return self.getTypedRuleContext(luaParser.BlockContext,i)


        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.ExpContext)
            else:
                return self.getTypedRuleContext(luaParser.ExpContext,i)


        def namelist(self):
            return self.getTypedRuleContext(luaParser.NamelistContext,0)


        def funcname(self):
            return self.getTypedRuleContext(luaParser.FuncnameContext,0)


        def funcbody(self):
            return self.getTypedRuleContext(luaParser.FuncbodyContext,0)


        def getRuleIndex(self):
            return luaParser.RULE_stat

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStat" ):
                listener.enterStat(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStat" ):
                listener.exitStat(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStat" ):
                return visitor.visitStat(self)
            else:
                return visitor.visitChildren(self)




    def stat(self):

        localctx = luaParser.StatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_stat)
        self._la = 0 # Token type
        try:
            self.state = 147
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 66
                self.match(luaParser.T__23)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 67
                self.varlist()
                self.state = 68
                self.match(luaParser.T__24)
                self.state = 69
                self.explist()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 71
                self.functioncall()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 72
                self.label()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 73
                self.match(luaParser.T__25)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 74
                self.match(luaParser.T__26)
                self.state = 75
                self.match(luaParser.ID)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 76
                self.match(luaParser.T__27)
                self.state = 77
                self.block()
                self.state = 78
                self.match(luaParser.T__28)
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 80
                self.match(luaParser.T__29)
                self.state = 81
                self.exp(0)
                self.state = 82
                self.match(luaParser.T__27)
                self.state = 83
                self.block()
                self.state = 84
                self.match(luaParser.T__28)
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 86
                self.match(luaParser.T__30)
                self.state = 87
                self.block()
                self.state = 88
                self.match(luaParser.T__31)
                self.state = 89
                self.exp(0)
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 91
                self.match(luaParser.T__32)
                self.state = 92
                self.exp(0)
                self.state = 93
                self.match(luaParser.T__33)
                self.state = 94
                self.block()
                self.state = 102
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==luaParser.T__34:
                    self.state = 95
                    self.match(luaParser.T__34)
                    self.state = 96
                    self.exp(0)
                    self.state = 97
                    self.match(luaParser.T__33)
                    self.state = 98
                    self.block()
                    self.state = 104
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 107
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==luaParser.T__35:
                    self.state = 105
                    self.match(luaParser.T__35)
                    self.state = 106
                    self.block()


                self.state = 109
                self.match(luaParser.T__28)
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 111
                self.match(luaParser.T__36)
                self.state = 112
                self.match(luaParser.ID)
                self.state = 113
                self.match(luaParser.T__24)
                self.state = 114
                self.exp(0)
                self.state = 115
                self.match(luaParser.T__37)
                self.state = 116
                self.exp(0)
                self.state = 119
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==luaParser.T__37:
                    self.state = 117
                    self.match(luaParser.T__37)
                    self.state = 118
                    self.exp(0)


                self.state = 121
                self.match(luaParser.T__27)
                self.state = 122
                self.block()
                self.state = 123
                self.match(luaParser.T__28)
                pass

            elif la_ == 12:
                self.enterOuterAlt(localctx, 12)
                self.state = 125
                self.match(luaParser.T__36)
                self.state = 126
                self.namelist()
                self.state = 127
                self.match(luaParser.T__38)
                self.state = 128
                self.explist()
                self.state = 129
                self.match(luaParser.T__27)
                self.state = 130
                self.block()
                self.state = 131
                self.match(luaParser.T__28)
                pass

            elif la_ == 13:
                self.enterOuterAlt(localctx, 13)
                self.state = 133
                self.match(luaParser.T__39)
                self.state = 134
                self.funcname()
                self.state = 135
                self.funcbody()
                pass

            elif la_ == 14:
                self.enterOuterAlt(localctx, 14)
                self.state = 137
                self.match(luaParser.T__40)
                self.state = 138
                self.match(luaParser.T__39)
                self.state = 139
                self.match(luaParser.ID)
                self.state = 140
                self.funcbody()
                pass

            elif la_ == 15:
                self.enterOuterAlt(localctx, 15)
                self.state = 141
                self.match(luaParser.T__40)
                self.state = 142
                self.namelist()
                self.state = 145
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==luaParser.T__24:
                    self.state = 143
                    self.match(luaParser.T__24)
                    self.state = 144
                    self.explist()


                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RetstatContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def explist(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.ExplistContext)
            else:
                return self.getTypedRuleContext(luaParser.ExplistContext,i)


        def getRuleIndex(self):
            return luaParser.RULE_retstat

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRetstat" ):
                listener.enterRetstat(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRetstat" ):
                listener.exitRetstat(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRetstat" ):
                return visitor.visitRetstat(self)
            else:
                return visitor.visitChildren(self)




    def retstat(self):

        localctx = luaParser.RetstatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_retstat)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 149
            self.match(luaParser.T__41)
            self.state = 153
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while ((((_la - 2)) & ~0x3f) == 0 and ((1 << (_la - 2)) & ((1 << (luaParser.T__1 - 2)) | (1 << (luaParser.T__8 - 2)) | (1 << (luaParser.T__21 - 2)) | (1 << (luaParser.T__22 - 2)) | (1 << (luaParser.T__39 - 2)) | (1 << (luaParser.T__45 - 2)) | (1 << (luaParser.T__49 - 2)) | (1 << (luaParser.T__50 - 2)) | (1 << (luaParser.NIL - 2)) | (1 << (luaParser.BOOL - 2)) | (1 << (luaParser.LONGSTRING - 2)) | (1 << (luaParser.NUMBERAL - 2)) | (1 << (luaParser.ID - 2)) | (1 << (luaParser.NORMAL_STRING - 2)))) != 0):
                self.state = 150
                self.explist()
                self.state = 155
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 157
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==luaParser.T__23:
                self.state = 156
                self.match(luaParser.T__23)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LabelContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(luaParser.ID, 0)

        def getRuleIndex(self):
            return luaParser.RULE_label

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLabel" ):
                listener.enterLabel(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLabel" ):
                listener.exitLabel(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLabel" ):
                return visitor.visitLabel(self)
            else:
                return visitor.visitChildren(self)




    def label(self):

        localctx = luaParser.LabelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_label)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 159
            self.match(luaParser.T__42)
            self.state = 160
            self.match(luaParser.ID)
            self.state = 161
            self.match(luaParser.T__42)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncnameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(luaParser.ID)
            else:
                return self.getToken(luaParser.ID, i)

        def getRuleIndex(self):
            return luaParser.RULE_funcname

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFuncname" ):
                listener.enterFuncname(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFuncname" ):
                listener.exitFuncname(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncname" ):
                return visitor.visitFuncname(self)
            else:
                return visitor.visitChildren(self)




    def funcname(self):

        localctx = luaParser.FuncnameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_funcname)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 163
            self.match(luaParser.ID)
            self.state = 168
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==luaParser.T__43:
                self.state = 164
                self.match(luaParser.T__43)
                self.state = 165
                self.match(luaParser.ID)
                self.state = 170
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 173
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==luaParser.T__44:
                self.state = 171
                self.match(luaParser.T__44)
                self.state = 172
                self.match(luaParser.ID)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.VarContext)
            else:
                return self.getTypedRuleContext(luaParser.VarContext,i)


        def getRuleIndex(self):
            return luaParser.RULE_varlist

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarlist" ):
                listener.enterVarlist(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarlist" ):
                listener.exitVarlist(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarlist" ):
                return visitor.visitVarlist(self)
            else:
                return visitor.visitChildren(self)




    def varlist(self):

        localctx = luaParser.VarlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_varlist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 175
            self.var()
            self.state = 180
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==luaParser.T__37:
                self.state = 176
                self.match(luaParser.T__37)
                self.state = 177
                self.var()
                self.state = 182
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(luaParser.ID, 0)

        def exp(self):
            return self.getTypedRuleContext(luaParser.ExpContext,0)


        def varsuffix(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.VarsuffixContext)
            else:
                return self.getTypedRuleContext(luaParser.VarsuffixContext,i)


        def functioncallsuffix(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.FunctioncallsuffixContext)
            else:
                return self.getTypedRuleContext(luaParser.FunctioncallsuffixContext,i)


        def getRuleIndex(self):
            return luaParser.RULE_var

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar" ):
                listener.enterVar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar" ):
                listener.exitVar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar" ):
                return visitor.visitVar(self)
            else:
                return visitor.visitChildren(self)




    def var(self):

        localctx = luaParser.VarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_var)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 189
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [luaParser.ID]:
                self.state = 183
                self.match(luaParser.ID)
                pass
            elif token in [luaParser.T__45]:
                self.state = 184
                self.match(luaParser.T__45)
                self.state = 185
                self.exp(0)
                self.state = 186
                self.match(luaParser.T__46)
                self.state = 187
                self.varsuffix()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 195
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,14,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 193
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [luaParser.T__44, luaParser.T__45, luaParser.T__50, luaParser.LONGSTRING, luaParser.NORMAL_STRING]:
                        self.state = 191
                        self.functioncallsuffix()
                        pass
                    elif token in [luaParser.T__43, luaParser.T__47]:
                        self.state = 192
                        self.varsuffix()
                        pass
                    else:
                        raise NoViableAltException(self)
             
                self.state = 197
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,14,self._ctx)

            self.state = 201
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,15,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 198
                    self.varsuffix() 
                self.state = 203
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,15,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarsuffixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self):
            return self.getTypedRuleContext(luaParser.ExpContext,0)


        def ID(self):
            return self.getToken(luaParser.ID, 0)

        def getRuleIndex(self):
            return luaParser.RULE_varsuffix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarsuffix" ):
                listener.enterVarsuffix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarsuffix" ):
                listener.exitVarsuffix(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarsuffix" ):
                return visitor.visitVarsuffix(self)
            else:
                return visitor.visitChildren(self)




    def varsuffix(self):

        localctx = luaParser.VarsuffixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_varsuffix)
        try:
            self.state = 210
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [luaParser.T__47]:
                self.enterOuterAlt(localctx, 1)
                self.state = 204
                self.match(luaParser.T__47)
                self.state = 205
                self.exp(0)
                self.state = 206
                self.match(luaParser.T__48)
                pass
            elif token in [luaParser.T__43]:
                self.enterOuterAlt(localctx, 2)
                self.state = 208
                self.match(luaParser.T__43)
                self.state = 209
                self.match(luaParser.ID)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class NamelistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(luaParser.ID)
            else:
                return self.getToken(luaParser.ID, i)

        def getRuleIndex(self):
            return luaParser.RULE_namelist

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNamelist" ):
                listener.enterNamelist(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNamelist" ):
                listener.exitNamelist(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNamelist" ):
                return visitor.visitNamelist(self)
            else:
                return visitor.visitChildren(self)




    def namelist(self):

        localctx = luaParser.NamelistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_namelist)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 212
            self.match(luaParser.ID)
            self.state = 217
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,17,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 213
                    self.match(luaParser.T__37)
                    self.state = 214
                    self.match(luaParser.ID) 
                self.state = 219
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,17,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExplistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.ExpContext)
            else:
                return self.getTypedRuleContext(luaParser.ExpContext,i)


        def getRuleIndex(self):
            return luaParser.RULE_explist

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExplist" ):
                listener.enterExplist(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExplist" ):
                listener.exitExplist(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExplist" ):
                return visitor.visitExplist(self)
            else:
                return visitor.visitChildren(self)




    def explist(self):

        localctx = luaParser.ExplistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_explist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 220
            self.exp(0)
            self.state = 225
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==luaParser.T__37:
                self.state = 221
                self.match(luaParser.T__37)
                self.state = 222
                self.exp(0)
                self.state = 227
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NIL(self):
            return self.getToken(luaParser.NIL, 0)

        def BOOL(self):
            return self.getToken(luaParser.BOOL, 0)

        def NUMBERAL(self):
            return self.getToken(luaParser.NUMBERAL, 0)

        def string(self):
            return self.getTypedRuleContext(luaParser.StringContext,0)


        def functiondef(self):
            return self.getTypedRuleContext(luaParser.FunctiondefContext,0)


        def var(self):
            return self.getTypedRuleContext(luaParser.VarContext,0)


        def functioncall(self):
            return self.getTypedRuleContext(luaParser.FunctioncallContext,0)


        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.ExpContext)
            else:
                return self.getTypedRuleContext(luaParser.ExpContext,i)


        def tableconstructor(self):
            return self.getTypedRuleContext(luaParser.TableconstructorContext,0)


        def unop(self):
            return self.getTypedRuleContext(luaParser.UnopContext,0)


        def binop(self):
            return self.getTypedRuleContext(luaParser.BinopContext,0)


        def getRuleIndex(self):
            return luaParser.RULE_exp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExp" ):
                listener.enterExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExp" ):
                listener.exitExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp" ):
                return visitor.visitExp(self)
            else:
                return visitor.visitChildren(self)



    def exp(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = luaParser.ExpContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 28
        self.enterRecursionRule(localctx, 28, self.RULE_exp, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 245
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.state = 229
                self.match(luaParser.NIL)
                pass

            elif la_ == 2:
                self.state = 230
                self.match(luaParser.BOOL)
                pass

            elif la_ == 3:
                self.state = 231
                self.match(luaParser.NUMBERAL)
                pass

            elif la_ == 4:
                self.state = 232
                self.string()
                pass

            elif la_ == 5:
                self.state = 233
                self.match(luaParser.T__49)
                pass

            elif la_ == 6:
                self.state = 234
                self.functiondef()
                pass

            elif la_ == 7:
                self.state = 235
                self.var()
                pass

            elif la_ == 8:
                self.state = 236
                self.functioncall()
                pass

            elif la_ == 9:
                self.state = 237
                self.match(luaParser.T__45)
                self.state = 238
                self.exp(0)
                self.state = 239
                self.match(luaParser.T__46)
                pass

            elif la_ == 10:
                self.state = 241
                self.tableconstructor()
                pass

            elif la_ == 11:
                self.state = 242
                self.unop()
                self.state = 243
                self.exp(2)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 253
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,20,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = luaParser.ExpContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp)
                    self.state = 247
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 248
                    self.binop()
                    self.state = 249
                    self.exp(2) 
                self.state = 255
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,20,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class FunctioncallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var(self):
            return self.getTypedRuleContext(luaParser.VarContext,0)


        def exp(self):
            return self.getTypedRuleContext(luaParser.ExpContext,0)


        def functioncallsuffix(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.FunctioncallsuffixContext)
            else:
                return self.getTypedRuleContext(luaParser.FunctioncallsuffixContext,i)


        def getRuleIndex(self):
            return luaParser.RULE_functioncall

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctioncall" ):
                listener.enterFunctioncall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctioncall" ):
                listener.exitFunctioncall(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctioncall" ):
                return visitor.visitFunctioncall(self)
            else:
                return visitor.visitChildren(self)




    def functioncall(self):

        localctx = luaParser.FunctioncallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_functioncall)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 261
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
            if la_ == 1:
                self.state = 256
                self.var()
                pass

            elif la_ == 2:
                self.state = 257
                self.match(luaParser.T__45)
                self.state = 258
                self.exp(0)
                self.state = 259
                self.match(luaParser.T__46)
                pass


            self.state = 264 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 263
                    self.functioncallsuffix()

                else:
                    raise NoViableAltException(self)
                self.state = 266 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,22,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctioncallsuffixContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def args(self):
            return self.getTypedRuleContext(luaParser.ArgsContext,0)


        def ID(self):
            return self.getToken(luaParser.ID, 0)

        def getRuleIndex(self):
            return luaParser.RULE_functioncallsuffix

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctioncallsuffix" ):
                listener.enterFunctioncallsuffix(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctioncallsuffix" ):
                listener.exitFunctioncallsuffix(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctioncallsuffix" ):
                return visitor.visitFunctioncallsuffix(self)
            else:
                return visitor.visitChildren(self)




    def functioncallsuffix(self):

        localctx = luaParser.FunctioncallsuffixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_functioncallsuffix)
        try:
            self.state = 272
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [luaParser.T__45, luaParser.T__50, luaParser.LONGSTRING, luaParser.NORMAL_STRING]:
                self.enterOuterAlt(localctx, 1)
                self.state = 268
                self.args()
                pass
            elif token in [luaParser.T__44]:
                self.enterOuterAlt(localctx, 2)
                self.state = 269
                self.match(luaParser.T__44)
                self.state = 270
                self.match(luaParser.ID)
                self.state = 271
                self.args()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ArgsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def explist(self):
            return self.getTypedRuleContext(luaParser.ExplistContext,0)


        def tableconstructor(self):
            return self.getTypedRuleContext(luaParser.TableconstructorContext,0)


        def string(self):
            return self.getTypedRuleContext(luaParser.StringContext,0)


        def getRuleIndex(self):
            return luaParser.RULE_args

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgs" ):
                listener.enterArgs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgs" ):
                listener.exitArgs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgs" ):
                return visitor.visitArgs(self)
            else:
                return visitor.visitChildren(self)




    def args(self):

        localctx = luaParser.ArgsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_args)
        self._la = 0 # Token type
        try:
            self.state = 281
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [luaParser.T__45]:
                self.enterOuterAlt(localctx, 1)
                self.state = 274
                self.match(luaParser.T__45)
                self.state = 276
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if ((((_la - 2)) & ~0x3f) == 0 and ((1 << (_la - 2)) & ((1 << (luaParser.T__1 - 2)) | (1 << (luaParser.T__8 - 2)) | (1 << (luaParser.T__21 - 2)) | (1 << (luaParser.T__22 - 2)) | (1 << (luaParser.T__39 - 2)) | (1 << (luaParser.T__45 - 2)) | (1 << (luaParser.T__49 - 2)) | (1 << (luaParser.T__50 - 2)) | (1 << (luaParser.NIL - 2)) | (1 << (luaParser.BOOL - 2)) | (1 << (luaParser.LONGSTRING - 2)) | (1 << (luaParser.NUMBERAL - 2)) | (1 << (luaParser.ID - 2)) | (1 << (luaParser.NORMAL_STRING - 2)))) != 0):
                    self.state = 275
                    self.explist()


                self.state = 278
                self.match(luaParser.T__46)
                pass
            elif token in [luaParser.T__50]:
                self.enterOuterAlt(localctx, 2)
                self.state = 279
                self.tableconstructor()
                pass
            elif token in [luaParser.LONGSTRING, luaParser.NORMAL_STRING]:
                self.enterOuterAlt(localctx, 3)
                self.state = 280
                self.string()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FunctiondefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funcbody(self):
            return self.getTypedRuleContext(luaParser.FuncbodyContext,0)


        def getRuleIndex(self):
            return luaParser.RULE_functiondef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctiondef" ):
                listener.enterFunctiondef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctiondef" ):
                listener.exitFunctiondef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctiondef" ):
                return visitor.visitFunctiondef(self)
            else:
                return visitor.visitChildren(self)




    def functiondef(self):

        localctx = luaParser.FunctiondefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_functiondef)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 283
            self.match(luaParser.T__39)
            self.state = 284
            self.funcbody()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncbodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def block(self):
            return self.getTypedRuleContext(luaParser.BlockContext,0)


        def parlist(self):
            return self.getTypedRuleContext(luaParser.ParlistContext,0)


        def getRuleIndex(self):
            return luaParser.RULE_funcbody

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFuncbody" ):
                listener.enterFuncbody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFuncbody" ):
                listener.exitFuncbody(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncbody" ):
                return visitor.visitFuncbody(self)
            else:
                return visitor.visitChildren(self)




    def funcbody(self):

        localctx = luaParser.FuncbodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_funcbody)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 286
            self.match(luaParser.T__45)
            self.state = 288
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==luaParser.T__49 or _la==luaParser.ID:
                self.state = 287
                self.parlist()


            self.state = 290
            self.match(luaParser.T__46)
            self.state = 291
            self.block()
            self.state = 292
            self.match(luaParser.T__28)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ParlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def namelist(self):
            return self.getTypedRuleContext(luaParser.NamelistContext,0)


        def getRuleIndex(self):
            return luaParser.RULE_parlist

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParlist" ):
                listener.enterParlist(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParlist" ):
                listener.exitParlist(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParlist" ):
                return visitor.visitParlist(self)
            else:
                return visitor.visitChildren(self)




    def parlist(self):

        localctx = luaParser.ParlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_parlist)
        self._la = 0 # Token type
        try:
            self.state = 300
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [luaParser.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 294
                self.namelist()
                self.state = 297
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==luaParser.T__37:
                    self.state = 295
                    self.match(luaParser.T__37)
                    self.state = 296
                    self.match(luaParser.T__49)


                pass
            elif token in [luaParser.T__49]:
                self.enterOuterAlt(localctx, 2)
                self.state = 299
                self.match(luaParser.T__49)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FieldsepContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return luaParser.RULE_fieldsep

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFieldsep" ):
                listener.enterFieldsep(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFieldsep" ):
                listener.exitFieldsep(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFieldsep" ):
                return visitor.visitFieldsep(self)
            else:
                return visitor.visitChildren(self)




    def fieldsep(self):

        localctx = luaParser.FieldsepContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_fieldsep)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 302
            _la = self._input.LA(1)
            if not(_la==luaParser.T__23 or _la==luaParser.T__37):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TableconstructorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def field(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.FieldContext)
            else:
                return self.getTypedRuleContext(luaParser.FieldContext,i)


        def fieldsep(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.FieldsepContext)
            else:
                return self.getTypedRuleContext(luaParser.FieldsepContext,i)


        def getRuleIndex(self):
            return luaParser.RULE_tableconstructor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTableconstructor" ):
                listener.enterTableconstructor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTableconstructor" ):
                listener.exitTableconstructor(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTableconstructor" ):
                return visitor.visitTableconstructor(self)
            else:
                return visitor.visitChildren(self)




    def tableconstructor(self):

        localctx = luaParser.TableconstructorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_tableconstructor)
        self._la = 0 # Token type
        try:
            self.state = 321
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,31,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 304
                self.match(luaParser.T__50)
                self.state = 305
                self.field()
                self.state = 311
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,29,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 306
                        self.fieldsep()
                        self.state = 307
                        self.field() 
                    self.state = 313
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,29,self._ctx)

                self.state = 315
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==luaParser.T__23 or _la==luaParser.T__37:
                    self.state = 314
                    self.fieldsep()


                self.state = 317
                self.match(luaParser.T__51)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 319
                self.match(luaParser.T__50)
                self.state = 320
                self.match(luaParser.T__51)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FieldContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(luaParser.ExpContext)
            else:
                return self.getTypedRuleContext(luaParser.ExpContext,i)


        def ID(self):
            return self.getToken(luaParser.ID, 0)

        def getRuleIndex(self):
            return luaParser.RULE_field

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterField" ):
                listener.enterField(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitField" ):
                listener.exitField(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitField" ):
                return visitor.visitField(self)
            else:
                return visitor.visitChildren(self)




    def field(self):

        localctx = luaParser.FieldContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_field)
        try:
            self.state = 333
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,32,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 323
                self.match(luaParser.T__47)
                self.state = 324
                self.exp(0)
                self.state = 325
                self.match(luaParser.T__48)
                self.state = 326
                self.match(luaParser.T__24)
                self.state = 327
                self.exp(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 329
                self.match(luaParser.ID)
                self.state = 330
                self.match(luaParser.T__24)
                self.state = 331
                self.exp(0)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 332
                self.exp(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[14] = self.exp_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def exp_sempred(self, localctx:ExpContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 1)
         




