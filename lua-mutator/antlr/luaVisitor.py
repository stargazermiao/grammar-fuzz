# Generated from lua.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .luaParser import luaParser
else:
    from luaParser import luaParser

# This class defines a complete generic visitor for a parse tree produced by luaParser.

class luaVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by luaParser#string.
    def visitString(self, ctx:luaParser.StringContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#binop.
    def visitBinop(self, ctx:luaParser.BinopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#unop.
    def visitUnop(self, ctx:luaParser.UnopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#chunk.
    def visitChunk(self, ctx:luaParser.ChunkContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#block.
    def visitBlock(self, ctx:luaParser.BlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#stat.
    def visitStat(self, ctx:luaParser.StatContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#retstat.
    def visitRetstat(self, ctx:luaParser.RetstatContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#label.
    def visitLabel(self, ctx:luaParser.LabelContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#funcname.
    def visitFuncname(self, ctx:luaParser.FuncnameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#varlist.
    def visitVarlist(self, ctx:luaParser.VarlistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#var.
    def visitVar(self, ctx:luaParser.VarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#varsuffix.
    def visitVarsuffix(self, ctx:luaParser.VarsuffixContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#namelist.
    def visitNamelist(self, ctx:luaParser.NamelistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#explist.
    def visitExplist(self, ctx:luaParser.ExplistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#exp.
    def visitExp(self, ctx:luaParser.ExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#functioncall.
    def visitFunctioncall(self, ctx:luaParser.FunctioncallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#functioncallsuffix.
    def visitFunctioncallsuffix(self, ctx:luaParser.FunctioncallsuffixContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#args.
    def visitArgs(self, ctx:luaParser.ArgsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#functiondef.
    def visitFunctiondef(self, ctx:luaParser.FunctiondefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#funcbody.
    def visitFuncbody(self, ctx:luaParser.FuncbodyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#parlist.
    def visitParlist(self, ctx:luaParser.ParlistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#fieldsep.
    def visitFieldsep(self, ctx:luaParser.FieldsepContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#tableconstructor.
    def visitTableconstructor(self, ctx:luaParser.TableconstructorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by luaParser#field.
    def visitField(self, ctx:luaParser.FieldContext):
        return self.visitChildren(ctx)



del luaParser