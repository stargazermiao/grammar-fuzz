'''
a lua random mutator
random mutation is like this:
1. random pick a node from AST
2. replace it with some a random generated sub tree
'''
from antlr4 import *
from luaVisitor import luaVisitor
from luaParser import luaParser
from luaLexGen import TokenGenerator


# get all node in AST
class LuaASTFlattenVisitor(luaVisitor):

    # Visit a parse tree produced by luaParser#string.
    def visitString(self, ctx: luaParser.StringContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#binop.
    def visitBinop(self, ctx: luaParser.BinopContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#unop.
    def visitUnop(self, ctx: luaParser.UnopContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#chunk.
    def visitChunk(self, ctx: luaParser.ChunkContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#block.
    def visitBlock(self, ctx: luaParser.BlockContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#stat.
    def visitStat(self, ctx: luaParser.StatContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#retstat.
    def visitRetstat(self, ctx: luaParser.RetstatContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#label.
    def visitLabel(self, ctx: luaParser.LabelContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#funcname.
    def visitFuncname(self, ctx: luaParser.FuncnameContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#varlist.
    def visitVarlist(self, ctx: luaParser.VarlistContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#var.
    def visitVar(self, ctx: luaParser.VarContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#varsuffix.
    def visitVarsuffix(self, ctx: luaParser.VarsuffixContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#namelist.
    def visitNamelist(self, ctx: luaParser.NamelistContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#explist.
    def visitExplist(self, ctx: luaParser.ExplistContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#exp.
    def visitExp(self, ctx: luaParser.ExpContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#functioncall.
    def visitFunctioncall(self, ctx: luaParser.FunctioncallContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#functioncallsuffix.
    def visitFunctioncallsuffix(self,
                                ctx: luaParser.FunctioncallsuffixContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#args.
    def visitArgs(self, ctx: luaParser.ArgsContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#functiondef.
    def visitFunctiondef(self, ctx: luaParser.FunctiondefContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#funcbody.
    def visitFuncbody(self, ctx: luaParser.FuncbodyContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#parlist.
    def visitParlist(self, ctx: luaParser.ParlistContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#fieldsep.
    def visitFieldsep(self, ctx: luaParser.FieldsepContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#tableconstructor.
    def visitTableconstructor(self, ctx: luaParser.TableconstructorContext):
        return [ctx] + self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#field.
    def visitField(self, ctx: luaParser.FieldContext):
        return [ctx] + self.visitChildren(ctx)



def randomMutate(stream: TokenStream, node: ParserRuleContext):
    '''
    random mutate a node inside a token stream
    '''
    start, end = node.getSourceInterval()
    
    


# this will create a new node which is same type of target ctx
class LuaMutator(luaVisitor):
    def __init__(self, target: ParserRuleContext):
        '''
        target is target rule index
        '''
        self.tokenGen = TokenGenerator()
        self.target = target

    # Visit a parse tree produced by luaParser#string.
    def visitString(self, ctx: luaParser.StringContext):
        if ctx is self.target:
            self.tokenGen.genString()
        else:
            return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#binop.
    def visitBinop(self, ctx: luaParser.BinopContext):
        tokens = ctx.getTokens()
        
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#unop.
    def visitUnop(self, ctx: luaParser.UnopContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#chunk.
    def visitChunk(self, ctx: luaParser.ChunkContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#block.
    def visitBlock(self, ctx: luaParser.BlockContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#stat.
    def visitStat(self, ctx: luaParser.StatContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#retstat.
    def visitRetstat(self, ctx: luaParser.RetstatContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#label.
    def visitLabel(self, ctx: luaParser.LabelContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#funcname.
    def visitFuncname(self, ctx: luaParser.FuncnameContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#varlist.
    def visitVarlist(self, ctx: luaParser.VarlistContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#var.
    def visitVar(self, ctx: luaParser.VarContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#varsuffix.
    def visitVarsuffix(self, ctx: luaParser.VarsuffixContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#namelist.
    def visitNamelist(self, ctx: luaParser.NamelistContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#explist.
    def visitExplist(self, ctx: luaParser.ExplistContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#exp.
    def visitExp(self, ctx: luaParser.ExpContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#functioncall.
    def visitFunctioncall(self, ctx: luaParser.FunctioncallContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#functioncallsuffix.
    def visitFunctioncallsuffix(self,
                                ctx: luaParser.FunctioncallsuffixContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#args.
    def visitArgs(self, ctx: luaParser.ArgsContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#functiondef.
    def visitFunctiondef(self, ctx: luaParser.FunctiondefContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#funcbody.
    def visitFuncbody(self, ctx: luaParser.FuncbodyContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#parlist.
    def visitParlist(self, ctx: luaParser.ParlistContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#fieldsep.
    def visitFieldsep(self, ctx: luaParser.FieldsepContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#tableconstructor.
    def visitTableconstructor(self, ctx: luaParser.TableconstructorContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)

    # Visit a parse tree produced by luaParser#field.
    def visitField(self, ctx: luaParser.FieldContext):
        self.replaceChild(ctx)
        return self.visitChildren(ctx)
