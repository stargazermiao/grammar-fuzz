# Generated from lua.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .luaParser import luaParser
else:
    from luaParser import luaParser

# This class defines a complete listener for a parse tree produced by luaParser.
class luaListener(ParseTreeListener):

    # Enter a parse tree produced by luaParser#string.
    def enterString(self, ctx:luaParser.StringContext):
        pass

    # Exit a parse tree produced by luaParser#string.
    def exitString(self, ctx:luaParser.StringContext):
        pass


    # Enter a parse tree produced by luaParser#binop.
    def enterBinop(self, ctx:luaParser.BinopContext):
        pass

    # Exit a parse tree produced by luaParser#binop.
    def exitBinop(self, ctx:luaParser.BinopContext):
        pass


    # Enter a parse tree produced by luaParser#unop.
    def enterUnop(self, ctx:luaParser.UnopContext):
        pass

    # Exit a parse tree produced by luaParser#unop.
    def exitUnop(self, ctx:luaParser.UnopContext):
        pass


    # Enter a parse tree produced by luaParser#chunk.
    def enterChunk(self, ctx:luaParser.ChunkContext):
        pass

    # Exit a parse tree produced by luaParser#chunk.
    def exitChunk(self, ctx:luaParser.ChunkContext):
        pass


    # Enter a parse tree produced by luaParser#block.
    def enterBlock(self, ctx:luaParser.BlockContext):
        pass

    # Exit a parse tree produced by luaParser#block.
    def exitBlock(self, ctx:luaParser.BlockContext):
        pass


    # Enter a parse tree produced by luaParser#stat.
    def enterStat(self, ctx:luaParser.StatContext):
        pass

    # Exit a parse tree produced by luaParser#stat.
    def exitStat(self, ctx:luaParser.StatContext):
        pass


    # Enter a parse tree produced by luaParser#retstat.
    def enterRetstat(self, ctx:luaParser.RetstatContext):
        pass

    # Exit a parse tree produced by luaParser#retstat.
    def exitRetstat(self, ctx:luaParser.RetstatContext):
        pass


    # Enter a parse tree produced by luaParser#label.
    def enterLabel(self, ctx:luaParser.LabelContext):
        pass

    # Exit a parse tree produced by luaParser#label.
    def exitLabel(self, ctx:luaParser.LabelContext):
        pass


    # Enter a parse tree produced by luaParser#funcname.
    def enterFuncname(self, ctx:luaParser.FuncnameContext):
        pass

    # Exit a parse tree produced by luaParser#funcname.
    def exitFuncname(self, ctx:luaParser.FuncnameContext):
        pass


    # Enter a parse tree produced by luaParser#varlist.
    def enterVarlist(self, ctx:luaParser.VarlistContext):
        pass

    # Exit a parse tree produced by luaParser#varlist.
    def exitVarlist(self, ctx:luaParser.VarlistContext):
        pass


    # Enter a parse tree produced by luaParser#var.
    def enterVar(self, ctx:luaParser.VarContext):
        pass

    # Exit a parse tree produced by luaParser#var.
    def exitVar(self, ctx:luaParser.VarContext):
        pass


    # Enter a parse tree produced by luaParser#varsuffix.
    def enterVarsuffix(self, ctx:luaParser.VarsuffixContext):
        pass

    # Exit a parse tree produced by luaParser#varsuffix.
    def exitVarsuffix(self, ctx:luaParser.VarsuffixContext):
        pass


    # Enter a parse tree produced by luaParser#namelist.
    def enterNamelist(self, ctx:luaParser.NamelistContext):
        pass

    # Exit a parse tree produced by luaParser#namelist.
    def exitNamelist(self, ctx:luaParser.NamelistContext):
        pass


    # Enter a parse tree produced by luaParser#explist.
    def enterExplist(self, ctx:luaParser.ExplistContext):
        pass

    # Exit a parse tree produced by luaParser#explist.
    def exitExplist(self, ctx:luaParser.ExplistContext):
        pass


    # Enter a parse tree produced by luaParser#exp.
    def enterExp(self, ctx:luaParser.ExpContext):
        pass

    # Exit a parse tree produced by luaParser#exp.
    def exitExp(self, ctx:luaParser.ExpContext):
        pass


    # Enter a parse tree produced by luaParser#functioncall.
    def enterFunctioncall(self, ctx:luaParser.FunctioncallContext):
        pass

    # Exit a parse tree produced by luaParser#functioncall.
    def exitFunctioncall(self, ctx:luaParser.FunctioncallContext):
        pass


    # Enter a parse tree produced by luaParser#functioncallsuffix.
    def enterFunctioncallsuffix(self, ctx:luaParser.FunctioncallsuffixContext):
        pass

    # Exit a parse tree produced by luaParser#functioncallsuffix.
    def exitFunctioncallsuffix(self, ctx:luaParser.FunctioncallsuffixContext):
        pass


    # Enter a parse tree produced by luaParser#args.
    def enterArgs(self, ctx:luaParser.ArgsContext):
        pass

    # Exit a parse tree produced by luaParser#args.
    def exitArgs(self, ctx:luaParser.ArgsContext):
        pass


    # Enter a parse tree produced by luaParser#functiondef.
    def enterFunctiondef(self, ctx:luaParser.FunctiondefContext):
        pass

    # Exit a parse tree produced by luaParser#functiondef.
    def exitFunctiondef(self, ctx:luaParser.FunctiondefContext):
        pass


    # Enter a parse tree produced by luaParser#funcbody.
    def enterFuncbody(self, ctx:luaParser.FuncbodyContext):
        pass

    # Exit a parse tree produced by luaParser#funcbody.
    def exitFuncbody(self, ctx:luaParser.FuncbodyContext):
        pass


    # Enter a parse tree produced by luaParser#parlist.
    def enterParlist(self, ctx:luaParser.ParlistContext):
        pass

    # Exit a parse tree produced by luaParser#parlist.
    def exitParlist(self, ctx:luaParser.ParlistContext):
        pass


    # Enter a parse tree produced by luaParser#fieldsep.
    def enterFieldsep(self, ctx:luaParser.FieldsepContext):
        pass

    # Exit a parse tree produced by luaParser#fieldsep.
    def exitFieldsep(self, ctx:luaParser.FieldsepContext):
        pass


    # Enter a parse tree produced by luaParser#tableconstructor.
    def enterTableconstructor(self, ctx:luaParser.TableconstructorContext):
        pass

    # Exit a parse tree produced by luaParser#tableconstructor.
    def exitTableconstructor(self, ctx:luaParser.TableconstructorContext):
        pass


    # Enter a parse tree produced by luaParser#field.
    def enterField(self, ctx:luaParser.FieldContext):
        pass

    # Exit a parse tree produced by luaParser#field.
    def exitField(self, ctx:luaParser.FieldContext):
        pass


