# Generated from mrubysimp.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3!")
        buf.write("\u008e\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\2\3\3\6\3\30\n\3\r\3\16")
        buf.write("\3\31\3\4\3\4\3\4\3\4\3\4\3\4\5\4\"\n\4\3\4\3\4\5\4&\n")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4/\n\4\3\4\3\4\3\4\3")
        buf.write("\4\5\4\65\n\4\3\4\3\4\5\49\n\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\5\4D\n\4\3\4\3\4\3\4\3\4\5\4J\n\4\3\4\3\4")
        buf.write("\5\4N\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4f\n\4")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\5\5n\n\5\3\6\3\6\3\6\3\6\3\7")
        buf.write("\3\7\3\7\3\7\3\7\5\7y\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u008a\n\b\3\t\3\t")
        buf.write("\3\t\2\2\n\2\4\6\b\n\f\16\20\2\2\2\u00a4\2\22\3\2\2\2")
        buf.write("\4\27\3\2\2\2\6e\3\2\2\2\bm\3\2\2\2\no\3\2\2\2\fx\3\2")
        buf.write("\2\2\16\u0089\3\2\2\2\20\u008b\3\2\2\2\22\23\7\13\2\2")
        buf.write("\23\24\5\4\3\2\24\25\7\2\2\3\25\3\3\2\2\2\26\30\5\6\4")
        buf.write("\2\27\26\3\2\2\2\30\31\3\2\2\2\31\27\3\2\2\2\31\32\3\2")
        buf.write("\2\2\32\5\3\2\2\2\33\34\7\17\2\2\34\35\5\b\5\2\35\36\7")
        buf.write("\16\2\2\36\37\5\20\t\2\37!\7\20\2\2 \"\5\f\7\2! \3\2\2")
        buf.write("\2!\"\3\2\2\2\"#\3\2\2\2#%\7\21\2\2$&\5\4\3\2%$\3\2\2")
        buf.write("\2%&\3\2\2\2&f\3\2\2\2\'(\5\b\5\2()\7\22\2\2)*\5\b\5\2")
        buf.write("*+\7\16\2\2+,\5\20\t\2,.\7\20\2\2-/\5\f\7\2.-\3\2\2\2")
        buf.write("./\3\2\2\2/\60\3\2\2\2\60\61\7\21\2\2\61\62\7\23\2\2\62")
        buf.write("\64\7\25\2\2\63\65\5\f\7\2\64\63\3\2\2\2\64\65\3\2\2\2")
        buf.write("\65\66\3\2\2\2\668\7\25\2\2\679\5\4\3\28\67\3\2\2\289")
        buf.write("\3\2\2\29:\3\2\2\2:;\7\24\2\2;f\3\2\2\2<=\5\b\5\2=>\7")
        buf.write("\22\2\2>?\5\20\t\2?@\7\16\2\2@A\5\20\t\2AC\7\20\2\2BD")
        buf.write("\5\f\7\2CB\3\2\2\2CD\3\2\2\2DE\3\2\2\2EF\7\21\2\2FG\7")
        buf.write("\23\2\2GI\7\25\2\2HJ\5\f\7\2IH\3\2\2\2IJ\3\2\2\2JK\3\2")
        buf.write("\2\2KM\7\25\2\2LN\5\4\3\2ML\3\2\2\2MN\3\2\2\2NO\3\2\2")
        buf.write("\2OP\7\24\2\2Pf\3\2\2\2QR\5\b\5\2RS\7\22\2\2ST\5\b\5\2")
        buf.write("Tf\3\2\2\2UV\5\b\5\2VW\7\22\2\2WX\5\16\b\2Xf\3\2\2\2Y")
        buf.write("Z\7\26\2\2Zf\5\b\5\2[\\\7\32\2\2\\f\5\b\5\2]^\7\27\2\2")
        buf.write("^f\5\b\5\2_`\7\30\2\2`f\5\b\5\2ab\7\31\2\2bf\5\b\5\2c")
        buf.write("d\7\33\2\2df\5\b\5\2e\33\3\2\2\2e\'\3\2\2\2e<\3\2\2\2")
        buf.write("eQ\3\2\2\2eU\3\2\2\2eY\3\2\2\2e[\3\2\2\2e]\3\2\2\2e_\3")
        buf.write("\2\2\2ea\3\2\2\2ec\3\2\2\2f\7\3\2\2\2gn\7\37\2\2hn\5\n")
        buf.write("\6\2ij\7\20\2\2jk\5\n\6\2kl\7\21\2\2ln\3\2\2\2mg\3\2\2")
        buf.write("\2mh\3\2\2\2mi\3\2\2\2n\t\3\2\2\2op\5\20\t\2pq\7\r\2\2")
        buf.write("qr\5\20\t\2r\13\3\2\2\2sy\5\b\5\2tu\5\b\5\2uv\7\34\2\2")
        buf.write("vw\5\f\7\2wy\3\2\2\2xs\3\2\2\2xt\3\2\2\2y\r\3\2\2\2z{")
        buf.write("\7\3\2\2{|\7\37\2\2|\u008a\7\3\2\2}\u008a\7\36\2\2~\u008a")
        buf.write("\7\4\2\2\177\u008a\7\5\2\2\u0080\u008a\7\6\2\2\u0081\u008a")
        buf.write("\7\7\2\2\u0082\u0083\5\b\5\2\u0083\u0084\7\35\2\2\u0084")
        buf.write("\u0085\5\b\5\2\u0085\u008a\3\2\2\2\u0086\u008a\7\b\2\2")
        buf.write("\u0087\u008a\7\t\2\2\u0088\u008a\7\n\2\2\u0089z\3\2\2")
        buf.write("\2\u0089}\3\2\2\2\u0089~\3\2\2\2\u0089\177\3\2\2\2\u0089")
        buf.write("\u0080\3\2\2\2\u0089\u0081\3\2\2\2\u0089\u0082\3\2\2\2")
        buf.write("\u0089\u0086\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u0088\3")
        buf.write("\2\2\2\u008a\17\3\2\2\2\u008b\u008c\7\37\2\2\u008c\21")
        buf.write("\3\2\2\2\17\31!%.\648CIMemx\u0089")
        return buf.getvalue()


class mrubysimpParser ( Parser ):

    grammarFileName = "mrubysimp.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'\"'", "'nil'", "'true'", "'false'", 
                     "'/foo/'", "'[]'", "'[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,nil]'", 
                     "'{}'", "'a=0\nb=\"asdfasdfasdf adaf asdf asdfa sdf asdfasdfasdfa sdf\"\nc={1=>1, 2=>\"foo\", \"foo\"=>nil, nil=> nil }\nd=[1,nil,\" sdfg\"]\nsrand(1337)\n'", 
                     "'\n'", "'::'", "'.'", "'def'", "'('", "')'", "'='", 
                     "'{'", "'}'", "'|'", "'return'", "'raise'", "'yield'", 
                     "'continue'", "'break'", "'next'", "','", "'..'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "START_HEAD", "NEWLINE", "DUO_COL", "DOT", 
                      "DEF", "LP", "RP", "EQ", "LCB", "RCB", "MID", "RETURN", 
                      "RAISE", "YIELD", "CONTINUE", "BREAK", "NEXT", "COMMA", 
                      "DOT2", "NUM", "NAME", "WS", "ErrorChar" ]

    RULE_start = 0
    RULE_program = 1
    RULE_statement = 2
    RULE_var = 3
    RULE_ref = 4
    RULE_args = 5
    RULE_val = 6
    RULE_identifier = 7

    ruleNames =  [ "start", "program", "statement", "var", "ref", "args", 
                   "val", "identifier" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    START_HEAD=9
    NEWLINE=10
    DUO_COL=11
    DOT=12
    DEF=13
    LP=14
    RP=15
    EQ=16
    LCB=17
    RCB=18
    MID=19
    RETURN=20
    RAISE=21
    YIELD=22
    CONTINUE=23
    BREAK=24
    NEXT=25
    COMMA=26
    DOT2=27
    NUM=28
    NAME=29
    WS=30
    ErrorChar=31

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class StartContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def START_HEAD(self):
            return self.getToken(mrubysimpParser.START_HEAD, 0)

        def program(self):
            return self.getTypedRuleContext(mrubysimpParser.ProgramContext,0)


        def EOF(self):
            return self.getToken(mrubysimpParser.EOF, 0)

        def getRuleIndex(self):
            return mrubysimpParser.RULE_start

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStart" ):
                return visitor.visitStart(self)
            else:
                return visitor.visitChildren(self)




    def start(self):

        localctx = mrubysimpParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_start)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 16
            self.match(mrubysimpParser.START_HEAD)
            self.state = 17
            self.program()
            self.state = 18
            self.match(mrubysimpParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(mrubysimpParser.StatementContext)
            else:
                return self.getTypedRuleContext(mrubysimpParser.StatementContext,i)


        def getRuleIndex(self):
            return mrubysimpParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = mrubysimpParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_program)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 21 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 20
                    self.statement()

                else:
                    raise NoViableAltException(self)
                self.state = 23 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,0,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DEF(self):
            return self.getToken(mrubysimpParser.DEF, 0)

        def var(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(mrubysimpParser.VarContext)
            else:
                return self.getTypedRuleContext(mrubysimpParser.VarContext,i)


        def DOT(self):
            return self.getToken(mrubysimpParser.DOT, 0)

        def identifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(mrubysimpParser.IdentifierContext)
            else:
                return self.getTypedRuleContext(mrubysimpParser.IdentifierContext,i)


        def LP(self):
            return self.getToken(mrubysimpParser.LP, 0)

        def RP(self):
            return self.getToken(mrubysimpParser.RP, 0)

        def args(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(mrubysimpParser.ArgsContext)
            else:
                return self.getTypedRuleContext(mrubysimpParser.ArgsContext,i)


        def program(self):
            return self.getTypedRuleContext(mrubysimpParser.ProgramContext,0)


        def EQ(self):
            return self.getToken(mrubysimpParser.EQ, 0)

        def LCB(self):
            return self.getToken(mrubysimpParser.LCB, 0)

        def MID(self, i:int=None):
            if i is None:
                return self.getTokens(mrubysimpParser.MID)
            else:
                return self.getToken(mrubysimpParser.MID, i)

        def RCB(self):
            return self.getToken(mrubysimpParser.RCB, 0)

        def val(self):
            return self.getTypedRuleContext(mrubysimpParser.ValContext,0)


        def RETURN(self):
            return self.getToken(mrubysimpParser.RETURN, 0)

        def BREAK(self):
            return self.getToken(mrubysimpParser.BREAK, 0)

        def RAISE(self):
            return self.getToken(mrubysimpParser.RAISE, 0)

        def YIELD(self):
            return self.getToken(mrubysimpParser.YIELD, 0)

        def CONTINUE(self):
            return self.getToken(mrubysimpParser.CONTINUE, 0)

        def NEXT(self):
            return self.getToken(mrubysimpParser.NEXT, 0)

        def getRuleIndex(self):
            return mrubysimpParser.RULE_statement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = mrubysimpParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_statement)
        self._la = 0 # Token type
        try:
            self.state = 99
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 25
                self.match(mrubysimpParser.DEF)
                self.state = 26
                self.var()
                self.state = 27
                self.match(mrubysimpParser.DOT)
                self.state = 28
                self.identifier()
                self.state = 29
                self.match(mrubysimpParser.LP)
                self.state = 31
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==mrubysimpParser.LP or _la==mrubysimpParser.NAME:
                    self.state = 30
                    self.args()


                self.state = 33
                self.match(mrubysimpParser.RP)
                self.state = 35
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
                if la_ == 1:
                    self.state = 34
                    self.program()


                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 37
                self.var()
                self.state = 38
                self.match(mrubysimpParser.EQ)
                self.state = 39
                self.var()
                self.state = 40
                self.match(mrubysimpParser.DOT)
                self.state = 41
                self.identifier()
                self.state = 42
                self.match(mrubysimpParser.LP)
                self.state = 44
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==mrubysimpParser.LP or _la==mrubysimpParser.NAME:
                    self.state = 43
                    self.args()


                self.state = 46
                self.match(mrubysimpParser.RP)
                self.state = 47
                self.match(mrubysimpParser.LCB)
                self.state = 48
                self.match(mrubysimpParser.MID)
                self.state = 50
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==mrubysimpParser.LP or _la==mrubysimpParser.NAME:
                    self.state = 49
                    self.args()


                self.state = 52
                self.match(mrubysimpParser.MID)
                self.state = 54
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << mrubysimpParser.DEF) | (1 << mrubysimpParser.LP) | (1 << mrubysimpParser.RETURN) | (1 << mrubysimpParser.RAISE) | (1 << mrubysimpParser.YIELD) | (1 << mrubysimpParser.CONTINUE) | (1 << mrubysimpParser.BREAK) | (1 << mrubysimpParser.NEXT) | (1 << mrubysimpParser.NAME))) != 0):
                    self.state = 53
                    self.program()


                self.state = 56
                self.match(mrubysimpParser.RCB)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 58
                self.var()
                self.state = 59
                self.match(mrubysimpParser.EQ)
                self.state = 60
                self.identifier()
                self.state = 61
                self.match(mrubysimpParser.DOT)
                self.state = 62
                self.identifier()
                self.state = 63
                self.match(mrubysimpParser.LP)
                self.state = 65
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==mrubysimpParser.LP or _la==mrubysimpParser.NAME:
                    self.state = 64
                    self.args()


                self.state = 67
                self.match(mrubysimpParser.RP)
                self.state = 68
                self.match(mrubysimpParser.LCB)
                self.state = 69
                self.match(mrubysimpParser.MID)
                self.state = 71
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==mrubysimpParser.LP or _la==mrubysimpParser.NAME:
                    self.state = 70
                    self.args()


                self.state = 73
                self.match(mrubysimpParser.MID)
                self.state = 75
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << mrubysimpParser.DEF) | (1 << mrubysimpParser.LP) | (1 << mrubysimpParser.RETURN) | (1 << mrubysimpParser.RAISE) | (1 << mrubysimpParser.YIELD) | (1 << mrubysimpParser.CONTINUE) | (1 << mrubysimpParser.BREAK) | (1 << mrubysimpParser.NEXT) | (1 << mrubysimpParser.NAME))) != 0):
                    self.state = 74
                    self.program()


                self.state = 77
                self.match(mrubysimpParser.RCB)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 79
                self.var()
                self.state = 80
                self.match(mrubysimpParser.EQ)
                self.state = 81
                self.var()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 83
                self.var()
                self.state = 84
                self.match(mrubysimpParser.EQ)
                self.state = 85
                self.val()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 87
                self.match(mrubysimpParser.RETURN)
                self.state = 88
                self.var()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 89
                self.match(mrubysimpParser.BREAK)
                self.state = 90
                self.var()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 91
                self.match(mrubysimpParser.RAISE)
                self.state = 92
                self.var()
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 93
                self.match(mrubysimpParser.YIELD)
                self.state = 94
                self.var()
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 95
                self.match(mrubysimpParser.CONTINUE)
                self.state = 96
                self.var()
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 97
                self.match(mrubysimpParser.NEXT)
                self.state = 98
                self.var()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(mrubysimpParser.NAME, 0)

        def ref(self):
            return self.getTypedRuleContext(mrubysimpParser.RefContext,0)


        def LP(self):
            return self.getToken(mrubysimpParser.LP, 0)

        def RP(self):
            return self.getToken(mrubysimpParser.RP, 0)

        def getRuleIndex(self):
            return mrubysimpParser.RULE_var

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar" ):
                return visitor.visitVar(self)
            else:
                return visitor.visitChildren(self)




    def var(self):

        localctx = mrubysimpParser.VarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_var)
        try:
            self.state = 107
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 101
                self.match(mrubysimpParser.NAME)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 102
                self.ref()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 103
                self.match(mrubysimpParser.LP)
                self.state = 104
                self.ref()
                self.state = 105
                self.match(mrubysimpParser.RP)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def identifier(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(mrubysimpParser.IdentifierContext)
            else:
                return self.getTypedRuleContext(mrubysimpParser.IdentifierContext,i)


        def DUO_COL(self):
            return self.getToken(mrubysimpParser.DUO_COL, 0)

        def getRuleIndex(self):
            return mrubysimpParser.RULE_ref

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRef" ):
                return visitor.visitRef(self)
            else:
                return visitor.visitChildren(self)




    def ref(self):

        localctx = mrubysimpParser.RefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_ref)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 109
            self.identifier()
            self.state = 110
            self.match(mrubysimpParser.DUO_COL)
            self.state = 111
            self.identifier()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArgsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var(self):
            return self.getTypedRuleContext(mrubysimpParser.VarContext,0)


        def COMMA(self):
            return self.getToken(mrubysimpParser.COMMA, 0)

        def args(self):
            return self.getTypedRuleContext(mrubysimpParser.ArgsContext,0)


        def getRuleIndex(self):
            return mrubysimpParser.RULE_args

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgs" ):
                return visitor.visitArgs(self)
            else:
                return visitor.visitChildren(self)




    def args(self):

        localctx = mrubysimpParser.ArgsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_args)
        try:
            self.state = 118
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 113
                self.var()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 114
                self.var()
                self.state = 115
                self.match(mrubysimpParser.COMMA)
                self.state = 116
                self.args()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ValContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(mrubysimpParser.NAME, 0)

        def NUM(self):
            return self.getToken(mrubysimpParser.NUM, 0)

        def var(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(mrubysimpParser.VarContext)
            else:
                return self.getTypedRuleContext(mrubysimpParser.VarContext,i)


        def DOT2(self):
            return self.getToken(mrubysimpParser.DOT2, 0)

        def getRuleIndex(self):
            return mrubysimpParser.RULE_val

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVal" ):
                return visitor.visitVal(self)
            else:
                return visitor.visitChildren(self)




    def val(self):

        localctx = mrubysimpParser.ValContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_val)
        try:
            self.state = 135
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [mrubysimpParser.T__0]:
                self.enterOuterAlt(localctx, 1)
                self.state = 120
                self.match(mrubysimpParser.T__0)
                self.state = 121
                self.match(mrubysimpParser.NAME)
                self.state = 122
                self.match(mrubysimpParser.T__0)
                pass
            elif token in [mrubysimpParser.NUM]:
                self.enterOuterAlt(localctx, 2)
                self.state = 123
                self.match(mrubysimpParser.NUM)
                pass
            elif token in [mrubysimpParser.T__1]:
                self.enterOuterAlt(localctx, 3)
                self.state = 124
                self.match(mrubysimpParser.T__1)
                pass
            elif token in [mrubysimpParser.T__2]:
                self.enterOuterAlt(localctx, 4)
                self.state = 125
                self.match(mrubysimpParser.T__2)
                pass
            elif token in [mrubysimpParser.T__3]:
                self.enterOuterAlt(localctx, 5)
                self.state = 126
                self.match(mrubysimpParser.T__3)
                pass
            elif token in [mrubysimpParser.T__4]:
                self.enterOuterAlt(localctx, 6)
                self.state = 127
                self.match(mrubysimpParser.T__4)
                pass
            elif token in [mrubysimpParser.LP, mrubysimpParser.NAME]:
                self.enterOuterAlt(localctx, 7)
                self.state = 128
                self.var()
                self.state = 129
                self.match(mrubysimpParser.DOT2)
                self.state = 130
                self.var()
                pass
            elif token in [mrubysimpParser.T__5]:
                self.enterOuterAlt(localctx, 8)
                self.state = 132
                self.match(mrubysimpParser.T__5)
                pass
            elif token in [mrubysimpParser.T__6]:
                self.enterOuterAlt(localctx, 9)
                self.state = 133
                self.match(mrubysimpParser.T__6)
                pass
            elif token in [mrubysimpParser.T__7]:
                self.enterOuterAlt(localctx, 10)
                self.state = 134
                self.match(mrubysimpParser.T__7)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IdentifierContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(mrubysimpParser.NAME, 0)

        def getRuleIndex(self):
            return mrubysimpParser.RULE_identifier

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIdentifier" ):
                return visitor.visitIdentifier(self)
            else:
                return visitor.visitChildren(self)




    def identifier(self):

        localctx = mrubysimpParser.IdentifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_identifier)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 137
            self.match(mrubysimpParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





