# Generated from mrubysimp.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .mrubysimpParser import mrubysimpParser
else:
    from mrubysimpParser import mrubysimpParser

# This class defines a complete generic visitor for a parse tree produced by mrubysimpParser.

class mrubysimpVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by mrubysimpParser#start.
    def visitStart(self, ctx:mrubysimpParser.StartContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by mrubysimpParser#program.
    def visitProgram(self, ctx:mrubysimpParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by mrubysimpParser#statement.
    def visitStatement(self, ctx:mrubysimpParser.StatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by mrubysimpParser#var.
    def visitVar(self, ctx:mrubysimpParser.VarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by mrubysimpParser#ref.
    def visitRef(self, ctx:mrubysimpParser.RefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by mrubysimpParser#args.
    def visitArgs(self, ctx:mrubysimpParser.ArgsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by mrubysimpParser#val.
    def visitVal(self, ctx:mrubysimpParser.ValContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by mrubysimpParser#identifier.
    def visitIdentifier(self, ctx:mrubysimpParser.IdentifierContext):
        return self.visitChildren(ctx)



del mrubysimpParser