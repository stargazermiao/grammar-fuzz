# afl++ mruby grammar random mutator

# Yihao Sun
# 2020 Syracuse

import time
import random
import requests
from antlr4 import *
from SexprVisitor import SexprVisitor
from mrubysimpLexer import mrubysimpLexer
from mrubysimpParser import mrubysimpParser
from ast import ASTNode
from antlr4.error.ErrorListener import ErrorListener

# some default seed progam
SEED = '''a=0
b="asdfasdfasdf adaf asdf asdfa sdf asdfasdfasdfa sdf"
c={1=>1, 2=>"foo", "foo"=>nil, nil=> nil }
d=[1,nil," sdfg"]
srand(1337)
a = 4

'''

PREV_BUFFER = bytearray()
TRIM_BUFFER = bytearray()


class SyntaxErrorListner(ErrorListener):
    def __init__(self):
        super(SyntaxErrorListner, self).__init__()

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        print("syntax error: {} , line: {}, col: {}, msg: {}".format(
            offendingSymbol, line, column, msg))
        raise Exception("Syntax Error")


def init(seed):
    '''
    Called once when AFLFuzz starts up. Used to seed our RNG.
    @type seed: int
    @param seed: A 32-bit random value
    '''
    random.seed(seed)


def deinit():
    pass

# random select a node form it's AST


def randomSelectNode(node: ASTNode, max_id):
    position = random.randint(0, max_id)

    def search(node: ASTNode):
        if node.id == position:
            return node
        else:
            for c in node.children:
                res = search(c)
                if res is not None:
                    return res
            return None
    return search(node)


def parse(code):
    '''
    parse a give string into ASTNode
    '''
    sstream = InputStream(code)
    lexer = mrubysimpLexer(sstream)
    tStream = CommonTokenStream(lexer)
    parser = mrubysimpParser(tStream)
    parser.addErrorListener(SyntaxErrorListner())
    try:
        root = parser.start()
        visitor = SexprVisitor()
        rootAST = visitor.visit(root)
    except:
        print(code)
        exit(0)
        sstream = InputStream(SEED)
        lexer = mrubysimpLexer(sstream)
        tStream = CommonTokenStream(lexer)
        parser = mrubysimpParser(tStream)
        root = parser.start()
        visitor = SexprVisitor()
        rootAST = visitor.visit(root)
    return (rootAST, visitor.counter)


def json_to_ast(j):
    ast = ASTNode(j['type'], int(j['id']), int(j['startL']),
                  int(j['startC']), int(j['endL']), int(j['endC']))
    children = []
    for c in j['children']:
        if c is not None:
            children.append(json_to_ast(c))
    ast.children = children
    return ast


def jparse(code):
    '''
    parse in java.... antlr's py port has severe performance issue
    '''
    rep = requests.post("http://localhost:8081/parse",
                        data={'code': code.encode('utf-8')})
    rootJson = rep.json()
    rootAST = json_to_ast(rootJson)
    # print(rootAST)
    return (rootAST, rootJson['totalCount'])


def fuzz(buf, add_buf, max_size):
    '''
    Called per fuzzing iteration.
    @type buf: bytearray
    @param buf: The buffer that should be mutated.
    @type add_buf: bytearray
    @param add_buf: A second buffer that can be used as mutation source.
    @type max_size: int
    @param max_size: Maximum size of the mutated output. The mutation must not
        produce data larger than max_size.
    @rtype: bytearray
    @return: A new bytearray containing the mutated data
    '''
    global PREV_BUFFER
    try:
        bufStr = buf.decode()
    except UnicodeDecodeError:
        bufStr = SEED
    PREV_BUFFER = bufStr
    # print(bufStr)
    #  parsing
    # start_t = time.time()
    # rootAST, vcounter = parse(bufStr)
    rootAST, vcounter = jparse(bufStr)

    # end_t = time.time()

    # if this is a very deep AST, just discard it switch to pure random mutation
    # because the excecuttion of mutation create by this one will be harmful to later
    # I suspect this should be something in trimer, but it's hard to even finish iteration one
    # print("parsing time : {} ms \n".format(end_t - start_t))

    # random muatate
    selected = None
    # start_t = time.time()
    while selected is None:
        selected = randomSelectNode(rootAST, vcounter)
    # find position need to be mutated
    rep = requests.post("http://localhost:8080/mutate",
                        data={'original': selected.type})
    muatated = rep.json()['mutated']
    # end_t = time.time()
    # print("mutation time : {} ms\n".format(end_t - start_t))
    # print(muatated)
    # re create mutated code
    # delete all char in pre-mutated code
    lines = bufStr.splitlines(keepends=True)
    newlines = []
    # print("select from ({} {}) to ({} {})".format(selected.startL, selected.startC, selected.endL, selected.endC))
    replaced = ""
    for i in range(len(lines)):
        if i < (selected.startL - 1):
            newlines.append(lines[i])
        elif i == (selected.startL - 1) and i == (selected.endL - 1):
            head = lines[i][:selected.startC]
            tail = lines[i][(selected.endC+1):]
            replaced = lines[i][selected.startC:(selected.endC+1)]
            newlines.append(head + muatated + tail)
        elif i == (selected.startL - 1):
            nline = ""
            replaced = replaced + lines[i][selected.startC:]
            if selected.startC != 0:
                nline = lines[i][:selected.startC]
            newlines.append(nline + muatated)
        elif i < (selected.endL - 1):
            continue
        elif i != (selected.startL - 1) and i == (selected.endL - 1):
            replaced = replaced + lines[i][:(selected.endC+1)]
            if selected.endC >= len(lines[i]):
                continue
            else:
                newlines.append(lines[i][(selected.endC+1):])
        else:
            newlines.append(lines[i])
    # print("repalced part: \n {} \n".format(replaced))
    return bytearray("".join(newlines).encode('utf-8'))


# trim here is just go back to previous valid program
def init_trim(buf):
    '''
    Called per trimming iteration.

    @type buf: bytearray
    @param buf: The buffer that should be trimmed.

    @rtype: int
    @return: The maximum number of trimming steps.
    '''
    global TRIM_BUFFER
    TRIM_BUFFER = buf
    # Figure out how many trimming steps are possible.
    # If this is not possible for your trimming, you can
    # return 1 instead and always return 0 in post_trim
    # until you are done (then you return 1).
    return 1


def trim():
    '''
    Called per trimming iteration.

    @rtype: bytearray
    @return: A new bytearray containing the trimmed data.
    '''
    global TRIM_BUFFER
    global PREV_BUFFER

    start_t = time.time()
    rootAST, vcounter = jparse(TRIM_BUFFER.decode())

    end_t = time.time()

    if end_t - start_t >= 0.05:
        return bytearray(SEED.encode('utf-8'))
    else:
        return TRIM_BUFFER


def post_trim(success):
    '''
    Called after each trimming operation.

    @type success: bool
    @param success: Indicates if the last trim operation was successful.

    @rtype: int
    @return: The next trim index (0 to max number of steps) where max
             number of steps indicates the trimming is done.
    '''
    # global ...
    # if not success:
    #     # Restore last known successful input, determine next index
    # else:
    #     # Just determine the next index, based on what was successfully
    #     # removed in the last step

    # return next_index
    return 1


# test
if __name__ == "__main__":
    _input = bytearray(SEED.encode("utf-8"))
    start_t = time.time()
    res = fuzz(_input, None, None)
    print(res)
    end_t = time.time()
    print("use time is {}".format(end_t - start_t))
    # while True:
    #     res = fuzz(_input, None, None)
    #     _input = res
