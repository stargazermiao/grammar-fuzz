# AST operation

# Yihao Sun
# Syracuse 2020

class ASTNode:
    '''
    ASTNode is abstract of a gernal AST, each one contain id, range(start and end col/row
    of a term, [[startLine, startColoum] [endLine, endColoum]]), type(the name of non
    terminate term), and also it's children node
    '''

    def __init__(self, type, _id, startL, startC, endL, endC, children=[]):
        self.type = type
        self.id = _id
        self.startL = startL
        self.startC = startC
        self.endL = endL
        self.endC = endC
        self.children = children

    def toSexpr(self):
        sexprTemplete = "({} {} (({} {}) ({} {})) ({}))"
        childrenS = " ".join(map(str, self.children))
        return sexprTemplete.format(self.type, self.id, self.startL, self.startC, self.endL, self.endC, childrenS)

    def __str__(self):
        return self.toSexpr()
