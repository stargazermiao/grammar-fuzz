# compile into sexpr

import time
from ast import ASTNode

from antlr4 import *

from mrubysimpLexer import mrubysimpLexer
from mrubysimpParser import mrubysimpParser
from mrubysimpVisitor import mrubysimpVisitor


class SexprVisitor(mrubysimpVisitor):
    
    def __init__(self):
        self.counter = 0

    def createASTNode(self, type, ctx: ParserRuleContext):
        startL = ctx.start.line
        startC = ctx.start.column
        endL = ctx.stop.line
        endC = ctx.stop.column + len(ctx.stop.text)
        if ctx.start.tokenIndex > ctx.stop.tokenIndex:
            s, e = ctx.getSourceInterval()
            print(ctx.getText())
            print("start is {}, end is {}".format(s , e))
        children = self.visitChildrenContext(ctx)
        newNode = ASTNode(type, self.counter, startL,
                          startC, endL, endC, children)
        self.counter = self.counter + 1
        return newNode

    def visitChildrenContext(self, ctx: ParserRuleContext):
        children = []
        if ctx.children is None:
            return []
        non_emp_children = filter(lambda c : c is not None, ctx.children)
        for c in non_emp_children:
            data = self.visit(c)
            if data is None:
                pass
            else:
                children.append(data)
        return children

    # Visit a parse tree produced by mrubysimpParser#start.
    def visitStart(self, ctx: mrubysimpParser.StartContext):
        return self.createASTNode("start", ctx)

    # Visit a parse tree produced by mrubysimpParser#program.
    def visitProgram(self, ctx: mrubysimpParser.ProgramContext):
        return self.createASTNode("program", ctx)

    # Visit a parse tree produced by mrubysimpParser#statement.
    def visitStatement(self, ctx: mrubysimpParser.StatementContext):
        return self.createASTNode("statement", ctx)

    # Visit a parse tree produced by mrubysimpParser#var.
    def visitVar(self, ctx: mrubysimpParser.VarContext):
        return self.createASTNode("var", ctx)

    # Visit a parse tree produced by mrubysimpParser#ref.
    def visitRef(self, ctx:mrubysimpParser.RefContext):
        return  self.createASTNode("ref", ctx)

    # Visit a parse tree produced by mrubysimpParser#args.
    def visitArgs(self, ctx: mrubysimpParser.ArgsContext):
        return self.createASTNode("args", ctx)

    # Visit a parse tree produced by mrubysimpParser#val.
    def visitVal(self, ctx: mrubysimpParser.ValContext):
        return self.createASTNode("val", ctx)

    # Visit a parse tree produced by mrubysimpParser#identifier.
    def visitIdentifier(self, ctx: mrubysimpParser.IdentifierContext):
        return self.createASTNode("identifier", ctx)


# test
if __name__ == "__main__":
    start_t = time.time()
    fstream = FileStream("test.rb")
    lexer = mrubysimpLexer(fstream)
    tokenStream = CommonTokenStream(lexer)
    parser = mrubysimpParser(tokenStream)
    root = parser.start()
    visitor = SexprVisitor()
    res = visitor.visit(root)
    print(res)
    end_t = time.time()
    print("running time: {} \n".format(end_t - start_t))
