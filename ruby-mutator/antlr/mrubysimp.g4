
grammar mrubysimp;

start : START_HEAD program EOF;

program : statement+
        ;

statement : DEF var DOT identifier LP args? RP program?
          | var EQ var DOT identifier LP args? RP LCB MID args? MID program? RCB
          | var EQ identifier DOT identifier LP args? RP LCB MID args? MID program? RCB
          | var EQ var
          | var EQ val
          | RETURN var
          | BREAK var
          | RAISE var
          | YIELD var
          | CONTINUE var
          | NEXT var
          ;

var : NAME
    | ref
    | LP ref RP
    ;

ref : identifier DUO_COL identifier;

args : var
     | var COMMA args
     ;

val : '"' NAME '"'
    | NUM
    | 'nil'
    | 'true'
    | 'false'
    | '/foo/'
    | var DOT2 var
    | '[]'
    | '[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,nil]'
    | '{}'
    ;

identifier
: NAME
;


// none: ;

// lexer

// program headr
START_HEAD : 'a=0\nb="asdfasdfasdf adaf asdf asdfa sdf asdfasdfasdfa sdf"\nc={1=>1, 2=>"foo", "foo"=>nil, nil=> nil }\nd=[1,nil," sdfg"]\nsrand(1337)\n';

// RID : NAME;
 

NEWLINE : '\n';
DUO_COL : '::';
DOT : '.';
DEF : 'def';
LP : '(';
RP : ')';
EQ : '=';
LCB : '{';
RCB : '}';
MID : '|';
RETURN : 'return';
RAISE : 'raise';
YIELD : 'yield';
CONTINUE : 'continue';
BREAK : 'break';
NEXT : 'next';
COMMA : ',';
DOT2 : '..';

NUM : DIGIT+;
fragment DIGIT : [0-9];

NAME : [a-zA-Z_][a-zA-Z_0-9]*;

WS
    : [ \t\u000C\r\n]+ -> skip
    ;

ErrorChar
   : .
   ;