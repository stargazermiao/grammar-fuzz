// Generated from /home/stargazermiao/workspace/PL/afl++-grammar/ruby-mutator/antlr/mrubysimp.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class mrubysimpParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, START_HEAD=9, 
		NEWLINE=10, DUO_COL=11, DOT=12, DEF=13, LP=14, RP=15, EQ=16, LCB=17, RCB=18, 
		MID=19, RETURN=20, RAISE=21, YIELD=22, CONTINUE=23, BREAK=24, NEXT=25, 
		COMMA=26, DOT2=27, NUM=28, NAME=29, WS=30, ErrorChar=31;
	public static final int
		RULE_start = 0, RULE_program = 1, RULE_statement = 2, RULE_var = 3, RULE_ref = 4, 
		RULE_args = 5, RULE_val = 6, RULE_identifier = 7;
	private static String[] makeRuleNames() {
		return new String[] {
			"start", "program", "statement", "var", "ref", "args", "val", "identifier"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'\"'", "'nil'", "'true'", "'false'", "'/foo/'", "'[]'", "'[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,nil]'", 
			"'{}'", "'a=0\nb=\"asdfasdfasdf adaf asdf asdfa sdf asdfasdfasdfa sdf\"\nc={1=>1, 2=>\"foo\", \"foo\"=>nil, nil=> nil }\nd=[1,nil,\" sdfg\"]\nsrand(1337)\n'", 
			"'\n'", "'::'", "'.'", "'def'", "'('", "')'", "'='", "'{'", "'}'", "'|'", 
			"'return'", "'raise'", "'yield'", "'continue'", "'break'", "'next'", 
			"','", "'..'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "START_HEAD", "NEWLINE", 
			"DUO_COL", "DOT", "DEF", "LP", "RP", "EQ", "LCB", "RCB", "MID", "RETURN", 
			"RAISE", "YIELD", "CONTINUE", "BREAK", "NEXT", "COMMA", "DOT2", "NUM", 
			"NAME", "WS", "ErrorChar"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "mrubysimp.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public mrubysimpParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StartContext extends ParserRuleContext {
		public TerminalNode START_HEAD() { return getToken(mrubysimpParser.START_HEAD, 0); }
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public TerminalNode EOF() { return getToken(mrubysimpParser.EOF, 0); }
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(16);
			match(START_HEAD);
			setState(17);
			program();
			setState(18);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(21); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(20);
					statement();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(23); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public TerminalNode DEF() { return getToken(mrubysimpParser.DEF, 0); }
		public List<VarContext> var() {
			return getRuleContexts(VarContext.class);
		}
		public VarContext var(int i) {
			return getRuleContext(VarContext.class,i);
		}
		public TerminalNode DOT() { return getToken(mrubysimpParser.DOT, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode LP() { return getToken(mrubysimpParser.LP, 0); }
		public TerminalNode RP() { return getToken(mrubysimpParser.RP, 0); }
		public List<ArgsContext> args() {
			return getRuleContexts(ArgsContext.class);
		}
		public ArgsContext args(int i) {
			return getRuleContext(ArgsContext.class,i);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public TerminalNode EQ() { return getToken(mrubysimpParser.EQ, 0); }
		public TerminalNode LCB() { return getToken(mrubysimpParser.LCB, 0); }
		public List<TerminalNode> MID() { return getTokens(mrubysimpParser.MID); }
		public TerminalNode MID(int i) {
			return getToken(mrubysimpParser.MID, i);
		}
		public TerminalNode RCB() { return getToken(mrubysimpParser.RCB, 0); }
		public ValContext val() {
			return getRuleContext(ValContext.class,0);
		}
		public TerminalNode RETURN() { return getToken(mrubysimpParser.RETURN, 0); }
		public TerminalNode BREAK() { return getToken(mrubysimpParser.BREAK, 0); }
		public TerminalNode RAISE() { return getToken(mrubysimpParser.RAISE, 0); }
		public TerminalNode YIELD() { return getToken(mrubysimpParser.YIELD, 0); }
		public TerminalNode CONTINUE() { return getToken(mrubysimpParser.CONTINUE, 0); }
		public TerminalNode NEXT() { return getToken(mrubysimpParser.NEXT, 0); }
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_statement);
		int _la;
		try {
			setState(99);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(25);
				match(DEF);
				setState(26);
				var();
				setState(27);
				match(DOT);
				setState(28);
				identifier();
				setState(29);
				match(LP);
				setState(31);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LP || _la==NAME) {
					{
					setState(30);
					args();
					}
				}

				setState(33);
				match(RP);
				setState(35);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
				case 1:
					{
					setState(34);
					program();
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(37);
				var();
				setState(38);
				match(EQ);
				setState(39);
				var();
				setState(40);
				match(DOT);
				setState(41);
				identifier();
				setState(42);
				match(LP);
				setState(44);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LP || _la==NAME) {
					{
					setState(43);
					args();
					}
				}

				setState(46);
				match(RP);
				setState(47);
				match(LCB);
				setState(48);
				match(MID);
				setState(50);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LP || _la==NAME) {
					{
					setState(49);
					args();
					}
				}

				setState(52);
				match(MID);
				setState(54);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DEF) | (1L << LP) | (1L << RETURN) | (1L << RAISE) | (1L << YIELD) | (1L << CONTINUE) | (1L << BREAK) | (1L << NEXT) | (1L << NAME))) != 0)) {
					{
					setState(53);
					program();
					}
				}

				setState(56);
				match(RCB);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(58);
				var();
				setState(59);
				match(EQ);
				setState(60);
				identifier();
				setState(61);
				match(DOT);
				setState(62);
				identifier();
				setState(63);
				match(LP);
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LP || _la==NAME) {
					{
					setState(64);
					args();
					}
				}

				setState(67);
				match(RP);
				setState(68);
				match(LCB);
				setState(69);
				match(MID);
				setState(71);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LP || _la==NAME) {
					{
					setState(70);
					args();
					}
				}

				setState(73);
				match(MID);
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DEF) | (1L << LP) | (1L << RETURN) | (1L << RAISE) | (1L << YIELD) | (1L << CONTINUE) | (1L << BREAK) | (1L << NEXT) | (1L << NAME))) != 0)) {
					{
					setState(74);
					program();
					}
				}

				setState(77);
				match(RCB);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(79);
				var();
				setState(80);
				match(EQ);
				setState(81);
				var();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(83);
				var();
				setState(84);
				match(EQ);
				setState(85);
				val();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(87);
				match(RETURN);
				setState(88);
				var();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(89);
				match(BREAK);
				setState(90);
				var();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(91);
				match(RAISE);
				setState(92);
				var();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(93);
				match(YIELD);
				setState(94);
				var();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(95);
				match(CONTINUE);
				setState(96);
				var();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(97);
				match(NEXT);
				setState(98);
				var();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(mrubysimpParser.NAME, 0); }
		public RefContext ref() {
			return getRuleContext(RefContext.class,0);
		}
		public TerminalNode LP() { return getToken(mrubysimpParser.LP, 0); }
		public TerminalNode RP() { return getToken(mrubysimpParser.RP, 0); }
		public VarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var; }
	}

	public final VarContext var() throws RecognitionException {
		VarContext _localctx = new VarContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_var);
		try {
			setState(107);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(101);
				match(NAME);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(102);
				ref();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(103);
				match(LP);
				setState(104);
				ref();
				setState(105);
				match(RP);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RefContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode DUO_COL() { return getToken(mrubysimpParser.DUO_COL, 0); }
		public RefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ref; }
	}

	public final RefContext ref() throws RecognitionException {
		RefContext _localctx = new RefContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_ref);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			identifier();
			setState(110);
			match(DUO_COL);
			setState(111);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(mrubysimpParser.COMMA, 0); }
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_args);
		try {
			setState(118);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(113);
				var();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(114);
				var();
				setState(115);
				match(COMMA);
				setState(116);
				args();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(mrubysimpParser.NAME, 0); }
		public TerminalNode NUM() { return getToken(mrubysimpParser.NUM, 0); }
		public List<VarContext> var() {
			return getRuleContexts(VarContext.class);
		}
		public VarContext var(int i) {
			return getRuleContext(VarContext.class,i);
		}
		public TerminalNode DOT2() { return getToken(mrubysimpParser.DOT2, 0); }
		public ValContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_val; }
	}

	public final ValContext val() throws RecognitionException {
		ValContext _localctx = new ValContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_val);
		try {
			setState(135);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				enterOuterAlt(_localctx, 1);
				{
				setState(120);
				match(T__0);
				setState(121);
				match(NAME);
				setState(122);
				match(T__0);
				}
				break;
			case NUM:
				enterOuterAlt(_localctx, 2);
				{
				setState(123);
				match(NUM);
				}
				break;
			case T__1:
				enterOuterAlt(_localctx, 3);
				{
				setState(124);
				match(T__1);
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 4);
				{
				setState(125);
				match(T__2);
				}
				break;
			case T__3:
				enterOuterAlt(_localctx, 5);
				{
				setState(126);
				match(T__3);
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 6);
				{
				setState(127);
				match(T__4);
				}
				break;
			case LP:
			case NAME:
				enterOuterAlt(_localctx, 7);
				{
				setState(128);
				var();
				setState(129);
				match(DOT2);
				setState(130);
				var();
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 8);
				{
				setState(132);
				match(T__5);
				}
				break;
			case T__6:
				enterOuterAlt(_localctx, 9);
				{
				setState(133);
				match(T__6);
				}
				break;
			case T__7:
				enterOuterAlt(_localctx, 10);
				{
				setState(134);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(mrubysimpParser.NAME, 0); }
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_identifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3!\u008e\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\2"+
		"\3\3\6\3\30\n\3\r\3\16\3\31\3\4\3\4\3\4\3\4\3\4\3\4\5\4\"\n\4\3\4\3\4"+
		"\5\4&\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4/\n\4\3\4\3\4\3\4\3\4\5\4\65"+
		"\n\4\3\4\3\4\5\49\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4D\n\4\3\4"+
		"\3\4\3\4\3\4\5\4J\n\4\3\4\3\4\5\4N\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4f\n\4\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\5\5n\n\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\5\7"+
		"y\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b"+
		"\u008a\n\b\3\t\3\t\3\t\2\2\n\2\4\6\b\n\f\16\20\2\2\2\u00a4\2\22\3\2\2"+
		"\2\4\27\3\2\2\2\6e\3\2\2\2\bm\3\2\2\2\no\3\2\2\2\fx\3\2\2\2\16\u0089\3"+
		"\2\2\2\20\u008b\3\2\2\2\22\23\7\13\2\2\23\24\5\4\3\2\24\25\7\2\2\3\25"+
		"\3\3\2\2\2\26\30\5\6\4\2\27\26\3\2\2\2\30\31\3\2\2\2\31\27\3\2\2\2\31"+
		"\32\3\2\2\2\32\5\3\2\2\2\33\34\7\17\2\2\34\35\5\b\5\2\35\36\7\16\2\2\36"+
		"\37\5\20\t\2\37!\7\20\2\2 \"\5\f\7\2! \3\2\2\2!\"\3\2\2\2\"#\3\2\2\2#"+
		"%\7\21\2\2$&\5\4\3\2%$\3\2\2\2%&\3\2\2\2&f\3\2\2\2\'(\5\b\5\2()\7\22\2"+
		"\2)*\5\b\5\2*+\7\16\2\2+,\5\20\t\2,.\7\20\2\2-/\5\f\7\2.-\3\2\2\2./\3"+
		"\2\2\2/\60\3\2\2\2\60\61\7\21\2\2\61\62\7\23\2\2\62\64\7\25\2\2\63\65"+
		"\5\f\7\2\64\63\3\2\2\2\64\65\3\2\2\2\65\66\3\2\2\2\668\7\25\2\2\679\5"+
		"\4\3\28\67\3\2\2\289\3\2\2\29:\3\2\2\2:;\7\24\2\2;f\3\2\2\2<=\5\b\5\2"+
		"=>\7\22\2\2>?\5\20\t\2?@\7\16\2\2@A\5\20\t\2AC\7\20\2\2BD\5\f\7\2CB\3"+
		"\2\2\2CD\3\2\2\2DE\3\2\2\2EF\7\21\2\2FG\7\23\2\2GI\7\25\2\2HJ\5\f\7\2"+
		"IH\3\2\2\2IJ\3\2\2\2JK\3\2\2\2KM\7\25\2\2LN\5\4\3\2ML\3\2\2\2MN\3\2\2"+
		"\2NO\3\2\2\2OP\7\24\2\2Pf\3\2\2\2QR\5\b\5\2RS\7\22\2\2ST\5\b\5\2Tf\3\2"+
		"\2\2UV\5\b\5\2VW\7\22\2\2WX\5\16\b\2Xf\3\2\2\2YZ\7\26\2\2Zf\5\b\5\2[\\"+
		"\7\32\2\2\\f\5\b\5\2]^\7\27\2\2^f\5\b\5\2_`\7\30\2\2`f\5\b\5\2ab\7\31"+
		"\2\2bf\5\b\5\2cd\7\33\2\2df\5\b\5\2e\33\3\2\2\2e\'\3\2\2\2e<\3\2\2\2e"+
		"Q\3\2\2\2eU\3\2\2\2eY\3\2\2\2e[\3\2\2\2e]\3\2\2\2e_\3\2\2\2ea\3\2\2\2"+
		"ec\3\2\2\2f\7\3\2\2\2gn\7\37\2\2hn\5\n\6\2ij\7\20\2\2jk\5\n\6\2kl\7\21"+
		"\2\2ln\3\2\2\2mg\3\2\2\2mh\3\2\2\2mi\3\2\2\2n\t\3\2\2\2op\5\20\t\2pq\7"+
		"\r\2\2qr\5\20\t\2r\13\3\2\2\2sy\5\b\5\2tu\5\b\5\2uv\7\34\2\2vw\5\f\7\2"+
		"wy\3\2\2\2xs\3\2\2\2xt\3\2\2\2y\r\3\2\2\2z{\7\3\2\2{|\7\37\2\2|\u008a"+
		"\7\3\2\2}\u008a\7\36\2\2~\u008a\7\4\2\2\177\u008a\7\5\2\2\u0080\u008a"+
		"\7\6\2\2\u0081\u008a\7\7\2\2\u0082\u0083\5\b\5\2\u0083\u0084\7\35\2\2"+
		"\u0084\u0085\5\b\5\2\u0085\u008a\3\2\2\2\u0086\u008a\7\b\2\2\u0087\u008a"+
		"\7\t\2\2\u0088\u008a\7\n\2\2\u0089z\3\2\2\2\u0089}\3\2\2\2\u0089~\3\2"+
		"\2\2\u0089\177\3\2\2\2\u0089\u0080\3\2\2\2\u0089\u0081\3\2\2\2\u0089\u0082"+
		"\3\2\2\2\u0089\u0086\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u0088\3\2\2\2\u008a"+
		"\17\3\2\2\2\u008b\u008c\7\37\2\2\u008c\21\3\2\2\2\17\31!%.\648CIMemx\u0089";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}