// Generated from /home/stargazermiao/workspace/PL/afl++-grammar/ruby-mutator/antlr/RubyParser.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RubyParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		ALIAS=1, BEGIN=2, BEGIN_BLOCK=3, BREAK=4, CASE=5, CLASS=6, DEF=7, DEFINED=8, 
		DO=9, ELSE=10, ELSIF=11, END_BLOCK=12, END=13, ENSURE=14, FALSE=15, FOR=16, 
		IF=17, IN=18, MODULE=19, NEXT=20, NIL=21, NOT=22, RAISE=23, REDO=24, RESCUE=25, 
		RETRY=26, RETURN=27, SELF=28, SUPER=29, THEN=30, TRUE=31, UNDEF=32, UNLESS=33, 
		UNTIL=34, WHEN=35, WHILE=36, YIELD=37, Integer=38, Float=39, Regex=40, 
		String=41, DollarSpecial=42, CRLF=43, AND=44, OR=45, PLUS=46, MINUS=47, 
		MOD=48, PLUS_ASSIGN=49, MOD_ASSIGN=50, SHARP=51, AND_ASSIGN=52, BIT_AND_ASSIGN=53, 
		ANDDOT=54, BIT_AND=55, EXP_ASSIGN=56, EXP=57, MUL=58, MUL_ASSIGN=59, DOLLAR=60, 
		AT=61, RIGHT_PAREN=62, LEFT_PAREN=63, RIGHT_SBRACKET=64, LEFT_SBRACKET=65, 
		RIGHT_RBRACKET=66, LEFT_RBRACKET=67, DOT3=68, DOT2=69, DOT=70, DIV_ASSIGN=71, 
		DIV=72, QUESTION=73, NOT_EQUAL=74, SIGH=75, COLON=76, COLON2=77, SEMICOLON=78, 
		COMMA=79, MINUS_ASSIGN=80, OR_ASSIGN=81, BIT_OR_ASSIGN=82, BIT_OR=83, 
		BIT_SHR_ASSIGN=84, GREATER_EQUAL=85, BIT_SHR=86, GREATER=87, ASSIGN=88, 
		ASSOC=89, EQUAL3=90, EQUAL=91, PATTERN_MATCH=92, LESS_EQUAL=93, LESS=94, 
		BIT_SHL_ASSIGN=95, BIT_SHL=96, BIT_NOT_ASSIGN=97, BIT_NOT=98, BIT_XOR_ASSIGN=99, 
		BIT_XOR=100, SL_COMMENT=101, ML_COMMENT=102, WS=103, Identifier=104, ShellCommand=105;
	public static final int
		RULE_compilation_unit = 0, RULE_statement_list_terms = 1, RULE_opt_statement_list_terms = 2, 
		RULE_statement_list_noterms = 3, RULE_opt_statement_terms = 4, RULE_statement = 5, 
		RULE_undef_statement = 6, RULE_opt_function_name_or_symbol = 7, RULE_alias_statement = 8, 
		RULE_function_name_or_symbol = 9, RULE_begin_block = 10, RULE_end_block = 11, 
		RULE_module_definition = 12, RULE_class_definition = 13, RULE_superclass = 14, 
		RULE_class_header = 15, RULE_function_definition = 16, RULE_function_definition_header = 17, 
		RULE_opt_function_definition_params = 18, RULE_opt_expr = 19, RULE_function_name = 20, 
		RULE_opt_cpath_func_suffix = 21, RULE_function_definition_params = 22, 
		RULE_func_def_body = 23, RULE_opt_function_definition_param_list = 24, 
		RULE_function_definition_param = 25, RULE_function_call_param = 26, RULE_expr = 27, 
		RULE_math_or_bit_op = 28, RULE_not_or_bitnot = 29, RULE_cpath_expr_suffix = 30, 
		RULE_expr_statement_suffix = 31, RULE_opt_expr_statement_suffix = 32, 
		RULE_primary = 33, RULE_comma_hash_asssos = 34, RULE_comma_exprs = 35, 
		RULE_dot2_or_dot3 = 36, RULE_opt_else_if = 37, RULE_opt_else = 38, RULE_func_call_parameters = 39, 
		RULE_func_call_parameters_no_bracket = 40, RULE_function_call_param_rest = 41, 
		RULE_rescure_param = 42, RULE_opt_rescure_param = 43, RULE_case_body = 44, 
		RULE_when_cond_rest = 45, RULE_case_bodys = 46, RULE_opt_case_bodys = 47, 
		RULE_when_cond = 48, RULE_if_tail = 49, RULE_if_tails = 50, RULE_else_tail = 51, 
		RULE_opt_else_tail = 52, RULE_dot_ref = 53, RULE_logicalOperator = 54, 
		RULE_equalsOperator = 55, RULE_compareOperator = 56, RULE_bitOperator = 57, 
		RULE_mathOperator = 58, RULE_assignOperator = 59, RULE_notSym = 60, RULE_block = 61, 
		RULE_block_stmt_list = 62, RULE_block_params = 63, RULE_opt_block_params = 64, 
		RULE_expr_block_params_list = 65, RULE_id_symbol = 66, RULE_symbol = 67, 
		RULE_hash_asso = 68, RULE_variable_path = 69, RULE_cpath = 70, RULE_literal = 71, 
		RULE_id_or_literal = 72, RULE_cpath_suffix = 73, RULE_cpath_suffixs = 74, 
		RULE_identifier = 75, RULE_empty = 76, RULE_globalVar = 77, RULE_classVar = 78, 
		RULE_instanceVar = 79, RULE_idArg = 80, RULE_do_keyword = 81, RULE_then_keyword = 82, 
		RULE_string = 83, RULE_crlf = 84, RULE_crlfs = 85, RULE_opt_crlfs = 86, 
		RULE_opt_comma = 87, RULE_opt_mul = 88, RULE_terms = 89, RULE_term = 90, 
		RULE_opt_terms = 91, RULE_none = 92;
	private static String[] makeRuleNames() {
		return new String[] {
			"compilation_unit", "statement_list_terms", "opt_statement_list_terms", 
			"statement_list_noterms", "opt_statement_terms", "statement", "undef_statement", 
			"opt_function_name_or_symbol", "alias_statement", "function_name_or_symbol", 
			"begin_block", "end_block", "module_definition", "class_definition", 
			"superclass", "class_header", "function_definition", "function_definition_header", 
			"opt_function_definition_params", "opt_expr", "function_name", "opt_cpath_func_suffix", 
			"function_definition_params", "func_def_body", "opt_function_definition_param_list", 
			"function_definition_param", "function_call_param", "expr", "math_or_bit_op", 
			"not_or_bitnot", "cpath_expr_suffix", "expr_statement_suffix", "opt_expr_statement_suffix", 
			"primary", "comma_hash_asssos", "comma_exprs", "dot2_or_dot3", "opt_else_if", 
			"opt_else", "func_call_parameters", "func_call_parameters_no_bracket", 
			"function_call_param_rest", "rescure_param", "opt_rescure_param", "case_body", 
			"when_cond_rest", "case_bodys", "opt_case_bodys", "when_cond", "if_tail", 
			"if_tails", "else_tail", "opt_else_tail", "dot_ref", "logicalOperator", 
			"equalsOperator", "compareOperator", "bitOperator", "mathOperator", "assignOperator", 
			"notSym", "block", "block_stmt_list", "block_params", "opt_block_params", 
			"expr_block_params_list", "id_symbol", "symbol", "hash_asso", "variable_path", 
			"cpath", "literal", "id_or_literal", "cpath_suffix", "cpath_suffixs", 
			"identifier", "empty", "globalVar", "classVar", "instanceVar", "idArg", 
			"do_keyword", "then_keyword", "string", "crlf", "crlfs", "opt_crlfs", 
			"opt_comma", "opt_mul", "terms", "term", "opt_terms", "none"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'alias'", "'begin'", "'BEGIN'", "'break'", "'case'", "'class'", 
			"'def'", "'defined?'", "'do'", "'else'", "'elsif'", "'END'", "'end'", 
			"'ensure'", "'false'", "'for'", "'if'", "'in'", "'module'", "'next'", 
			"'nil'", "'not'", "'raise'", "'redo'", "'rescue'", "'retry'", "'return'", 
			"'self'", "'super'", "'then'", "'true'", "'undef'", "'unless'", "'until'", 
			"'when'", "'while'", "'yield'", null, null, null, null, null, null, null, 
			null, "'+'", "'-'", "'%'", "'+='", "'%='", "'#'", "'&&='", "'&='", "'&.'", 
			"'&'", "'**='", "'**'", "'*'", "'*='", "'$'", "'@'", "'}'", "'{'", "']'", 
			"'['", "')'", "'('", "'...'", "'..'", "'.'", "'/='", "'/'", "'?'", "'!='", 
			"'!'", "':'", "'::'", "';'", "','", "'-='", "'||='", "'|='", "'|'", "'>>='", 
			"'>='", "'>>'", "'>'", "'='", "'=>'", "'==='", "'=='", "'=~'", "'<='", 
			"'<'", "'<<='", "'<<'", "'~='", "'~'", "'^='", "'^'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "ALIAS", "BEGIN", "BEGIN_BLOCK", "BREAK", "CASE", "CLASS", "DEF", 
			"DEFINED", "DO", "ELSE", "ELSIF", "END_BLOCK", "END", "ENSURE", "FALSE", 
			"FOR", "IF", "IN", "MODULE", "NEXT", "NIL", "NOT", "RAISE", "REDO", "RESCUE", 
			"RETRY", "RETURN", "SELF", "SUPER", "THEN", "TRUE", "UNDEF", "UNLESS", 
			"UNTIL", "WHEN", "WHILE", "YIELD", "Integer", "Float", "Regex", "String", 
			"DollarSpecial", "CRLF", "AND", "OR", "PLUS", "MINUS", "MOD", "PLUS_ASSIGN", 
			"MOD_ASSIGN", "SHARP", "AND_ASSIGN", "BIT_AND_ASSIGN", "ANDDOT", "BIT_AND", 
			"EXP_ASSIGN", "EXP", "MUL", "MUL_ASSIGN", "DOLLAR", "AT", "RIGHT_PAREN", 
			"LEFT_PAREN", "RIGHT_SBRACKET", "LEFT_SBRACKET", "RIGHT_RBRACKET", "LEFT_RBRACKET", 
			"DOT3", "DOT2", "DOT", "DIV_ASSIGN", "DIV", "QUESTION", "NOT_EQUAL", 
			"SIGH", "COLON", "COLON2", "SEMICOLON", "COMMA", "MINUS_ASSIGN", "OR_ASSIGN", 
			"BIT_OR_ASSIGN", "BIT_OR", "BIT_SHR_ASSIGN", "GREATER_EQUAL", "BIT_SHR", 
			"GREATER", "ASSIGN", "ASSOC", "EQUAL3", "EQUAL", "PATTERN_MATCH", "LESS_EQUAL", 
			"LESS", "BIT_SHL_ASSIGN", "BIT_SHL", "BIT_NOT_ASSIGN", "BIT_NOT", "BIT_XOR_ASSIGN", 
			"BIT_XOR", "SL_COMMENT", "ML_COMMENT", "WS", "Identifier", "ShellCommand"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "RubyParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public RubyParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class Compilation_unitContext extends ParserRuleContext {
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public Compilation_unitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compilation_unit; }
	}

	public final Compilation_unitContext compilation_unit() throws RecognitionException {
		Compilation_unitContext _localctx = new Compilation_unitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_compilation_unit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			statement_list_terms(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement_list_termsContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public Statement_list_termsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement_list_terms; }
	}

	public final Statement_list_termsContext statement_list_terms() throws RecognitionException {
		return statement_list_terms(0);
	}

	private Statement_list_termsContext statement_list_terms(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Statement_list_termsContext _localctx = new Statement_list_termsContext(_ctx, _parentState);
		Statement_list_termsContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_statement_list_terms, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(193);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ALIAS:
			case BEGIN:
			case BEGIN_BLOCK:
			case BREAK:
			case CASE:
			case CLASS:
			case DEF:
			case DEFINED:
			case END_BLOCK:
			case ENSURE:
			case FALSE:
			case FOR:
			case IF:
			case MODULE:
			case NEXT:
			case NIL:
			case NOT:
			case RAISE:
			case REDO:
			case RESCUE:
			case RETRY:
			case RETURN:
			case SELF:
			case SUPER:
			case TRUE:
			case UNDEF:
			case UNLESS:
			case UNTIL:
			case WHEN:
			case WHILE:
			case YIELD:
			case Integer:
			case Float:
			case Regex:
			case String:
			case DollarSpecial:
			case AND:
			case OR:
			case PLUS:
			case MINUS:
			case MOD:
			case PLUS_ASSIGN:
			case MOD_ASSIGN:
			case AND_ASSIGN:
			case BIT_AND_ASSIGN:
			case BIT_AND:
			case EXP_ASSIGN:
			case EXP:
			case MUL:
			case MUL_ASSIGN:
			case DOLLAR:
			case AT:
			case LEFT_PAREN:
			case LEFT_SBRACKET:
			case LEFT_RBRACKET:
			case DIV_ASSIGN:
			case DIV:
			case NOT_EQUAL:
			case SIGH:
			case COLON:
			case COLON2:
			case MINUS_ASSIGN:
			case OR_ASSIGN:
			case BIT_OR_ASSIGN:
			case BIT_OR:
			case BIT_SHR_ASSIGN:
			case GREATER_EQUAL:
			case BIT_SHR:
			case GREATER:
			case ASSIGN:
			case EQUAL3:
			case EQUAL:
			case LESS_EQUAL:
			case LESS:
			case BIT_SHL_ASSIGN:
			case BIT_SHL:
			case BIT_NOT_ASSIGN:
			case BIT_NOT:
			case BIT_XOR_ASSIGN:
			case BIT_XOR:
			case Identifier:
			case ShellCommand:
				{
				setState(189);
				statement();
				setState(190);
				terms();
				}
				break;
			case CRLF:
			case SEMICOLON:
			case SL_COMMENT:
				{
				setState(192);
				terms();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(201);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Statement_list_termsContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_statement_list_terms);
					setState(195);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(196);
					statement();
					setState(197);
					terms();
					}
					} 
				}
				setState(203);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Opt_statement_list_termsContext extends ParserRuleContext {
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_statement_list_termsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_statement_list_terms; }
	}

	public final Opt_statement_list_termsContext opt_statement_list_terms() throws RecognitionException {
		Opt_statement_list_termsContext _localctx = new Opt_statement_list_termsContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_opt_statement_list_terms);
		try {
			setState(206);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(204);
				statement_list_terms(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(205);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement_list_notermsContext extends ParserRuleContext {
		public Opt_statement_termsContext opt_statement_terms() {
			return getRuleContext(Opt_statement_termsContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Statement_list_notermsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement_list_noterms; }
	}

	public final Statement_list_notermsContext statement_list_noterms() throws RecognitionException {
		Statement_list_notermsContext _localctx = new Statement_list_notermsContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_statement_list_noterms);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(208);
			opt_statement_terms();
			setState(209);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_statement_termsContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public Opt_statement_termsContext opt_statement_terms() {
			return getRuleContext(Opt_statement_termsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_statement_termsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_statement_terms; }
	}

	public final Opt_statement_termsContext opt_statement_terms() throws RecognitionException {
		Opt_statement_termsContext _localctx = new Opt_statement_termsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_opt_statement_terms);
		try {
			setState(219);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(211);
				statement();
				setState(212);
				terms();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(214);
				statement();
				setState(215);
				terms();
				setState(216);
				opt_statement_terms();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(218);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Begin_blockContext begin_block() {
			return getRuleContext(Begin_blockContext.class,0);
		}
		public End_blockContext end_block() {
			return getRuleContext(End_blockContext.class,0);
		}
		public Alias_statementContext alias_statement() {
			return getRuleContext(Alias_statementContext.class,0);
		}
		public Undef_statementContext undef_statement() {
			return getRuleContext(Undef_statementContext.class,0);
		}
		public TerminalNode ENSURE() { return getToken(RubyParser.ENSURE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_statement);
		try {
			setState(227);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BEGIN_BLOCK:
				enterOuterAlt(_localctx, 1);
				{
				setState(221);
				begin_block();
				}
				break;
			case END_BLOCK:
				enterOuterAlt(_localctx, 2);
				{
				setState(222);
				end_block();
				}
				break;
			case ALIAS:
				enterOuterAlt(_localctx, 3);
				{
				setState(223);
				alias_statement();
				}
				break;
			case UNDEF:
				enterOuterAlt(_localctx, 4);
				{
				setState(224);
				undef_statement();
				}
				break;
			case ENSURE:
				enterOuterAlt(_localctx, 5);
				{
				setState(225);
				match(ENSURE);
				}
				break;
			case BEGIN:
			case BREAK:
			case CASE:
			case CLASS:
			case DEF:
			case DEFINED:
			case FALSE:
			case FOR:
			case IF:
			case MODULE:
			case NEXT:
			case NIL:
			case NOT:
			case RAISE:
			case REDO:
			case RESCUE:
			case RETRY:
			case RETURN:
			case SELF:
			case SUPER:
			case TRUE:
			case UNLESS:
			case UNTIL:
			case WHEN:
			case WHILE:
			case YIELD:
			case Integer:
			case Float:
			case Regex:
			case String:
			case DollarSpecial:
			case AND:
			case OR:
			case PLUS:
			case MINUS:
			case MOD:
			case PLUS_ASSIGN:
			case MOD_ASSIGN:
			case AND_ASSIGN:
			case BIT_AND_ASSIGN:
			case BIT_AND:
			case EXP_ASSIGN:
			case EXP:
			case MUL:
			case MUL_ASSIGN:
			case DOLLAR:
			case AT:
			case LEFT_PAREN:
			case LEFT_SBRACKET:
			case LEFT_RBRACKET:
			case DIV_ASSIGN:
			case DIV:
			case NOT_EQUAL:
			case SIGH:
			case COLON:
			case COLON2:
			case MINUS_ASSIGN:
			case OR_ASSIGN:
			case BIT_OR_ASSIGN:
			case BIT_OR:
			case BIT_SHR_ASSIGN:
			case GREATER_EQUAL:
			case BIT_SHR:
			case GREATER:
			case ASSIGN:
			case EQUAL3:
			case EQUAL:
			case LESS_EQUAL:
			case LESS:
			case BIT_SHL_ASSIGN:
			case BIT_SHL:
			case BIT_NOT_ASSIGN:
			case BIT_NOT:
			case BIT_XOR_ASSIGN:
			case BIT_XOR:
			case Identifier:
			case ShellCommand:
				enterOuterAlt(_localctx, 6);
				{
				setState(226);
				expr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Undef_statementContext extends ParserRuleContext {
		public TerminalNode UNDEF() { return getToken(RubyParser.UNDEF, 0); }
		public Function_name_or_symbolContext function_name_or_symbol() {
			return getRuleContext(Function_name_or_symbolContext.class,0);
		}
		public Opt_function_name_or_symbolContext opt_function_name_or_symbol() {
			return getRuleContext(Opt_function_name_or_symbolContext.class,0);
		}
		public Undef_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_undef_statement; }
	}

	public final Undef_statementContext undef_statement() throws RecognitionException {
		Undef_statementContext _localctx = new Undef_statementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_undef_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			match(UNDEF);
			setState(230);
			function_name_or_symbol();
			setState(231);
			opt_function_name_or_symbol();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_function_name_or_symbolContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public Function_name_or_symbolContext function_name_or_symbol() {
			return getRuleContext(Function_name_or_symbolContext.class,0);
		}
		public Opt_function_name_or_symbolContext opt_function_name_or_symbol() {
			return getRuleContext(Opt_function_name_or_symbolContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_function_name_or_symbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_function_name_or_symbol; }
	}

	public final Opt_function_name_or_symbolContext opt_function_name_or_symbol() throws RecognitionException {
		Opt_function_name_or_symbolContext _localctx = new Opt_function_name_or_symbolContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_opt_function_name_or_symbol);
		try {
			setState(240);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(233);
				match(COMMA);
				setState(234);
				function_name_or_symbol();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(235);
				match(COMMA);
				setState(236);
				function_name_or_symbol();
				setState(237);
				opt_function_name_or_symbol();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(239);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alias_statementContext extends ParserRuleContext {
		public TerminalNode ALIAS() { return getToken(RubyParser.ALIAS, 0); }
		public List<Function_name_or_symbolContext> function_name_or_symbol() {
			return getRuleContexts(Function_name_or_symbolContext.class);
		}
		public Function_name_or_symbolContext function_name_or_symbol(int i) {
			return getRuleContext(Function_name_or_symbolContext.class,i);
		}
		public Alias_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alias_statement; }
	}

	public final Alias_statementContext alias_statement() throws RecognitionException {
		Alias_statementContext _localctx = new Alias_statementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_alias_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(242);
			match(ALIAS);
			setState(243);
			function_name_or_symbol();
			setState(244);
			function_name_or_symbol();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_name_or_symbolContext extends ParserRuleContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public SymbolContext symbol() {
			return getRuleContext(SymbolContext.class,0);
		}
		public Function_name_or_symbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_name_or_symbol; }
	}

	public final Function_name_or_symbolContext function_name_or_symbol() throws RecognitionException {
		Function_name_or_symbolContext _localctx = new Function_name_or_symbolContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_function_name_or_symbol);
		try {
			setState(248);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BREAK:
			case FALSE:
			case NEXT:
			case NIL:
			case REDO:
			case RETRY:
			case SELF:
			case SUPER:
			case TRUE:
			case Integer:
			case Float:
			case String:
			case DollarSpecial:
			case AND:
			case OR:
			case PLUS:
			case MINUS:
			case MOD:
			case PLUS_ASSIGN:
			case MOD_ASSIGN:
			case AND_ASSIGN:
			case BIT_AND_ASSIGN:
			case BIT_AND:
			case EXP_ASSIGN:
			case EXP:
			case MUL:
			case MUL_ASSIGN:
			case DOLLAR:
			case AT:
			case LEFT_PAREN:
			case LEFT_SBRACKET:
			case DIV_ASSIGN:
			case DIV:
			case NOT_EQUAL:
			case MINUS_ASSIGN:
			case OR_ASSIGN:
			case BIT_OR_ASSIGN:
			case BIT_OR:
			case BIT_SHR_ASSIGN:
			case GREATER_EQUAL:
			case BIT_SHR:
			case GREATER:
			case ASSIGN:
			case EQUAL3:
			case EQUAL:
			case LESS_EQUAL:
			case LESS:
			case BIT_SHL_ASSIGN:
			case BIT_SHL:
			case BIT_NOT_ASSIGN:
			case BIT_XOR_ASSIGN:
			case BIT_XOR:
			case Identifier:
			case ShellCommand:
				enterOuterAlt(_localctx, 1);
				{
				setState(246);
				function_name();
				}
				break;
			case COLON:
				enterOuterAlt(_localctx, 2);
				{
				setState(247);
				symbol();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Begin_blockContext extends ParserRuleContext {
		public TerminalNode BEGIN_BLOCK() { return getToken(RubyParser.BEGIN_BLOCK, 0); }
		public TerminalNode LEFT_PAREN() { return getToken(RubyParser.LEFT_PAREN, 0); }
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public TerminalNode RIGHT_PAREN() { return getToken(RubyParser.RIGHT_PAREN, 0); }
		public Begin_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_begin_block; }
	}

	public final Begin_blockContext begin_block() throws RecognitionException {
		Begin_blockContext _localctx = new Begin_blockContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_begin_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(250);
			match(BEGIN_BLOCK);
			setState(251);
			match(LEFT_PAREN);
			setState(252);
			statement_list_terms(0);
			setState(253);
			match(RIGHT_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class End_blockContext extends ParserRuleContext {
		public TerminalNode END_BLOCK() { return getToken(RubyParser.END_BLOCK, 0); }
		public TerminalNode LEFT_PAREN() { return getToken(RubyParser.LEFT_PAREN, 0); }
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public TerminalNode RIGHT_PAREN() { return getToken(RubyParser.RIGHT_PAREN, 0); }
		public End_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_end_block; }
	}

	public final End_blockContext end_block() throws RecognitionException {
		End_blockContext _localctx = new End_blockContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_end_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(255);
			match(END_BLOCK);
			setState(256);
			match(LEFT_PAREN);
			setState(257);
			statement_list_terms(0);
			setState(258);
			match(RIGHT_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Module_definitionContext extends ParserRuleContext {
		public TerminalNode MODULE() { return getToken(RubyParser.MODULE, 0); }
		public CpathContext cpath() {
			return getRuleContext(CpathContext.class,0);
		}
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public Module_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_module_definition; }
	}

	public final Module_definitionContext module_definition() throws RecognitionException {
		Module_definitionContext _localctx = new Module_definitionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_module_definition);
		try {
			setState(269);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(260);
				match(MODULE);
				setState(261);
				cpath();
				setState(262);
				statement_list_terms(0);
				setState(263);
				match(END);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(265);
				match(MODULE);
				setState(266);
				cpath();
				setState(267);
				match(END);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_definitionContext extends ParserRuleContext {
		public Class_headerContext class_header() {
			return getRuleContext(Class_headerContext.class,0);
		}
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public Class_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_definition; }
	}

	public final Class_definitionContext class_definition() throws RecognitionException {
		Class_definitionContext _localctx = new Class_definitionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_class_definition);
		try {
			setState(278);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(271);
				class_header();
				setState(272);
				statement_list_terms(0);
				setState(273);
				match(END);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(275);
				class_header();
				setState(276);
				match(END);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SuperclassContext extends ParserRuleContext {
		public TerminalNode LESS() { return getToken(RubyParser.LESS, 0); }
		public Id_symbolContext id_symbol() {
			return getRuleContext(Id_symbolContext.class,0);
		}
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public SuperclassContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_superclass; }
	}

	public final SuperclassContext superclass() throws RecognitionException {
		SuperclassContext _localctx = new SuperclassContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_superclass);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(280);
			match(LESS);
			setState(281);
			id_symbol();
			setState(282);
			terms();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_headerContext extends ParserRuleContext {
		public TerminalNode CLASS() { return getToken(RubyParser.CLASS, 0); }
		public CpathContext cpath() {
			return getRuleContext(CpathContext.class,0);
		}
		public SuperclassContext superclass() {
			return getRuleContext(SuperclassContext.class,0);
		}
		public TerminalNode BIT_SHL() { return getToken(RubyParser.BIT_SHL, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public Class_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_header; }
	}

	public final Class_headerContext class_header() throws RecognitionException {
		Class_headerContext _localctx = new Class_headerContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_class_header);
		try {
			setState(293);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(284);
				match(CLASS);
				setState(285);
				cpath();
				setState(286);
				superclass();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(288);
				match(CLASS);
				setState(289);
				cpath();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(290);
				match(CLASS);
				setState(291);
				match(BIT_SHL);
				setState(292);
				identifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definitionContext extends ParserRuleContext {
		public Function_definition_headerContext function_definition_header() {
			return getRuleContext(Function_definition_headerContext.class,0);
		}
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public Function_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_definition; }
	}

	public final Function_definitionContext function_definition() throws RecognitionException {
		Function_definitionContext _localctx = new Function_definitionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_function_definition);
		try {
			setState(302);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(295);
				function_definition_header();
				setState(296);
				statement_list_terms(0);
				setState(297);
				match(END);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(299);
				function_definition_header();
				setState(300);
				match(END);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definition_headerContext extends ParserRuleContext {
		public TerminalNode DEF() { return getToken(RubyParser.DEF, 0); }
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public Opt_function_definition_paramsContext opt_function_definition_params() {
			return getRuleContext(Opt_function_definition_paramsContext.class,0);
		}
		public Opt_exprContext opt_expr() {
			return getRuleContext(Opt_exprContext.class,0);
		}
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public Function_definition_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_definition_header; }
	}

	public final Function_definition_headerContext function_definition_header() throws RecognitionException {
		Function_definition_headerContext _localctx = new Function_definition_headerContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_function_definition_header);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(304);
			match(DEF);
			setState(305);
			function_name();
			setState(306);
			opt_function_definition_params();
			setState(307);
			opt_expr();
			setState(308);
			terms();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_function_definition_paramsContext extends ParserRuleContext {
		public Function_definition_paramsContext function_definition_params() {
			return getRuleContext(Function_definition_paramsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_function_definition_paramsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_function_definition_params; }
	}

	public final Opt_function_definition_paramsContext opt_function_definition_params() throws RecognitionException {
		Opt_function_definition_paramsContext _localctx = new Opt_function_definition_paramsContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_opt_function_definition_params);
		try {
			setState(312);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(310);
				function_definition_params();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(311);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_exprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_expr; }
	}

	public final Opt_exprContext opt_expr() throws RecognitionException {
		Opt_exprContext _localctx = new Opt_exprContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_opt_expr);
		try {
			setState(316);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(314);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(315);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_nameContext extends ParserRuleContext {
		public CpathContext cpath() {
			return getRuleContext(CpathContext.class,0);
		}
		public Opt_cpath_func_suffixContext opt_cpath_func_suffix() {
			return getRuleContext(Opt_cpath_func_suffixContext.class,0);
		}
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public AssignOperatorContext assignOperator() {
			return getRuleContext(AssignOperatorContext.class,0);
		}
		public MathOperatorContext mathOperator() {
			return getRuleContext(MathOperatorContext.class,0);
		}
		public TerminalNode AT() { return getToken(RubyParser.AT, 0); }
		public BitOperatorContext bitOperator() {
			return getRuleContext(BitOperatorContext.class,0);
		}
		public CompareOperatorContext compareOperator() {
			return getRuleContext(CompareOperatorContext.class,0);
		}
		public EqualsOperatorContext equalsOperator() {
			return getRuleContext(EqualsOperatorContext.class,0);
		}
		public LogicalOperatorContext logicalOperator() {
			return getRuleContext(LogicalOperatorContext.class,0);
		}
		public Function_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_name; }
	}

	public final Function_nameContext function_name() throws RecognitionException {
		Function_nameContext _localctx = new Function_nameContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_function_name);
		try {
			setState(330);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(318);
				cpath();
				setState(319);
				opt_cpath_func_suffix();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(321);
				literal();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(322);
				assignOperator();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(323);
				mathOperator();
				setState(324);
				match(AT);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(326);
				bitOperator();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(327);
				compareOperator();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(328);
				equalsOperator();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(329);
				logicalOperator();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_cpath_func_suffixContext extends ParserRuleContext {
		public TerminalNode QUESTION() { return getToken(RubyParser.QUESTION, 0); }
		public TerminalNode SIGH() { return getToken(RubyParser.SIGH, 0); }
		public TerminalNode ASSIGN() { return getToken(RubyParser.ASSIGN, 0); }
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_cpath_func_suffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_cpath_func_suffix; }
	}

	public final Opt_cpath_func_suffixContext opt_cpath_func_suffix() throws RecognitionException {
		Opt_cpath_func_suffixContext _localctx = new Opt_cpath_func_suffixContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_opt_cpath_func_suffix);
		try {
			setState(336);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(332);
				match(QUESTION);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(333);
				match(SIGH);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(334);
				match(ASSIGN);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(335);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definition_paramsContext extends ParserRuleContext {
		public TerminalNode LEFT_RBRACKET() { return getToken(RubyParser.LEFT_RBRACKET, 0); }
		public Func_def_bodyContext func_def_body() {
			return getRuleContext(Func_def_bodyContext.class,0);
		}
		public TerminalNode RIGHT_RBRACKET() { return getToken(RubyParser.RIGHT_RBRACKET, 0); }
		public Function_definition_paramsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_definition_params; }
	}

	public final Function_definition_paramsContext function_definition_params() throws RecognitionException {
		Function_definition_paramsContext _localctx = new Function_definition_paramsContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_function_definition_params);
		try {
			setState(345);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(338);
				match(LEFT_RBRACKET);
				setState(339);
				func_def_body();
				setState(340);
				match(RIGHT_RBRACKET);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(342);
				match(LEFT_RBRACKET);
				setState(343);
				match(RIGHT_RBRACKET);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(344);
				func_def_body();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_def_bodyContext extends ParserRuleContext {
		public Function_definition_paramContext function_definition_param() {
			return getRuleContext(Function_definition_paramContext.class,0);
		}
		public Opt_function_definition_param_listContext opt_function_definition_param_list() {
			return getRuleContext(Opt_function_definition_param_listContext.class,0);
		}
		public Func_def_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_def_body; }
	}

	public final Func_def_bodyContext func_def_body() throws RecognitionException {
		Func_def_bodyContext _localctx = new Func_def_bodyContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_func_def_body);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(347);
			function_definition_param();
			setState(348);
			opt_function_definition_param_list();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_function_definition_param_listContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public Function_definition_paramContext function_definition_param() {
			return getRuleContext(Function_definition_paramContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_function_definition_param_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_function_definition_param_list; }
	}

	public final Opt_function_definition_param_listContext opt_function_definition_param_list() throws RecognitionException {
		Opt_function_definition_param_listContext _localctx = new Opt_function_definition_param_listContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_opt_function_definition_param_list);
		try {
			setState(355);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COMMA:
				enterOuterAlt(_localctx, 1);
				{
				setState(350);
				match(COMMA);
				setState(351);
				opt_crlfs();
				setState(352);
				function_definition_param();
				}
				break;
			case BEGIN:
			case BREAK:
			case CASE:
			case CLASS:
			case DEF:
			case DEFINED:
			case FALSE:
			case FOR:
			case IF:
			case MODULE:
			case NEXT:
			case NIL:
			case NOT:
			case RAISE:
			case REDO:
			case RESCUE:
			case RETRY:
			case RETURN:
			case SELF:
			case SUPER:
			case TRUE:
			case UNLESS:
			case UNTIL:
			case WHEN:
			case WHILE:
			case YIELD:
			case Integer:
			case Float:
			case Regex:
			case String:
			case DollarSpecial:
			case CRLF:
			case AND:
			case OR:
			case PLUS:
			case MINUS:
			case MOD:
			case PLUS_ASSIGN:
			case MOD_ASSIGN:
			case AND_ASSIGN:
			case BIT_AND_ASSIGN:
			case BIT_AND:
			case EXP_ASSIGN:
			case EXP:
			case MUL:
			case MUL_ASSIGN:
			case DOLLAR:
			case AT:
			case LEFT_PAREN:
			case LEFT_SBRACKET:
			case RIGHT_RBRACKET:
			case LEFT_RBRACKET:
			case DIV_ASSIGN:
			case DIV:
			case NOT_EQUAL:
			case SIGH:
			case COLON:
			case COLON2:
			case SEMICOLON:
			case MINUS_ASSIGN:
			case OR_ASSIGN:
			case BIT_OR_ASSIGN:
			case BIT_OR:
			case BIT_SHR_ASSIGN:
			case GREATER_EQUAL:
			case BIT_SHR:
			case GREATER:
			case ASSIGN:
			case EQUAL3:
			case EQUAL:
			case LESS_EQUAL:
			case LESS:
			case BIT_SHL_ASSIGN:
			case BIT_SHL:
			case BIT_NOT_ASSIGN:
			case BIT_NOT:
			case BIT_XOR_ASSIGN:
			case BIT_XOR:
			case SL_COMMENT:
			case Identifier:
			case ShellCommand:
				enterOuterAlt(_localctx, 2);
				{
				setState(354);
				none();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definition_paramContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode MUL() { return getToken(RubyParser.MUL, 0); }
		public TerminalNode EXP() { return getToken(RubyParser.EXP, 0); }
		public TerminalNode BIT_AND() { return getToken(RubyParser.BIT_AND, 0); }
		public Hash_assoContext hash_asso() {
			return getRuleContext(Hash_assoContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(RubyParser.ASSIGN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Function_definition_paramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_definition_param; }
	}

	public final Function_definition_paramContext function_definition_param() throws RecognitionException {
		Function_definition_paramContext _localctx = new Function_definition_paramContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_function_definition_param);
		try {
			setState(369);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(357);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(358);
				match(MUL);
				setState(359);
				identifier();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(360);
				match(EXP);
				setState(361);
				identifier();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(362);
				match(BIT_AND);
				setState(363);
				identifier();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(364);
				hash_asso();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(365);
				identifier();
				setState(366);
				match(ASSIGN);
				setState(367);
				expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_call_paramContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Hash_assoContext hash_asso() {
			return getRuleContext(Hash_assoContext.class,0);
		}
		public Function_call_paramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call_param; }
	}

	public final Function_call_paramContext function_call_param() throws RecognitionException {
		Function_call_paramContext _localctx = new Function_call_paramContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_function_call_param);
		try {
			setState(373);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(371);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(372);
				hash_asso();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExprLogicalNotContext extends ExprContext {
		public Not_or_bitnotContext not_or_bitnot() {
			return getRuleContext(Not_or_bitnotContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ExprLogicalNotContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprDotClassContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Dot_refContext dot_ref() {
			return getRuleContext(Dot_refContext.class,0);
		}
		public TerminalNode CLASS() { return getToken(RubyParser.CLASS, 0); }
		public ExprDotClassContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprCalcuationContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public Math_or_bit_opContext math_or_bit_op() {
			return getRuleContext(Math_or_bit_opContext.class,0);
		}
		public ExprCalcuationContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprPrimaryContext extends ExprContext {
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public ExprPrimaryContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprCompareLogicalContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public CompareOperatorContext compareOperator() {
			return getRuleContext(CompareOperatorContext.class,0);
		}
		public ExprCompareLogicalContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprBatchAssignContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Opt_commaContext opt_comma() {
			return getRuleContext(Opt_commaContext.class,0);
		}
		public Opt_mulContext opt_mul() {
			return getRuleContext(Opt_mulContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(RubyParser.ASSIGN, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprBatchAssignContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprPatternMatch2Context extends ExprContext {
		public Token bop;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode BIT_NOT() { return getToken(RubyParser.BIT_NOT, 0); }
		public ExprPatternMatch2Context(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprPatternMatch3Context extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode SIGH() { return getToken(RubyParser.SIGH, 0); }
		public TerminalNode BIT_NOT() { return getToken(RubyParser.BIT_NOT, 0); }
		public ExprPatternMatch3Context(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprPrefixCalcContext extends ExprContext {
		public Token prefix;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(RubyParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(RubyParser.MINUS, 0); }
		public TerminalNode MUL() { return getToken(RubyParser.MUL, 0); }
		public TerminalNode MOD() { return getToken(RubyParser.MOD, 0); }
		public TerminalNode BIT_AND() { return getToken(RubyParser.BIT_AND, 0); }
		public ExprPrefixCalcContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprFunctionCall3Context extends ExprContext {
		public CpathContext cpath() {
			return getRuleContext(CpathContext.class,0);
		}
		public Cpath_expr_suffixContext cpath_expr_suffix() {
			return getRuleContext(Cpath_expr_suffixContext.class,0);
		}
		public ExprFunctionCall3Context(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprQuestionContext extends ExprContext {
		public Token postfix;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode QUESTION() { return getToken(RubyParser.QUESTION, 0); }
		public ExprQuestionContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprFunctionCall2Context extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Dot_refContext dot_ref() {
			return getRuleContext(Dot_refContext.class,0);
		}
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public Func_call_parameters_no_bracketContext func_call_parameters_no_bracket() {
			return getRuleContext(Func_call_parameters_no_bracketContext.class,0);
		}
		public Opt_expr_statement_suffixContext opt_expr_statement_suffix() {
			return getRuleContext(Opt_expr_statement_suffixContext.class,0);
		}
		public ExprFunctionCall2Context(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprFunctionCall1Context extends ExprContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public Func_call_parameters_no_bracketContext func_call_parameters_no_bracket() {
			return getRuleContext(Func_call_parameters_no_bracketContext.class,0);
		}
		public Opt_expr_statement_suffixContext opt_expr_statement_suffix() {
			return getRuleContext(Opt_expr_statement_suffixContext.class,0);
		}
		public ExprFunctionCall1Context(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprEqualTestContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public EqualsOperatorContext equalsOperator() {
			return getRuleContext(EqualsOperatorContext.class,0);
		}
		public ExprEqualTestContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprRangeContext extends ExprContext {
		public Token bop;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Opt_exprContext opt_expr() {
			return getRuleContext(Opt_exprContext.class,0);
		}
		public TerminalNode DOT2() { return getToken(RubyParser.DOT2, 0); }
		public TerminalNode DOT3() { return getToken(RubyParser.DOT3, 0); }
		public ExprRangeContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprPatternMatch1Context extends ExprContext {
		public Token bop;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode PATTERN_MATCH() { return getToken(RubyParser.PATTERN_MATCH, 0); }
		public ExprPatternMatch1Context(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprBlockContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ExprBlockContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprLogicalJoinContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public LogicalOperatorContext logicalOperator() {
			return getRuleContext(LogicalOperatorContext.class,0);
		}
		public ExprLogicalJoinContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprArrayAccessContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LEFT_SBRACKET() { return getToken(RubyParser.LEFT_SBRACKET, 0); }
		public TerminalNode RIGHT_SBRACKET() { return getToken(RubyParser.RIGHT_SBRACKET, 0); }
		public ExprArrayAccessContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprConditionStatementContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode QUESTION() { return getToken(RubyParser.QUESTION, 0); }
		public TerminalNode COLON() { return getToken(RubyParser.COLON, 0); }
		public ExprConditionStatementContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprWitStatementSuffixContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Expr_statement_suffixContext expr_statement_suffix() {
			return getRuleContext(Expr_statement_suffixContext.class,0);
		}
		public ExprWitStatementSuffixContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprDefineTestContext extends ExprContext {
		public Token prefix;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode DEFINED() { return getToken(RubyParser.DEFINED, 0); }
		public ExprDefineTestContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprListContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprListContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprDotRefContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Dot_refContext dot_ref() {
			return getRuleContext(Dot_refContext.class,0);
		}
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprDotRefContext(ExprContext ctx) { copyFrom(ctx); }
	}
	public static class ExprAssignContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AssignOperatorContext assignOperator() {
			return getRuleContext(AssignOperatorContext.class,0);
		}
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprAssignContext(ExprContext ctx) { copyFrom(ctx); }
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 54;
		enterRecursionRule(_localctx, 54, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(391);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				_localctx = new ExprPrimaryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(376);
				primary();
				}
				break;
			case 2:
				{
				_localctx = new ExprPrefixCalcContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(377);
				((ExprPrefixCalcContext)_localctx).prefix = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << MOD) | (1L << BIT_AND) | (1L << MUL))) != 0)) ) {
					((ExprPrefixCalcContext)_localctx).prefix = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(378);
				expr(21);
				}
				break;
			case 3:
				{
				_localctx = new ExprDefineTestContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(379);
				((ExprDefineTestContext)_localctx).prefix = match(DEFINED);
				setState(380);
				expr(19);
				}
				break;
			case 4:
				{
				_localctx = new ExprLogicalNotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(381);
				not_or_bitnot();
				setState(382);
				expr(12);
				}
				break;
			case 5:
				{
				_localctx = new ExprFunctionCall1Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(384);
				function_name();
				setState(385);
				func_call_parameters_no_bracket();
				setState(386);
				opt_expr_statement_suffix();
				}
				break;
			case 6:
				{
				_localctx = new ExprFunctionCall3Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(388);
				cpath();
				setState(389);
				cpath_expr_suffix();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(477);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(475);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
					case 1:
						{
						_localctx = new ExprListContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(393);
						if (!(precpred(_ctx, 24))) throw new FailedPredicateException(this, "precpred(_ctx, 24)");
						setState(394);
						match(COMMA);
						setState(395);
						opt_crlfs();
						setState(396);
						expr(25);
						}
						break;
					case 2:
						{
						_localctx = new ExprDotRefContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(398);
						if (!(precpred(_ctx, 23))) throw new FailedPredicateException(this, "precpred(_ctx, 23)");
						setState(399);
						dot_ref();
						setState(400);
						opt_crlfs();
						setState(401);
						expr(24);
						}
						break;
					case 3:
						{
						_localctx = new ExprBatchAssignContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(403);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(404);
						opt_comma();
						setState(405);
						opt_mul();
						setState(406);
						match(ASSIGN);
						setState(407);
						opt_crlfs();
						setState(408);
						expr(18);
						}
						break;
					case 4:
						{
						_localctx = new ExprAssignContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(410);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(411);
						assignOperator();
						setState(412);
						opt_crlfs();
						setState(413);
						expr(17);
						}
						break;
					case 5:
						{
						_localctx = new ExprPatternMatch1Context(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(415);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(416);
						((ExprPatternMatch1Context)_localctx).bop = match(PATTERN_MATCH);
						setState(417);
						expr(16);
						}
						break;
					case 6:
						{
						_localctx = new ExprPatternMatch2Context(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(418);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(419);
						((ExprPatternMatch2Context)_localctx).bop = match(BIT_NOT);
						setState(420);
						expr(15);
						}
						break;
					case 7:
						{
						_localctx = new ExprPatternMatch3Context(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(421);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(422);
						match(SIGH);
						setState(423);
						match(BIT_NOT);
						setState(424);
						expr(14);
						}
						break;
					case 8:
						{
						_localctx = new ExprCompareLogicalContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(425);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						{
						setState(426);
						compareOperator();
						}
						setState(427);
						opt_crlfs();
						setState(428);
						expr(12);
						}
						break;
					case 9:
						{
						_localctx = new ExprLogicalJoinContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(430);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						{
						setState(431);
						logicalOperator();
						}
						setState(432);
						opt_crlfs();
						setState(433);
						expr(11);
						}
						break;
					case 10:
						{
						_localctx = new ExprEqualTestContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(435);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						{
						setState(436);
						equalsOperator();
						}
						setState(437);
						opt_crlfs();
						setState(438);
						expr(10);
						}
						break;
					case 11:
						{
						_localctx = new ExprCalcuationContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(440);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						{
						setState(441);
						math_or_bit_op();
						}
						setState(442);
						opt_crlfs();
						setState(443);
						expr(9);
						}
						break;
					case 12:
						{
						_localctx = new ExprConditionStatementContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(445);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(446);
						match(QUESTION);
						setState(447);
						expr(0);
						setState(448);
						match(COLON);
						setState(449);
						expr(8);
						}
						break;
					case 13:
						{
						_localctx = new ExprQuestionContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(451);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(452);
						((ExprQuestionContext)_localctx).postfix = match(QUESTION);
						}
						break;
					case 14:
						{
						_localctx = new ExprArrayAccessContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(453);
						if (!(precpred(_ctx, 20))) throw new FailedPredicateException(this, "precpred(_ctx, 20)");
						setState(454);
						match(LEFT_SBRACKET);
						setState(455);
						expr(0);
						setState(456);
						match(RIGHT_SBRACKET);
						}
						break;
					case 15:
						{
						_localctx = new ExprRangeContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(458);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(459);
						((ExprRangeContext)_localctx).bop = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==DOT3 || _la==DOT2) ) {
							((ExprRangeContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(460);
						opt_expr();
						}
						break;
					case 16:
						{
						_localctx = new ExprBlockContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(461);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(462);
						block();
						}
						break;
					case 17:
						{
						_localctx = new ExprWitStatementSuffixContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(463);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(464);
						expr_statement_suffix();
						}
						break;
					case 18:
						{
						_localctx = new ExprDotClassContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(465);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(466);
						dot_ref();
						setState(467);
						match(CLASS);
						}
						break;
					case 19:
						{
						_localctx = new ExprFunctionCall2Context(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(469);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(470);
						dot_ref();
						setState(471);
						function_name();
						setState(472);
						func_call_parameters_no_bracket();
						setState(473);
						opt_expr_statement_suffix();
						}
						break;
					}
					} 
				}
				setState(479);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Math_or_bit_opContext extends ParserRuleContext {
		public MathOperatorContext mathOperator() {
			return getRuleContext(MathOperatorContext.class,0);
		}
		public BitOperatorContext bitOperator() {
			return getRuleContext(BitOperatorContext.class,0);
		}
		public Math_or_bit_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_math_or_bit_op; }
	}

	public final Math_or_bit_opContext math_or_bit_op() throws RecognitionException {
		Math_or_bit_opContext _localctx = new Math_or_bit_opContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_math_or_bit_op);
		try {
			setState(482);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
			case MINUS:
			case MOD:
			case EXP:
			case MUL:
			case DIV:
				enterOuterAlt(_localctx, 1);
				{
				setState(480);
				mathOperator();
				}
				break;
			case BIT_AND:
			case BIT_OR:
			case BIT_SHR:
			case BIT_SHL:
			case BIT_XOR:
				enterOuterAlt(_localctx, 2);
				{
				setState(481);
				bitOperator();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Not_or_bitnotContext extends ParserRuleContext {
		public NotSymContext notSym() {
			return getRuleContext(NotSymContext.class,0);
		}
		public TerminalNode BIT_NOT() { return getToken(RubyParser.BIT_NOT, 0); }
		public Not_or_bitnotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not_or_bitnot; }
	}

	public final Not_or_bitnotContext not_or_bitnot() throws RecognitionException {
		Not_or_bitnotContext _localctx = new Not_or_bitnotContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_not_or_bitnot);
		try {
			setState(486);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT:
			case SIGH:
				enterOuterAlt(_localctx, 1);
				{
				setState(484);
				notSym();
				}
				break;
			case BIT_NOT:
				enterOuterAlt(_localctx, 2);
				{
				setState(485);
				match(BIT_NOT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cpath_expr_suffixContext extends ParserRuleContext {
		public TerminalNode QUESTION() { return getToken(RubyParser.QUESTION, 0); }
		public TerminalNode SIGH() { return getToken(RubyParser.SIGH, 0); }
		public Cpath_expr_suffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cpath_expr_suffix; }
	}

	public final Cpath_expr_suffixContext cpath_expr_suffix() throws RecognitionException {
		Cpath_expr_suffixContext _localctx = new Cpath_expr_suffixContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_cpath_expr_suffix);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(488);
			_la = _input.LA(1);
			if ( !(_la==QUESTION || _la==SIGH) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_statement_suffixContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(RubyParser.IF, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode UNLESS() { return getToken(RubyParser.UNLESS, 0); }
		public TerminalNode WHILE() { return getToken(RubyParser.WHILE, 0); }
		public TerminalNode UNTIL() { return getToken(RubyParser.UNTIL, 0); }
		public TerminalNode RESCUE() { return getToken(RubyParser.RESCUE, 0); }
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public TerminalNode DO() { return getToken(RubyParser.DO, 0); }
		public Opt_block_paramsContext opt_block_params() {
			return getRuleContext(Opt_block_paramsContext.class,0);
		}
		public Opt_termsContext opt_terms() {
			return getRuleContext(Opt_termsContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public Expr_statement_suffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_statement_suffix; }
	}

	public final Expr_statement_suffixContext expr_statement_suffix() throws RecognitionException {
		Expr_statement_suffixContext _localctx = new Expr_statement_suffixContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_expr_statement_suffix);
		try {
			setState(514);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IF:
				enterOuterAlt(_localctx, 1);
				{
				setState(490);
				match(IF);
				setState(491);
				opt_crlfs();
				setState(492);
				expr(0);
				}
				break;
			case UNLESS:
				enterOuterAlt(_localctx, 2);
				{
				setState(494);
				match(UNLESS);
				setState(495);
				opt_crlfs();
				setState(496);
				expr(0);
				}
				break;
			case WHILE:
				enterOuterAlt(_localctx, 3);
				{
				setState(498);
				match(WHILE);
				setState(499);
				opt_crlfs();
				setState(500);
				expr(0);
				}
				break;
			case UNTIL:
				enterOuterAlt(_localctx, 4);
				{
				setState(502);
				match(UNTIL);
				setState(503);
				opt_crlfs();
				setState(504);
				expr(0);
				}
				break;
			case RESCUE:
				enterOuterAlt(_localctx, 5);
				{
				setState(506);
				match(RESCUE);
				setState(507);
				statement();
				}
				break;
			case DO:
				enterOuterAlt(_localctx, 6);
				{
				setState(508);
				match(DO);
				setState(509);
				opt_block_params();
				setState(510);
				opt_terms();
				setState(511);
				opt_statement_list_terms();
				setState(512);
				match(END);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_expr_statement_suffixContext extends ParserRuleContext {
		public Expr_statement_suffixContext expr_statement_suffix() {
			return getRuleContext(Expr_statement_suffixContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_expr_statement_suffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_expr_statement_suffix; }
	}

	public final Opt_expr_statement_suffixContext opt_expr_statement_suffix() throws RecognitionException {
		Opt_expr_statement_suffixContext _localctx = new Opt_expr_statement_suffixContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_opt_expr_statement_suffix);
		try {
			setState(518);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(516);
				expr_statement_suffix();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(517);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
	 
		public PrimaryContext() { }
		public void copyFrom(PrimaryContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrimaryVarPathContext extends PrimaryContext {
		public Variable_pathContext variable_path() {
			return getRuleContext(Variable_pathContext.class,0);
		}
		public PrimaryVarPathContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryStatementReturnContext extends PrimaryContext {
		public TerminalNode RETURN() { return getToken(RubyParser.RETURN, 0); }
		public Opt_exprContext opt_expr() {
			return getRuleContext(Opt_exprContext.class,0);
		}
		public PrimaryStatementReturnContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockModelDefContext extends PrimaryContext {
		public Module_definitionContext module_definition() {
			return getRuleContext(Module_definitionContext.class,0);
		}
		public PrimaryBlockModelDefContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryFunctionCall0Context extends PrimaryContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public Func_call_parametersContext func_call_parameters() {
			return getRuleContext(Func_call_parametersContext.class,0);
		}
		public PrimaryFunctionCall0Context(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryListExprContext extends PrimaryContext {
		public TerminalNode LEFT_SBRACKET() { return getToken(RubyParser.LEFT_SBRACKET, 0); }
		public List<Opt_crlfsContext> opt_crlfs() {
			return getRuleContexts(Opt_crlfsContext.class);
		}
		public Opt_crlfsContext opt_crlfs(int i) {
			return getRuleContext(Opt_crlfsContext.class,i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Comma_exprsContext comma_exprs() {
			return getRuleContext(Comma_exprsContext.class,0);
		}
		public Opt_commaContext opt_comma() {
			return getRuleContext(Opt_commaContext.class,0);
		}
		public TerminalNode RIGHT_SBRACKET() { return getToken(RubyParser.RIGHT_SBRACKET, 0); }
		public PrimaryListExprContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockCase2Context extends PrimaryContext {
		public TerminalNode CASE() { return getToken(RubyParser.CASE, 0); }
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public Opt_case_bodysContext opt_case_bodys() {
			return getRuleContext(Opt_case_bodysContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockCase2Context(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockCase1Context extends PrimaryContext {
		public TerminalNode CASE() { return getToken(RubyParser.CASE, 0); }
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public Opt_case_bodysContext opt_case_bodys() {
			return getRuleContext(Opt_case_bodysContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockCase1Context(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockIf3Context extends PrimaryContext {
		public TerminalNode IF() { return getToken(RubyParser.IF, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Opt_else_ifContext opt_else_if() {
			return getRuleContext(Opt_else_ifContext.class,0);
		}
		public Opt_elseContext opt_else() {
			return getRuleContext(Opt_elseContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockIf3Context(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockIf2Context extends PrimaryContext {
		public TerminalNode IF() { return getToken(RubyParser.IF, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public If_tailsContext if_tails() {
			return getRuleContext(If_tailsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockIf2Context(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockContext extends PrimaryContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public PrimaryBlockContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockUntilContext extends PrimaryContext {
		public TerminalNode UNTIL() { return getToken(RubyParser.UNTIL, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Do_keywordContext do_keyword() {
			return getRuleContext(Do_keywordContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockUntilContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockUnlessContext extends PrimaryContext {
		public TerminalNode UNLESS() { return getToken(RubyParser.UNLESS, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public Opt_else_tailContext opt_else_tail() {
			return getRuleContext(Opt_else_tailContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockUnlessContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryStatementBreakContext extends PrimaryContext {
		public TerminalNode BREAK() { return getToken(RubyParser.BREAK, 0); }
		public Opt_exprContext opt_expr() {
			return getRuleContext(Opt_exprContext.class,0);
		}
		public PrimaryStatementBreakContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockIfContext extends PrimaryContext {
		public TerminalNode IF() { return getToken(RubyParser.IF, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockIfContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockForContext extends PrimaryContext {
		public TerminalNode FOR() { return getToken(RubyParser.FOR, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode IN() { return getToken(RubyParser.IN, 0); }
		public When_condContext when_cond() {
			return getRuleContext(When_condContext.class,0);
		}
		public Opt_termsContext opt_terms() {
			return getRuleContext(Opt_termsContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockForContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryRangeContext extends PrimaryContext {
		public TerminalNode LEFT_RBRACKET() { return getToken(RubyParser.LEFT_RBRACKET, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Dot2_or_dot3Context dot2_or_dot3() {
			return getRuleContext(Dot2_or_dot3Context.class,0);
		}
		public Opt_exprContext opt_expr() {
			return getRuleContext(Opt_exprContext.class,0);
		}
		public TerminalNode RIGHT_RBRACKET() { return getToken(RubyParser.RIGHT_RBRACKET, 0); }
		public PrimaryRangeContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryStatementRaiseContext extends PrimaryContext {
		public TerminalNode RAISE() { return getToken(RubyParser.RAISE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public PrimaryStatementRaiseContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryStatementRescueContext extends PrimaryContext {
		public TerminalNode RESCUE() { return getToken(RubyParser.RESCUE, 0); }
		public Opt_rescure_paramContext opt_rescure_param() {
			return getRuleContext(Opt_rescure_paramContext.class,0);
		}
		public Opt_else_tailContext opt_else_tail() {
			return getRuleContext(Opt_else_tailContext.class,0);
		}
		public PrimaryStatementRescueContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryRegexContext extends PrimaryContext {
		public TerminalNode Regex() { return getToken(RubyParser.Regex, 0); }
		public PrimaryRegexContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryStatementYieldContext extends PrimaryContext {
		public TerminalNode YIELD() { return getToken(RubyParser.YIELD, 0); }
		public Opt_exprContext opt_expr() {
			return getRuleContext(Opt_exprContext.class,0);
		}
		public PrimaryStatementYieldContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockFunctionDefContext extends PrimaryContext {
		public Function_definitionContext function_definition() {
			return getRuleContext(Function_definitionContext.class,0);
		}
		public PrimaryBlockFunctionDefContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBracketContext extends PrimaryContext {
		public TerminalNode LEFT_RBRACKET() { return getToken(RubyParser.LEFT_RBRACKET, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RIGHT_RBRACKET() { return getToken(RubyParser.RIGHT_RBRACKET, 0); }
		public PrimaryBracketContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockClassDefContext extends PrimaryContext {
		public Class_definitionContext class_definition() {
			return getRuleContext(Class_definitionContext.class,0);
		}
		public PrimaryBlockClassDefContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBeginBlockContext extends PrimaryContext {
		public TerminalNode BEGIN() { return getToken(RubyParser.BEGIN, 0); }
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBeginBlockContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockWhenContext extends PrimaryContext {
		public TerminalNode WHEN() { return getToken(RubyParser.WHEN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public Statement_list_notermsContext statement_list_noterms() {
			return getRuleContext(Statement_list_notermsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockWhenContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryListHashContext extends PrimaryContext {
		public TerminalNode LEFT_PAREN() { return getToken(RubyParser.LEFT_PAREN, 0); }
		public List<Opt_crlfsContext> opt_crlfs() {
			return getRuleContexts(Opt_crlfsContext.class);
		}
		public Opt_crlfsContext opt_crlfs(int i) {
			return getRuleContext(Opt_crlfsContext.class,i);
		}
		public Hash_assoContext hash_asso() {
			return getRuleContext(Hash_assoContext.class,0);
		}
		public Comma_hash_asssosContext comma_hash_asssos() {
			return getRuleContext(Comma_hash_asssosContext.class,0);
		}
		public Opt_commaContext opt_comma() {
			return getRuleContext(Opt_commaContext.class,0);
		}
		public TerminalNode RIGHT_PAREN() { return getToken(RubyParser.RIGHT_PAREN, 0); }
		public PrimaryListHashContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimaryBlockWhileContext extends PrimaryContext {
		public TerminalNode WHILE() { return getToken(RubyParser.WHILE, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Do_keywordContext do_keyword() {
			return getRuleContext(Do_keywordContext.class,0);
		}
		public Opt_statement_list_termsContext opt_statement_list_terms() {
			return getRuleContext(Opt_statement_list_termsContext.class,0);
		}
		public TerminalNode END() { return getToken(RubyParser.END, 0); }
		public PrimaryBlockWhileContext(PrimaryContext ctx) { copyFrom(ctx); }
	}
	public static class PrimarySymbolContext extends PrimaryContext {
		public SymbolContext symbol() {
			return getRuleContext(SymbolContext.class,0);
		}
		public PrimarySymbolContext(PrimaryContext ctx) { copyFrom(ctx); }
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_primary);
		try {
			setState(645);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				_localctx = new PrimaryVarPathContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(520);
				variable_path(0);
				}
				break;
			case 2:
				_localctx = new PrimaryRegexContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(521);
				match(Regex);
				}
				break;
			case 3:
				_localctx = new PrimarySymbolContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(522);
				symbol();
				}
				break;
			case 4:
				_localctx = new PrimaryBracketContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(523);
				match(LEFT_RBRACKET);
				setState(524);
				expr(0);
				setState(525);
				match(RIGHT_RBRACKET);
				}
				break;
			case 5:
				_localctx = new PrimaryBlockContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(527);
				block();
				}
				break;
			case 6:
				_localctx = new PrimaryStatementBreakContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(528);
				match(BREAK);
				setState(529);
				opt_expr();
				}
				break;
			case 7:
				_localctx = new PrimaryStatementReturnContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(530);
				match(RETURN);
				setState(531);
				opt_expr();
				}
				break;
			case 8:
				_localctx = new PrimaryStatementRaiseContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(532);
				match(RAISE);
				setState(533);
				expr(0);
				}
				break;
			case 9:
				_localctx = new PrimaryStatementRescueContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(534);
				match(RESCUE);
				setState(535);
				opt_rescure_param();
				setState(536);
				opt_else_tail();
				}
				break;
			case 10:
				_localctx = new PrimaryStatementYieldContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(538);
				match(YIELD);
				setState(539);
				opt_expr();
				}
				break;
			case 11:
				_localctx = new PrimaryBeginBlockContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(540);
				match(BEGIN);
				setState(541);
				terms();
				setState(542);
				opt_statement_list_terms();
				setState(543);
				match(END);
				}
				break;
			case 12:
				_localctx = new PrimaryBlockIf3Context(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(545);
				match(IF);
				setState(546);
				expr(0);
				setState(547);
				then_keyword();
				setState(548);
				statement();
				setState(549);
				opt_else_if();
				setState(550);
				opt_else();
				setState(551);
				match(END);
				}
				break;
			case 13:
				_localctx = new PrimaryBlockIf2Context(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(553);
				match(IF);
				setState(554);
				opt_crlfs();
				setState(555);
				expr(0);
				setState(556);
				then_keyword();
				setState(557);
				opt_statement_list_terms();
				setState(558);
				if_tails();
				setState(559);
				match(END);
				}
				break;
			case 14:
				_localctx = new PrimaryBlockIfContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(561);
				match(IF);
				setState(562);
				opt_crlfs();
				setState(563);
				expr(0);
				setState(564);
				then_keyword();
				setState(565);
				opt_statement_list_terms();
				setState(566);
				match(END);
				}
				break;
			case 15:
				_localctx = new PrimaryBlockWhenContext(_localctx);
				enterOuterAlt(_localctx, 15);
				{
				setState(568);
				match(WHEN);
				setState(569);
				expr(0);
				setState(570);
				then_keyword();
				setState(571);
				statement_list_noterms();
				setState(572);
				match(END);
				}
				break;
			case 16:
				_localctx = new PrimaryBlockUnlessContext(_localctx);
				enterOuterAlt(_localctx, 16);
				{
				setState(574);
				match(UNLESS);
				setState(575);
				opt_crlfs();
				setState(576);
				expr(0);
				setState(577);
				then_keyword();
				setState(578);
				opt_statement_list_terms();
				setState(579);
				opt_else_tail();
				setState(580);
				match(END);
				}
				break;
			case 17:
				_localctx = new PrimaryBlockCase1Context(_localctx);
				enterOuterAlt(_localctx, 17);
				{
				setState(582);
				match(CASE);
				setState(583);
				opt_statement_list_terms();
				setState(584);
				opt_case_bodys();
				setState(585);
				match(END);
				}
				break;
			case 18:
				_localctx = new PrimaryBlockCase2Context(_localctx);
				enterOuterAlt(_localctx, 18);
				{
				setState(587);
				match(CASE);
				setState(588);
				terms();
				setState(589);
				opt_case_bodys();
				setState(590);
				match(END);
				}
				break;
			case 19:
				_localctx = new PrimaryBlockWhileContext(_localctx);
				enterOuterAlt(_localctx, 19);
				{
				setState(592);
				match(WHILE);
				setState(593);
				opt_crlfs();
				setState(594);
				expr(0);
				setState(595);
				do_keyword();
				setState(596);
				opt_statement_list_terms();
				setState(597);
				match(END);
				}
				break;
			case 20:
				_localctx = new PrimaryBlockUntilContext(_localctx);
				enterOuterAlt(_localctx, 20);
				{
				setState(599);
				match(UNTIL);
				setState(600);
				opt_crlfs();
				setState(601);
				expr(0);
				setState(602);
				do_keyword();
				setState(603);
				opt_statement_list_terms();
				setState(604);
				match(END);
				}
				break;
			case 21:
				_localctx = new PrimaryBlockForContext(_localctx);
				enterOuterAlt(_localctx, 21);
				{
				setState(606);
				match(FOR);
				setState(607);
				opt_crlfs();
				setState(608);
				expr(0);
				setState(609);
				match(IN);
				setState(610);
				when_cond();
				setState(611);
				opt_terms();
				setState(612);
				opt_statement_list_terms();
				setState(613);
				match(END);
				}
				break;
			case 22:
				_localctx = new PrimaryBlockClassDefContext(_localctx);
				enterOuterAlt(_localctx, 22);
				{
				setState(615);
				class_definition();
				}
				break;
			case 23:
				_localctx = new PrimaryBlockFunctionDefContext(_localctx);
				enterOuterAlt(_localctx, 23);
				{
				setState(616);
				function_definition();
				}
				break;
			case 24:
				_localctx = new PrimaryBlockModelDefContext(_localctx);
				enterOuterAlt(_localctx, 24);
				{
				setState(617);
				module_definition();
				}
				break;
			case 25:
				_localctx = new PrimaryFunctionCall0Context(_localctx);
				enterOuterAlt(_localctx, 25);
				{
				setState(618);
				function_name();
				setState(619);
				func_call_parameters();
				}
				break;
			case 26:
				_localctx = new PrimaryListHashContext(_localctx);
				enterOuterAlt(_localctx, 26);
				{
				setState(621);
				match(LEFT_PAREN);
				setState(622);
				opt_crlfs();
				setState(623);
				hash_asso();
				setState(624);
				opt_crlfs();
				setState(625);
				comma_hash_asssos();
				setState(626);
				opt_comma();
				setState(627);
				opt_crlfs();
				setState(628);
				match(RIGHT_PAREN);
				}
				break;
			case 27:
				_localctx = new PrimaryListExprContext(_localctx);
				enterOuterAlt(_localctx, 27);
				{
				setState(630);
				match(LEFT_SBRACKET);
				setState(631);
				opt_crlfs();
				setState(632);
				expr(0);
				setState(633);
				opt_crlfs();
				setState(634);
				comma_exprs();
				setState(635);
				opt_comma();
				setState(636);
				opt_crlfs();
				setState(637);
				match(RIGHT_SBRACKET);
				}
				break;
			case 28:
				_localctx = new PrimaryRangeContext(_localctx);
				enterOuterAlt(_localctx, 28);
				{
				setState(639);
				match(LEFT_RBRACKET);
				setState(640);
				expr(0);
				setState(641);
				dot2_or_dot3();
				setState(642);
				opt_expr();
				setState(643);
				match(RIGHT_RBRACKET);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Comma_hash_asssosContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public Hash_assoContext hash_asso() {
			return getRuleContext(Hash_assoContext.class,0);
		}
		public Comma_hash_asssosContext comma_hash_asssos() {
			return getRuleContext(Comma_hash_asssosContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Comma_hash_asssosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comma_hash_asssos; }
	}

	public final Comma_hash_asssosContext comma_hash_asssos() throws RecognitionException {
		Comma_hash_asssosContext _localctx = new Comma_hash_asssosContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_comma_hash_asssos);
		try {
			setState(657);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(647);
				match(COMMA);
				setState(648);
				opt_crlfs();
				setState(649);
				hash_asso();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(651);
				match(COMMA);
				setState(652);
				opt_crlfs();
				setState(653);
				hash_asso();
				setState(654);
				comma_hash_asssos();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(656);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Comma_exprsContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Comma_exprsContext comma_exprs() {
			return getRuleContext(Comma_exprsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Comma_exprsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comma_exprs; }
	}

	public final Comma_exprsContext comma_exprs() throws RecognitionException {
		Comma_exprsContext _localctx = new Comma_exprsContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_comma_exprs);
		try {
			setState(669);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(659);
				match(COMMA);
				setState(660);
				opt_crlfs();
				setState(661);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(663);
				match(COMMA);
				setState(664);
				opt_crlfs();
				setState(665);
				expr(0);
				setState(666);
				comma_exprs();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(668);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dot2_or_dot3Context extends ParserRuleContext {
		public TerminalNode DOT2() { return getToken(RubyParser.DOT2, 0); }
		public TerminalNode DOT3() { return getToken(RubyParser.DOT3, 0); }
		public Dot2_or_dot3Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dot2_or_dot3; }
	}

	public final Dot2_or_dot3Context dot2_or_dot3() throws RecognitionException {
		Dot2_or_dot3Context _localctx = new Dot2_or_dot3Context(_ctx, getState());
		enterRule(_localctx, 72, RULE_dot2_or_dot3);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(671);
			_la = _input.LA(1);
			if ( !(_la==DOT3 || _la==DOT2) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_else_ifContext extends ParserRuleContext {
		public TerminalNode ELSIF() { return getToken(RubyParser.ELSIF, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_else_ifContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_else_if; }
	}

	public final Opt_else_ifContext opt_else_if() throws RecognitionException {
		Opt_else_ifContext _localctx = new Opt_else_ifContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_opt_else_if);
		try {
			setState(679);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ELSIF:
				enterOuterAlt(_localctx, 1);
				{
				setState(673);
				match(ELSIF);
				setState(674);
				expr(0);
				setState(675);
				then_keyword();
				setState(676);
				statement();
				}
				break;
			case ELSE:
			case END:
				enterOuterAlt(_localctx, 2);
				{
				setState(678);
				none();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_elseContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(RubyParser.ELSE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_elseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_else; }
	}

	public final Opt_elseContext opt_else() throws RecognitionException {
		Opt_elseContext _localctx = new Opt_elseContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_opt_else);
		try {
			setState(684);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ELSE:
				enterOuterAlt(_localctx, 1);
				{
				setState(681);
				match(ELSE);
				setState(682);
				expr(0);
				}
				break;
			case END:
				enterOuterAlt(_localctx, 2);
				{
				setState(683);
				none();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_call_parametersContext extends ParserRuleContext {
		public TerminalNode LEFT_RBRACKET() { return getToken(RubyParser.LEFT_RBRACKET, 0); }
		public List<Opt_crlfsContext> opt_crlfs() {
			return getRuleContexts(Opt_crlfsContext.class);
		}
		public Opt_crlfsContext opt_crlfs(int i) {
			return getRuleContext(Opt_crlfsContext.class,i);
		}
		public Function_call_paramContext function_call_param() {
			return getRuleContext(Function_call_paramContext.class,0);
		}
		public Function_call_param_restContext function_call_param_rest() {
			return getRuleContext(Function_call_param_restContext.class,0);
		}
		public TerminalNode RIGHT_RBRACKET() { return getToken(RubyParser.RIGHT_RBRACKET, 0); }
		public Func_call_parametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_call_parameters; }
	}

	public final Func_call_parametersContext func_call_parameters() throws RecognitionException {
		Func_call_parametersContext _localctx = new Func_call_parametersContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_func_call_parameters);
		try {
			setState(697);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(686);
				match(LEFT_RBRACKET);
				setState(687);
				opt_crlfs();
				setState(688);
				function_call_param();
				setState(689);
				function_call_param_rest();
				setState(690);
				opt_crlfs();
				setState(691);
				match(RIGHT_RBRACKET);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(693);
				match(LEFT_RBRACKET);
				setState(694);
				opt_crlfs();
				setState(695);
				match(RIGHT_RBRACKET);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_call_parameters_no_bracketContext extends ParserRuleContext {
		public Function_call_paramContext function_call_param() {
			return getRuleContext(Function_call_paramContext.class,0);
		}
		public Function_call_param_restContext function_call_param_rest() {
			return getRuleContext(Function_call_param_restContext.class,0);
		}
		public Func_call_parameters_no_bracketContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_call_parameters_no_bracket; }
	}

	public final Func_call_parameters_no_bracketContext func_call_parameters_no_bracket() throws RecognitionException {
		Func_call_parameters_no_bracketContext _localctx = new Func_call_parameters_no_bracketContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_func_call_parameters_no_bracket);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(699);
			function_call_param();
			setState(700);
			function_call_param_rest();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_call_param_restContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public Function_call_paramContext function_call_param() {
			return getRuleContext(Function_call_paramContext.class,0);
		}
		public Function_call_param_restContext function_call_param_rest() {
			return getRuleContext(Function_call_param_restContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Function_call_param_restContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call_param_rest; }
	}

	public final Function_call_param_restContext function_call_param_rest() throws RecognitionException {
		Function_call_param_restContext _localctx = new Function_call_param_restContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_function_call_param_rest);
		try {
			setState(712);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(702);
				match(COMMA);
				setState(703);
				opt_crlfs();
				setState(704);
				function_call_param();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(706);
				match(COMMA);
				setState(707);
				opt_crlfs();
				setState(708);
				function_call_param();
				setState(709);
				function_call_param_rest();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(711);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rescure_paramContext extends ParserRuleContext {
		public Id_symbolContext id_symbol() {
			return getRuleContext(Id_symbolContext.class,0);
		}
		public Hash_assoContext hash_asso() {
			return getRuleContext(Hash_assoContext.class,0);
		}
		public TerminalNode ASSOC() { return getToken(RubyParser.ASSOC, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public Rescure_paramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rescure_param; }
	}

	public final Rescure_paramContext rescure_param() throws RecognitionException {
		Rescure_paramContext _localctx = new Rescure_paramContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_rescure_param);
		try {
			setState(718);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(714);
				id_symbol();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(715);
				hash_asso();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(716);
				match(ASSOC);
				setState(717);
				identifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_rescure_paramContext extends ParserRuleContext {
		public Rescure_paramContext rescure_param() {
			return getRuleContext(Rescure_paramContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_rescure_paramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_rescure_param; }
	}

	public final Opt_rescure_paramContext opt_rescure_param() throws RecognitionException {
		Opt_rescure_paramContext _localctx = new Opt_rescure_paramContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_opt_rescure_param);
		try {
			setState(722);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(720);
				rescure_param();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(721);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_bodyContext extends ParserRuleContext {
		public TerminalNode WHEN() { return getToken(RubyParser.WHEN, 0); }
		public When_condContext when_cond() {
			return getRuleContext(When_condContext.class,0);
		}
		public When_cond_restContext when_cond_rest() {
			return getRuleContext(When_cond_restContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public Else_tailContext else_tail() {
			return getRuleContext(Else_tailContext.class,0);
		}
		public Case_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_body; }
	}

	public final Case_bodyContext case_body() throws RecognitionException {
		Case_bodyContext _localctx = new Case_bodyContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_case_body);
		try {
			setState(731);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case WHEN:
				enterOuterAlt(_localctx, 1);
				{
				setState(724);
				match(WHEN);
				setState(725);
				when_cond();
				setState(726);
				when_cond_rest();
				setState(727);
				then_keyword();
				setState(728);
				statement_list_terms(0);
				}
				break;
			case ELSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(730);
				else_tail();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class When_cond_restContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public When_condContext when_cond() {
			return getRuleContext(When_condContext.class,0);
		}
		public When_cond_restContext when_cond_rest() {
			return getRuleContext(When_cond_restContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public When_cond_restContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_when_cond_rest; }
	}

	public final When_cond_restContext when_cond_rest() throws RecognitionException {
		When_cond_restContext _localctx = new When_cond_restContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_when_cond_rest);
		try {
			setState(740);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(733);
				match(COMMA);
				setState(734);
				when_cond();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(735);
				match(COMMA);
				setState(736);
				when_cond();
				setState(737);
				when_cond_rest();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(739);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_bodysContext extends ParserRuleContext {
		public Case_bodyContext case_body() {
			return getRuleContext(Case_bodyContext.class,0);
		}
		public Case_bodysContext case_bodys() {
			return getRuleContext(Case_bodysContext.class,0);
		}
		public Case_bodysContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_bodys; }
	}

	public final Case_bodysContext case_bodys() throws RecognitionException {
		Case_bodysContext _localctx = new Case_bodysContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_case_bodys);
		try {
			setState(746);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(742);
				case_body();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(743);
				case_body();
				setState(744);
				case_bodys();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_case_bodysContext extends ParserRuleContext {
		public Case_bodysContext case_bodys() {
			return getRuleContext(Case_bodysContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_case_bodysContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_case_bodys; }
	}

	public final Opt_case_bodysContext opt_case_bodys() throws RecognitionException {
		Opt_case_bodysContext _localctx = new Opt_case_bodysContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_opt_case_bodys);
		try {
			setState(750);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ELSE:
			case WHEN:
				enterOuterAlt(_localctx, 1);
				{
				setState(748);
				case_bodys();
				}
				break;
			case END:
				enterOuterAlt(_localctx, 2);
				{
				setState(749);
				none();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class When_condContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public When_condContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_when_cond; }
	}

	public final When_condContext when_cond() throws RecognitionException {
		When_condContext _localctx = new When_condContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_when_cond);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(752);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_tailContext extends ParserRuleContext {
		public TerminalNode ELSIF() { return getToken(RubyParser.ELSIF, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Then_keywordContext then_keyword() {
			return getRuleContext(Then_keywordContext.class,0);
		}
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public Else_tailContext else_tail() {
			return getRuleContext(Else_tailContext.class,0);
		}
		public If_tailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_tail; }
	}

	public final If_tailContext if_tail() throws RecognitionException {
		If_tailContext _localctx = new If_tailContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_if_tail);
		try {
			setState(761);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ELSIF:
				enterOuterAlt(_localctx, 1);
				{
				setState(754);
				match(ELSIF);
				setState(755);
				opt_crlfs();
				setState(756);
				expr(0);
				setState(757);
				then_keyword();
				setState(758);
				statement_list_terms(0);
				}
				break;
			case ELSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(760);
				else_tail();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_tailsContext extends ParserRuleContext {
		public If_tailContext if_tail() {
			return getRuleContext(If_tailContext.class,0);
		}
		public If_tailsContext if_tails() {
			return getRuleContext(If_tailsContext.class,0);
		}
		public If_tailsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_tails; }
	}

	public final If_tailsContext if_tails() throws RecognitionException {
		If_tailsContext _localctx = new If_tailsContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_if_tails);
		try {
			setState(767);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(763);
				if_tail();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(764);
				if_tail();
				setState(765);
				if_tails();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_tailContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(RubyParser.ELSE, 0); }
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public Statement_list_termsContext statement_list_terms() {
			return getRuleContext(Statement_list_termsContext.class,0);
		}
		public Else_tailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_tail; }
	}

	public final Else_tailContext else_tail() throws RecognitionException {
		Else_tailContext _localctx = new Else_tailContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_else_tail);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(769);
			match(ELSE);
			setState(770);
			opt_crlfs();
			setState(771);
			statement_list_terms(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_else_tailContext extends ParserRuleContext {
		public Else_tailContext else_tail() {
			return getRuleContext(Else_tailContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_else_tailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_else_tail; }
	}

	public final Opt_else_tailContext opt_else_tail() throws RecognitionException {
		Opt_else_tailContext _localctx = new Opt_else_tailContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_opt_else_tail);
		try {
			setState(775);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(773);
				else_tail();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(774);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dot_refContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(RubyParser.DOT, 0); }
		public TerminalNode ANDDOT() { return getToken(RubyParser.ANDDOT, 0); }
		public Dot_refContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dot_ref; }
	}

	public final Dot_refContext dot_ref() throws RecognitionException {
		Dot_refContext _localctx = new Dot_refContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_dot_ref);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(777);
			_la = _input.LA(1);
			if ( !(_la==ANDDOT || _la==DOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalOperatorContext extends ParserRuleContext {
		public TerminalNode OR() { return getToken(RubyParser.OR, 0); }
		public TerminalNode AND() { return getToken(RubyParser.AND, 0); }
		public LogicalOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperator; }
	}

	public final LogicalOperatorContext logicalOperator() throws RecognitionException {
		LogicalOperatorContext _localctx = new LogicalOperatorContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_logicalOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(779);
			_la = _input.LA(1);
			if ( !(_la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EqualsOperatorContext extends ParserRuleContext {
		public TerminalNode EQUAL() { return getToken(RubyParser.EQUAL, 0); }
		public TerminalNode NOT_EQUAL() { return getToken(RubyParser.NOT_EQUAL, 0); }
		public TerminalNode EQUAL3() { return getToken(RubyParser.EQUAL3, 0); }
		public EqualsOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalsOperator; }
	}

	public final EqualsOperatorContext equalsOperator() throws RecognitionException {
		EqualsOperatorContext _localctx = new EqualsOperatorContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_equalsOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(781);
			_la = _input.LA(1);
			if ( !(((((_la - 74)) & ~0x3f) == 0 && ((1L << (_la - 74)) & ((1L << (NOT_EQUAL - 74)) | (1L << (EQUAL3 - 74)) | (1L << (EQUAL - 74)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompareOperatorContext extends ParserRuleContext {
		public TerminalNode LESS() { return getToken(RubyParser.LESS, 0); }
		public TerminalNode GREATER() { return getToken(RubyParser.GREATER, 0); }
		public TerminalNode LESS_EQUAL() { return getToken(RubyParser.LESS_EQUAL, 0); }
		public TerminalNode GREATER_EQUAL() { return getToken(RubyParser.GREATER_EQUAL, 0); }
		public CompareOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compareOperator; }
	}

	public final CompareOperatorContext compareOperator() throws RecognitionException {
		CompareOperatorContext _localctx = new CompareOperatorContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_compareOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(783);
			_la = _input.LA(1);
			if ( !(((((_la - 85)) & ~0x3f) == 0 && ((1L << (_la - 85)) & ((1L << (GREATER_EQUAL - 85)) | (1L << (GREATER - 85)) | (1L << (LESS_EQUAL - 85)) | (1L << (LESS - 85)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BitOperatorContext extends ParserRuleContext {
		public TerminalNode BIT_SHL() { return getToken(RubyParser.BIT_SHL, 0); }
		public TerminalNode BIT_SHR() { return getToken(RubyParser.BIT_SHR, 0); }
		public TerminalNode BIT_AND() { return getToken(RubyParser.BIT_AND, 0); }
		public TerminalNode BIT_OR() { return getToken(RubyParser.BIT_OR, 0); }
		public TerminalNode BIT_XOR() { return getToken(RubyParser.BIT_XOR, 0); }
		public BitOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitOperator; }
	}

	public final BitOperatorContext bitOperator() throws RecognitionException {
		BitOperatorContext _localctx = new BitOperatorContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_bitOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(785);
			_la = _input.LA(1);
			if ( !(((((_la - 55)) & ~0x3f) == 0 && ((1L << (_la - 55)) & ((1L << (BIT_AND - 55)) | (1L << (BIT_OR - 55)) | (1L << (BIT_SHR - 55)) | (1L << (BIT_SHL - 55)) | (1L << (BIT_XOR - 55)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MathOperatorContext extends ParserRuleContext {
		public TerminalNode MUL() { return getToken(RubyParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(RubyParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(RubyParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(RubyParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(RubyParser.MINUS, 0); }
		public TerminalNode EXP() { return getToken(RubyParser.EXP, 0); }
		public MathOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mathOperator; }
	}

	public final MathOperatorContext mathOperator() throws RecognitionException {
		MathOperatorContext _localctx = new MathOperatorContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_mathOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(787);
			_la = _input.LA(1);
			if ( !(((((_la - 46)) & ~0x3f) == 0 && ((1L << (_la - 46)) & ((1L << (PLUS - 46)) | (1L << (MINUS - 46)) | (1L << (MOD - 46)) | (1L << (EXP - 46)) | (1L << (MUL - 46)) | (1L << (DIV - 46)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignOperatorContext extends ParserRuleContext {
		public TerminalNode PLUS_ASSIGN() { return getToken(RubyParser.PLUS_ASSIGN, 0); }
		public TerminalNode MINUS_ASSIGN() { return getToken(RubyParser.MINUS_ASSIGN, 0); }
		public TerminalNode MUL_ASSIGN() { return getToken(RubyParser.MUL_ASSIGN, 0); }
		public TerminalNode DIV_ASSIGN() { return getToken(RubyParser.DIV_ASSIGN, 0); }
		public TerminalNode MOD_ASSIGN() { return getToken(RubyParser.MOD_ASSIGN, 0); }
		public TerminalNode EXP_ASSIGN() { return getToken(RubyParser.EXP_ASSIGN, 0); }
		public TerminalNode BIT_OR_ASSIGN() { return getToken(RubyParser.BIT_OR_ASSIGN, 0); }
		public TerminalNode BIT_AND_ASSIGN() { return getToken(RubyParser.BIT_AND_ASSIGN, 0); }
		public TerminalNode OR_ASSIGN() { return getToken(RubyParser.OR_ASSIGN, 0); }
		public TerminalNode AND_ASSIGN() { return getToken(RubyParser.AND_ASSIGN, 0); }
		public TerminalNode BIT_XOR_ASSIGN() { return getToken(RubyParser.BIT_XOR_ASSIGN, 0); }
		public TerminalNode BIT_NOT_ASSIGN() { return getToken(RubyParser.BIT_NOT_ASSIGN, 0); }
		public TerminalNode BIT_SHL_ASSIGN() { return getToken(RubyParser.BIT_SHL_ASSIGN, 0); }
		public TerminalNode BIT_SHR_ASSIGN() { return getToken(RubyParser.BIT_SHR_ASSIGN, 0); }
		public TerminalNode ASSIGN() { return getToken(RubyParser.ASSIGN, 0); }
		public AssignOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignOperator; }
	}

	public final AssignOperatorContext assignOperator() throws RecognitionException {
		AssignOperatorContext _localctx = new AssignOperatorContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_assignOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(789);
			_la = _input.LA(1);
			if ( !(((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & ((1L << (PLUS_ASSIGN - 49)) | (1L << (MOD_ASSIGN - 49)) | (1L << (AND_ASSIGN - 49)) | (1L << (BIT_AND_ASSIGN - 49)) | (1L << (EXP_ASSIGN - 49)) | (1L << (MUL_ASSIGN - 49)) | (1L << (DIV_ASSIGN - 49)) | (1L << (MINUS_ASSIGN - 49)) | (1L << (OR_ASSIGN - 49)) | (1L << (BIT_OR_ASSIGN - 49)) | (1L << (BIT_SHR_ASSIGN - 49)) | (1L << (ASSIGN - 49)) | (1L << (BIT_SHL_ASSIGN - 49)) | (1L << (BIT_NOT_ASSIGN - 49)) | (1L << (BIT_XOR_ASSIGN - 49)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotSymContext extends ParserRuleContext {
		public TerminalNode NOT() { return getToken(RubyParser.NOT, 0); }
		public TerminalNode SIGH() { return getToken(RubyParser.SIGH, 0); }
		public NotSymContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notSym; }
	}

	public final NotSymContext notSym() throws RecognitionException {
		NotSymContext _localctx = new NotSymContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_notSym);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(791);
			_la = _input.LA(1);
			if ( !(_la==NOT || _la==SIGH) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode LEFT_PAREN() { return getToken(RubyParser.LEFT_PAREN, 0); }
		public List<Opt_crlfsContext> opt_crlfs() {
			return getRuleContexts(Opt_crlfsContext.class);
		}
		public Opt_crlfsContext opt_crlfs(int i) {
			return getRuleContext(Opt_crlfsContext.class,i);
		}
		public Opt_block_paramsContext opt_block_params() {
			return getRuleContext(Opt_block_paramsContext.class,0);
		}
		public Statement_list_notermsContext statement_list_noterms() {
			return getRuleContext(Statement_list_notermsContext.class,0);
		}
		public Block_stmt_listContext block_stmt_list() {
			return getRuleContext(Block_stmt_listContext.class,0);
		}
		public TerminalNode RIGHT_PAREN() { return getToken(RubyParser.RIGHT_PAREN, 0); }
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(793);
			match(LEFT_PAREN);
			setState(794);
			opt_crlfs();
			setState(795);
			opt_block_params();
			setState(796);
			statement_list_noterms();
			setState(797);
			opt_crlfs();
			setState(798);
			block_stmt_list();
			setState(799);
			match(RIGHT_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_stmt_listContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public Statement_list_notermsContext statement_list_noterms() {
			return getRuleContext(Statement_list_notermsContext.class,0);
		}
		public Opt_crlfsContext opt_crlfs() {
			return getRuleContext(Opt_crlfsContext.class,0);
		}
		public Block_stmt_listContext block_stmt_list() {
			return getRuleContext(Block_stmt_listContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Block_stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_stmt_list; }
	}

	public final Block_stmt_listContext block_stmt_list() throws RecognitionException {
		Block_stmt_listContext _localctx = new Block_stmt_listContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_block_stmt_list);
		try {
			setState(811);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(801);
				match(COMMA);
				setState(802);
				statement_list_noterms();
				setState(803);
				opt_crlfs();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(805);
				match(COMMA);
				setState(806);
				statement_list_noterms();
				setState(807);
				opt_crlfs();
				setState(808);
				block_stmt_list();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(810);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_paramsContext extends ParserRuleContext {
		public List<TerminalNode> BIT_OR() { return getTokens(RubyParser.BIT_OR); }
		public TerminalNode BIT_OR(int i) {
			return getToken(RubyParser.BIT_OR, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Expr_block_params_listContext expr_block_params_list() {
			return getRuleContext(Expr_block_params_listContext.class,0);
		}
		public Block_paramsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_params; }
	}

	public final Block_paramsContext block_params() throws RecognitionException {
		Block_paramsContext _localctx = new Block_paramsContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_block_params);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(813);
			match(BIT_OR);
			setState(814);
			expr(0);
			setState(815);
			expr_block_params_list();
			setState(816);
			match(BIT_OR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_block_paramsContext extends ParserRuleContext {
		public Block_paramsContext block_params() {
			return getRuleContext(Block_paramsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_block_paramsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_block_params; }
	}

	public final Opt_block_paramsContext opt_block_params() throws RecognitionException {
		Opt_block_paramsContext _localctx = new Opt_block_paramsContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_opt_block_params);
		try {
			setState(820);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(818);
				block_params();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(819);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_block_params_listContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Expr_block_params_listContext expr_block_params_list() {
			return getRuleContext(Expr_block_params_listContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Expr_block_params_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_block_params_list; }
	}

	public final Expr_block_params_listContext expr_block_params_list() throws RecognitionException {
		Expr_block_params_listContext _localctx = new Expr_block_params_listContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_expr_block_params_list);
		try {
			setState(829);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,44,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(822);
				match(COMMA);
				setState(823);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(824);
				match(COMMA);
				setState(825);
				expr(0);
				setState(826);
				expr_block_params_list();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(828);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Id_symbolContext extends ParserRuleContext {
		public CpathContext cpath() {
			return getRuleContext(CpathContext.class,0);
		}
		public TerminalNode COLON() { return getToken(RubyParser.COLON, 0); }
		public Id_symbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id_symbol; }
	}

	public final Id_symbolContext id_symbol() throws RecognitionException {
		Id_symbolContext _localctx = new Id_symbolContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_id_symbol);
		try {
			setState(834);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BREAK:
			case NEXT:
			case NIL:
			case REDO:
			case RETRY:
			case SELF:
			case SUPER:
			case DollarSpecial:
			case DOLLAR:
			case AT:
			case LEFT_PAREN:
			case LEFT_SBRACKET:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(831);
				cpath();
				}
				break;
			case COLON:
				enterOuterAlt(_localctx, 2);
				{
				setState(832);
				match(COLON);
				setState(833);
				cpath();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolContext extends ParserRuleContext {
		public TerminalNode COLON() { return getToken(RubyParser.COLON, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public SymbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbol; }
	}

	public final SymbolContext symbol() throws RecognitionException {
		SymbolContext _localctx = new SymbolContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_symbol);
		try {
			setState(842);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(836);
				match(COLON);
				setState(837);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(838);
				match(COLON);
				setState(839);
				string();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(840);
				match(COLON);
				setState(841);
				function_name();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Hash_assoContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASSOC() { return getToken(RubyParser.ASSOC, 0); }
		public TerminalNode COLON() { return getToken(RubyParser.COLON, 0); }
		public Hash_assoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hash_asso; }
	}

	public final Hash_assoContext hash_asso() throws RecognitionException {
		Hash_assoContext _localctx = new Hash_assoContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_hash_asso);
		try {
			setState(852);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(844);
				expr(0);
				setState(845);
				match(ASSOC);
				setState(846);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(848);
				expr(0);
				setState(849);
				match(COLON);
				setState(850);
				expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_pathContext extends ParserRuleContext {
		public Variable_pathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_path; }
	 
		public Variable_pathContext() { }
		public void copyFrom(Variable_pathContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VarPathLiteralContext extends Variable_pathContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public VarPathLiteralContext(Variable_pathContext ctx) { copyFrom(ctx); }
	}
	public static class VarPathIdContext extends Variable_pathContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public VarPathIdContext(Variable_pathContext ctx) { copyFrom(ctx); }
	}
	public static class VarPathGlobalPrefixContext extends Variable_pathContext {
		public TerminalNode COLON2() { return getToken(RubyParser.COLON2, 0); }
		public Variable_pathContext variable_path() {
			return getRuleContext(Variable_pathContext.class,0);
		}
		public VarPathGlobalPrefixContext(Variable_pathContext ctx) { copyFrom(ctx); }
	}
	public static class VarPathJoinContext extends Variable_pathContext {
		public Variable_pathContext variable_path() {
			return getRuleContext(Variable_pathContext.class,0);
		}
		public TerminalNode COLON2() { return getToken(RubyParser.COLON2, 0); }
		public Id_or_literalContext id_or_literal() {
			return getRuleContext(Id_or_literalContext.class,0);
		}
		public VarPathJoinContext(Variable_pathContext ctx) { copyFrom(ctx); }
	}

	public final Variable_pathContext variable_path() throws RecognitionException {
		return variable_path(0);
	}

	private Variable_pathContext variable_path(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Variable_pathContext _localctx = new Variable_pathContext(_ctx, _parentState);
		Variable_pathContext _prevctx = _localctx;
		int _startState = 138;
		enterRecursionRule(_localctx, 138, RULE_variable_path, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(859);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
			case 1:
				{
				_localctx = new VarPathIdContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(855);
				identifier();
				}
				break;
			case 2:
				{
				_localctx = new VarPathLiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(856);
				literal();
				}
				break;
			case 3:
				{
				_localctx = new VarPathGlobalPrefixContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(857);
				match(COLON2);
				setState(858);
				variable_path(1);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(866);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new VarPathJoinContext(new Variable_pathContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_variable_path);
					setState(861);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(862);
					match(COLON2);
					setState(863);
					id_or_literal();
					}
					} 
				}
				setState(868);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class CpathContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public Cpath_suffixsContext cpath_suffixs() {
			return getRuleContext(Cpath_suffixsContext.class,0);
		}
		public CpathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cpath; }
	}

	public final CpathContext cpath() throws RecognitionException {
		CpathContext _localctx = new CpathContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_cpath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(869);
			identifier();
			setState(870);
			cpath_suffixs();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode Float() { return getToken(RubyParser.Float, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode Integer() { return getToken(RubyParser.Integer, 0); }
		public TerminalNode TRUE() { return getToken(RubyParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(RubyParser.FALSE, 0); }
		public TerminalNode NIL() { return getToken(RubyParser.NIL, 0); }
		public TerminalNode DOT() { return getToken(RubyParser.DOT, 0); }
		public TerminalNode ShellCommand() { return getToken(RubyParser.ShellCommand, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_literal);
		try {
			setState(881);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(872);
				match(Float);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(873);
				string();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(874);
				match(Integer);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(875);
				match(TRUE);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(876);
				match(FALSE);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(877);
				match(NIL);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(878);
				match(Float);
				setState(879);
				match(DOT);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(880);
				match(ShellCommand);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Id_or_literalContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public Id_or_literalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id_or_literal; }
	}

	public final Id_or_literalContext id_or_literal() throws RecognitionException {
		Id_or_literalContext _localctx = new Id_or_literalContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_id_or_literal);
		try {
			setState(885);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(883);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(884);
				literal();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cpath_suffixContext extends ParserRuleContext {
		public TerminalNode COLON2() { return getToken(RubyParser.COLON2, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode DOT() { return getToken(RubyParser.DOT, 0); }
		public Cpath_suffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cpath_suffix; }
	}

	public final Cpath_suffixContext cpath_suffix() throws RecognitionException {
		Cpath_suffixContext _localctx = new Cpath_suffixContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_cpath_suffix);
		try {
			setState(891);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COLON2:
				enterOuterAlt(_localctx, 1);
				{
				setState(887);
				match(COLON2);
				setState(888);
				identifier();
				}
				break;
			case DOT:
				enterOuterAlt(_localctx, 2);
				{
				setState(889);
				match(DOT);
				setState(890);
				identifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cpath_suffixsContext extends ParserRuleContext {
		public Cpath_suffixContext cpath_suffix() {
			return getRuleContext(Cpath_suffixContext.class,0);
		}
		public Cpath_suffixsContext cpath_suffixs() {
			return getRuleContext(Cpath_suffixsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Cpath_suffixsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cpath_suffixs; }
	}

	public final Cpath_suffixsContext cpath_suffixs() throws RecognitionException {
		Cpath_suffixsContext _localctx = new Cpath_suffixsContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_cpath_suffixs);
		try {
			setState(898);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(893);
				cpath_suffix();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(894);
				cpath_suffix();
				setState(895);
				cpath_suffixs();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(897);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(RubyParser.Identifier, 0); }
		public GlobalVarContext globalVar() {
			return getRuleContext(GlobalVarContext.class,0);
		}
		public ClassVarContext classVar() {
			return getRuleContext(ClassVarContext.class,0);
		}
		public InstanceVarContext instanceVar() {
			return getRuleContext(InstanceVarContext.class,0);
		}
		public IdArgContext idArg() {
			return getRuleContext(IdArgContext.class,0);
		}
		public TerminalNode NEXT() { return getToken(RubyParser.NEXT, 0); }
		public TerminalNode REDO() { return getToken(RubyParser.REDO, 0); }
		public TerminalNode RETRY() { return getToken(RubyParser.RETRY, 0); }
		public TerminalNode BREAK() { return getToken(RubyParser.BREAK, 0); }
		public TerminalNode SELF() { return getToken(RubyParser.SELF, 0); }
		public TerminalNode SUPER() { return getToken(RubyParser.SUPER, 0); }
		public TerminalNode NIL() { return getToken(RubyParser.NIL, 0); }
		public EmptyContext empty() {
			return getRuleContext(EmptyContext.class,0);
		}
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_identifier);
		try {
			setState(913);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(900);
				match(Identifier);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(901);
				globalVar();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(902);
				classVar();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(903);
				instanceVar();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(904);
				idArg();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(905);
				match(NEXT);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(906);
				match(REDO);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(907);
				match(RETRY);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(908);
				match(BREAK);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(909);
				match(SELF);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(910);
				match(SUPER);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(911);
				match(NIL);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(912);
				empty();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EmptyContext extends ParserRuleContext {
		public TerminalNode LEFT_PAREN() { return getToken(RubyParser.LEFT_PAREN, 0); }
		public TerminalNode RIGHT_PAREN() { return getToken(RubyParser.RIGHT_PAREN, 0); }
		public TerminalNode LEFT_SBRACKET() { return getToken(RubyParser.LEFT_SBRACKET, 0); }
		public TerminalNode RIGHT_SBRACKET() { return getToken(RubyParser.RIGHT_SBRACKET, 0); }
		public EmptyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_empty; }
	}

	public final EmptyContext empty() throws RecognitionException {
		EmptyContext _localctx = new EmptyContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_empty);
		try {
			setState(919);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LEFT_PAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(915);
				match(LEFT_PAREN);
				setState(916);
				match(RIGHT_PAREN);
				}
				break;
			case LEFT_SBRACKET:
				enterOuterAlt(_localctx, 2);
				{
				setState(917);
				match(LEFT_SBRACKET);
				setState(918);
				match(RIGHT_SBRACKET);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GlobalVarContext extends ParserRuleContext {
		public TerminalNode DOLLAR() { return getToken(RubyParser.DOLLAR, 0); }
		public TerminalNode Identifier() { return getToken(RubyParser.Identifier, 0); }
		public GlobalVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_globalVar; }
	}

	public final GlobalVarContext globalVar() throws RecognitionException {
		GlobalVarContext _localctx = new GlobalVarContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_globalVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(921);
			match(DOLLAR);
			setState(922);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassVarContext extends ParserRuleContext {
		public List<TerminalNode> AT() { return getTokens(RubyParser.AT); }
		public TerminalNode AT(int i) {
			return getToken(RubyParser.AT, i);
		}
		public TerminalNode Identifier() { return getToken(RubyParser.Identifier, 0); }
		public ClassVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classVar; }
	}

	public final ClassVarContext classVar() throws RecognitionException {
		ClassVarContext _localctx = new ClassVarContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_classVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(924);
			match(AT);
			setState(925);
			match(AT);
			setState(926);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstanceVarContext extends ParserRuleContext {
		public TerminalNode AT() { return getToken(RubyParser.AT, 0); }
		public TerminalNode Identifier() { return getToken(RubyParser.Identifier, 0); }
		public InstanceVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instanceVar; }
	}

	public final InstanceVarContext instanceVar() throws RecognitionException {
		InstanceVarContext _localctx = new InstanceVarContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_instanceVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(928);
			match(AT);
			setState(929);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdArgContext extends ParserRuleContext {
		public TerminalNode DOLLAR() { return getToken(RubyParser.DOLLAR, 0); }
		public TerminalNode Integer() { return getToken(RubyParser.Integer, 0); }
		public TerminalNode DollarSpecial() { return getToken(RubyParser.DollarSpecial, 0); }
		public IdArgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idArg; }
	}

	public final IdArgContext idArg() throws RecognitionException {
		IdArgContext _localctx = new IdArgContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_idArg);
		try {
			setState(934);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOLLAR:
				enterOuterAlt(_localctx, 1);
				{
				setState(931);
				match(DOLLAR);
				setState(932);
				match(Integer);
				}
				break;
			case DollarSpecial:
				enterOuterAlt(_localctx, 2);
				{
				setState(933);
				match(DollarSpecial);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Do_keywordContext extends ParserRuleContext {
		public TerminalNode DO() { return getToken(RubyParser.DO, 0); }
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public TerminalNode COLON() { return getToken(RubyParser.COLON, 0); }
		public Do_keywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_do_keyword; }
	}

	public final Do_keywordContext do_keyword() throws RecognitionException {
		Do_keywordContext _localctx = new Do_keywordContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_do_keyword);
		try {
			setState(941);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DO:
				enterOuterAlt(_localctx, 1);
				{
				setState(936);
				match(DO);
				setState(937);
				terms();
				}
				break;
			case COLON:
				enterOuterAlt(_localctx, 2);
				{
				setState(938);
				match(COLON);
				setState(939);
				terms();
				}
				break;
			case CRLF:
			case SEMICOLON:
			case SL_COMMENT:
				enterOuterAlt(_localctx, 3);
				{
				setState(940);
				terms();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Then_keywordContext extends ParserRuleContext {
		public TerminalNode THEN() { return getToken(RubyParser.THEN, 0); }
		public Opt_termsContext opt_terms() {
			return getRuleContext(Opt_termsContext.class,0);
		}
		public TerminalNode COLON() { return getToken(RubyParser.COLON, 0); }
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public Then_keywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_then_keyword; }
	}

	public final Then_keywordContext then_keyword() throws RecognitionException {
		Then_keywordContext _localctx = new Then_keywordContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_then_keyword);
		try {
			setState(948);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case THEN:
				enterOuterAlt(_localctx, 1);
				{
				setState(943);
				match(THEN);
				setState(944);
				opt_terms();
				}
				break;
			case COLON:
				enterOuterAlt(_localctx, 2);
				{
				setState(945);
				match(COLON);
				setState(946);
				opt_terms();
				}
				break;
			case CRLF:
			case SEMICOLON:
			case SL_COMMENT:
				enterOuterAlt(_localctx, 3);
				{
				setState(947);
				terms();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public TerminalNode String() { return getToken(RubyParser.String, 0); }
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_string);
		try {
			setState(953);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,59,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(950);
				match(String);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(951);
				match(String);
				setState(952);
				string();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CrlfContext extends ParserRuleContext {
		public TerminalNode SL_COMMENT() { return getToken(RubyParser.SL_COMMENT, 0); }
		public TerminalNode CRLF() { return getToken(RubyParser.CRLF, 0); }
		public CrlfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_crlf; }
	}

	public final CrlfContext crlf() throws RecognitionException {
		CrlfContext _localctx = new CrlfContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_crlf);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(955);
			_la = _input.LA(1);
			if ( !(_la==CRLF || _la==SL_COMMENT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CrlfsContext extends ParserRuleContext {
		public CrlfContext crlf() {
			return getRuleContext(CrlfContext.class,0);
		}
		public CrlfsContext crlfs() {
			return getRuleContext(CrlfsContext.class,0);
		}
		public CrlfsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_crlfs; }
	}

	public final CrlfsContext crlfs() throws RecognitionException {
		CrlfsContext _localctx = new CrlfsContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_crlfs);
		try {
			setState(961);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(957);
				crlf();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(958);
				crlf();
				setState(959);
				crlfs();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_crlfsContext extends ParserRuleContext {
		public CrlfsContext crlfs() {
			return getRuleContext(CrlfsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_crlfsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_crlfs; }
	}

	public final Opt_crlfsContext opt_crlfs() throws RecognitionException {
		Opt_crlfsContext _localctx = new Opt_crlfsContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_opt_crlfs);
		try {
			setState(965);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(963);
				crlfs();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(964);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_commaContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RubyParser.COMMA, 0); }
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_commaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_comma; }
	}

	public final Opt_commaContext opt_comma() throws RecognitionException {
		Opt_commaContext _localctx = new Opt_commaContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_opt_comma);
		try {
			setState(969);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COMMA:
				enterOuterAlt(_localctx, 1);
				{
				setState(967);
				match(COMMA);
				}
				break;
			case CRLF:
			case MUL:
			case RIGHT_PAREN:
			case RIGHT_SBRACKET:
			case ASSIGN:
			case SL_COMMENT:
				enterOuterAlt(_localctx, 2);
				{
				setState(968);
				none();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_mulContext extends ParserRuleContext {
		public TerminalNode MUL() { return getToken(RubyParser.MUL, 0); }
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_mulContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_mul; }
	}

	public final Opt_mulContext opt_mul() throws RecognitionException {
		Opt_mulContext _localctx = new Opt_mulContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_opt_mul);
		try {
			setState(973);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MUL:
				enterOuterAlt(_localctx, 1);
				{
				setState(971);
				match(MUL);
				}
				break;
			case ASSIGN:
				enterOuterAlt(_localctx, 2);
				{
				setState(972);
				none();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermsContext extends ParserRuleContext {
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public TermsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_terms; }
	}

	public final TermsContext terms() throws RecognitionException {
		TermsContext _localctx = new TermsContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_terms);
		try {
			setState(979);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,64,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(975);
				term();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(976);
				term();
				setState(977);
				terms();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public TerminalNode SEMICOLON() { return getToken(RubyParser.SEMICOLON, 0); }
		public TerminalNode CRLF() { return getToken(RubyParser.CRLF, 0); }
		public TerminalNode SL_COMMENT() { return getToken(RubyParser.SL_COMMENT, 0); }
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(981);
			_la = _input.LA(1);
			if ( !(((((_la - 43)) & ~0x3f) == 0 && ((1L << (_la - 43)) & ((1L << (CRLF - 43)) | (1L << (SEMICOLON - 43)) | (1L << (SL_COMMENT - 43)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Opt_termsContext extends ParserRuleContext {
		public TermsContext terms() {
			return getRuleContext(TermsContext.class,0);
		}
		public NoneContext none() {
			return getRuleContext(NoneContext.class,0);
		}
		public Opt_termsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opt_terms; }
	}

	public final Opt_termsContext opt_terms() throws RecognitionException {
		Opt_termsContext _localctx = new Opt_termsContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_opt_terms);
		try {
			setState(985);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(983);
				terms();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(984);
				none();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NoneContext extends ParserRuleContext {
		public NoneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_none; }
	}

	public final NoneContext none() throws RecognitionException {
		NoneContext _localctx = new NoneContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_none);
		try {
			enterOuterAlt(_localctx, 1);
			{
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return statement_list_terms_sempred((Statement_list_termsContext)_localctx, predIndex);
		case 27:
			return expr_sempred((ExprContext)_localctx, predIndex);
		case 69:
			return variable_path_sempred((Variable_pathContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean statement_list_terms_sempred(Statement_list_termsContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 24);
		case 2:
			return precpred(_ctx, 23);
		case 3:
			return precpred(_ctx, 17);
		case 4:
			return precpred(_ctx, 16);
		case 5:
			return precpred(_ctx, 15);
		case 6:
			return precpred(_ctx, 14);
		case 7:
			return precpred(_ctx, 13);
		case 8:
			return precpred(_ctx, 11);
		case 9:
			return precpred(_ctx, 10);
		case 10:
			return precpred(_ctx, 9);
		case 11:
			return precpred(_ctx, 8);
		case 12:
			return precpred(_ctx, 7);
		case 13:
			return precpred(_ctx, 22);
		case 14:
			return precpred(_ctx, 20);
		case 15:
			return precpred(_ctx, 18);
		case 16:
			return precpred(_ctx, 6);
		case 17:
			return precpred(_ctx, 5);
		case 18:
			return precpred(_ctx, 4);
		case 19:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean variable_path_sempred(Variable_pathContext _localctx, int predIndex) {
		switch (predIndex) {
		case 20:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3k\u03e0\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\3\2\3\2"+
		"\3\3\3\3\3\3\3\3\3\3\5\3\u00c4\n\3\3\3\3\3\3\3\3\3\7\3\u00ca\n\3\f\3\16"+
		"\3\u00cd\13\3\3\4\3\4\5\4\u00d1\n\4\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\5\6\u00de\n\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00e6\n\7\3\b\3\b"+
		"\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00f3\n\t\3\n\3\n\3\n\3\n\3\13"+
		"\3\13\5\13\u00fb\n\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u0110\n\16\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\5\17\u0119\n\17\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\5\21\u0128\n\21\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\5\22\u0131\n\22\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\5\24\u013b"+
		"\n\24\3\25\3\25\5\25\u013f\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\5\26\u014d\n\26\3\27\3\27\3\27\3\27\5\27\u0153\n"+
		"\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u015c\n\30\3\31\3\31\3\31"+
		"\3\32\3\32\3\32\3\32\3\32\5\32\u0166\n\32\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\33\5\33\u0174\n\33\3\34\3\34\5\34\u0178\n"+
		"\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3"+
		"\35\3\35\3\35\5\35\u018a\n\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\7\35\u01de\n\35\f\35\16\35\u01e1\13\35\3\36\3\36"+
		"\5\36\u01e5\n\36\3\37\3\37\5\37\u01e9\n\37\3 \3 \3!\3!\3!\3!\3!\3!\3!"+
		"\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\5!\u0205\n!\3\"\3"+
		"\"\5\"\u0209\n\"\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3"+
		"#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3"+
		"#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3"+
		"#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3"+
		"#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3"+
		"#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\5#\u0288\n#\3$\3$\3$\3"+
		"$\3$\3$\3$\3$\3$\3$\5$\u0294\n$\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\5%\u02a0"+
		"\n%\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\5\'\u02aa\n\'\3(\3(\3(\5(\u02af\n(\3"+
		")\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\5)\u02bc\n)\3*\3*\3*\3+\3+\3+\3+\3+\3"+
		"+\3+\3+\3+\3+\5+\u02cb\n+\3,\3,\3,\3,\5,\u02d1\n,\3-\3-\5-\u02d5\n-\3"+
		".\3.\3.\3.\3.\3.\3.\5.\u02de\n.\3/\3/\3/\3/\3/\3/\3/\5/\u02e7\n/\3\60"+
		"\3\60\3\60\3\60\5\60\u02ed\n\60\3\61\3\61\5\61\u02f1\n\61\3\62\3\62\3"+
		"\63\3\63\3\63\3\63\3\63\3\63\3\63\5\63\u02fc\n\63\3\64\3\64\3\64\3\64"+
		"\5\64\u0302\n\64\3\65\3\65\3\65\3\65\3\66\3\66\5\66\u030a\n\66\3\67\3"+
		"\67\38\38\39\39\3:\3:\3;\3;\3<\3<\3=\3=\3>\3>\3?\3?\3?\3?\3?\3?\3?\3?"+
		"\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\5@\u032e\n@\3A\3A\3A\3A\3A\3B\3B\5B\u0337"+
		"\nB\3C\3C\3C\3C\3C\3C\3C\5C\u0340\nC\3D\3D\3D\5D\u0345\nD\3E\3E\3E\3E"+
		"\3E\3E\5E\u034d\nE\3F\3F\3F\3F\3F\3F\3F\3F\5F\u0357\nF\3G\3G\3G\3G\3G"+
		"\5G\u035e\nG\3G\3G\3G\7G\u0363\nG\fG\16G\u0366\13G\3H\3H\3H\3I\3I\3I\3"+
		"I\3I\3I\3I\3I\3I\5I\u0374\nI\3J\3J\5J\u0378\nJ\3K\3K\3K\3K\5K\u037e\n"+
		"K\3L\3L\3L\3L\3L\5L\u0385\nL\3M\3M\3M\3M\3M\3M\3M\3M\3M\3M\3M\3M\3M\5"+
		"M\u0394\nM\3N\3N\3N\3N\5N\u039a\nN\3O\3O\3O\3P\3P\3P\3P\3Q\3Q\3Q\3R\3"+
		"R\3R\5R\u03a9\nR\3S\3S\3S\3S\3S\5S\u03b0\nS\3T\3T\3T\3T\3T\5T\u03b7\n"+
		"T\3U\3U\3U\5U\u03bc\nU\3V\3V\3W\3W\3W\3W\5W\u03c4\nW\3X\3X\5X\u03c8\n"+
		"X\3Y\3Y\5Y\u03cc\nY\3Z\3Z\5Z\u03d0\nZ\3[\3[\3[\3[\5[\u03d6\n[\3\\\3\\"+
		"\3]\3]\5]\u03dc\n]\3^\3^\3^\2\5\48\u008c_\2\4\6\b\n\f\16\20\22\24\26\30"+
		"\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080"+
		"\u0082\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098"+
		"\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0"+
		"\u00b2\u00b4\u00b6\u00b8\u00ba\2\17\5\2\60\6299<<\3\2FG\4\2KKMM\4\288"+
		"HH\3\2./\4\2LL\\]\5\2WWYY_`\7\299UUXXbbff\5\2\60\62;<JJ\r\2\63\64\66\67"+
		"::==IIRTVVZZaaccee\4\2\30\30MM\4\2--gg\5\2--PPgg\2\u0428\2\u00bc\3\2\2"+
		"\2\4\u00c3\3\2\2\2\6\u00d0\3\2\2\2\b\u00d2\3\2\2\2\n\u00dd\3\2\2\2\f\u00e5"+
		"\3\2\2\2\16\u00e7\3\2\2\2\20\u00f2\3\2\2\2\22\u00f4\3\2\2\2\24\u00fa\3"+
		"\2\2\2\26\u00fc\3\2\2\2\30\u0101\3\2\2\2\32\u010f\3\2\2\2\34\u0118\3\2"+
		"\2\2\36\u011a\3\2\2\2 \u0127\3\2\2\2\"\u0130\3\2\2\2$\u0132\3\2\2\2&\u013a"+
		"\3\2\2\2(\u013e\3\2\2\2*\u014c\3\2\2\2,\u0152\3\2\2\2.\u015b\3\2\2\2\60"+
		"\u015d\3\2\2\2\62\u0165\3\2\2\2\64\u0173\3\2\2\2\66\u0177\3\2\2\28\u0189"+
		"\3\2\2\2:\u01e4\3\2\2\2<\u01e8\3\2\2\2>\u01ea\3\2\2\2@\u0204\3\2\2\2B"+
		"\u0208\3\2\2\2D\u0287\3\2\2\2F\u0293\3\2\2\2H\u029f\3\2\2\2J\u02a1\3\2"+
		"\2\2L\u02a9\3\2\2\2N\u02ae\3\2\2\2P\u02bb\3\2\2\2R\u02bd\3\2\2\2T\u02ca"+
		"\3\2\2\2V\u02d0\3\2\2\2X\u02d4\3\2\2\2Z\u02dd\3\2\2\2\\\u02e6\3\2\2\2"+
		"^\u02ec\3\2\2\2`\u02f0\3\2\2\2b\u02f2\3\2\2\2d\u02fb\3\2\2\2f\u0301\3"+
		"\2\2\2h\u0303\3\2\2\2j\u0309\3\2\2\2l\u030b\3\2\2\2n\u030d\3\2\2\2p\u030f"+
		"\3\2\2\2r\u0311\3\2\2\2t\u0313\3\2\2\2v\u0315\3\2\2\2x\u0317\3\2\2\2z"+
		"\u0319\3\2\2\2|\u031b\3\2\2\2~\u032d\3\2\2\2\u0080\u032f\3\2\2\2\u0082"+
		"\u0336\3\2\2\2\u0084\u033f\3\2\2\2\u0086\u0344\3\2\2\2\u0088\u034c\3\2"+
		"\2\2\u008a\u0356\3\2\2\2\u008c\u035d\3\2\2\2\u008e\u0367\3\2\2\2\u0090"+
		"\u0373\3\2\2\2\u0092\u0377\3\2\2\2\u0094\u037d\3\2\2\2\u0096\u0384\3\2"+
		"\2\2\u0098\u0393\3\2\2\2\u009a\u0399\3\2\2\2\u009c\u039b\3\2\2\2\u009e"+
		"\u039e\3\2\2\2\u00a0\u03a2\3\2\2\2\u00a2\u03a8\3\2\2\2\u00a4\u03af\3\2"+
		"\2\2\u00a6\u03b6\3\2\2\2\u00a8\u03bb\3\2\2\2\u00aa\u03bd\3\2\2\2\u00ac"+
		"\u03c3\3\2\2\2\u00ae\u03c7\3\2\2\2\u00b0\u03cb\3\2\2\2\u00b2\u03cf\3\2"+
		"\2\2\u00b4\u03d5\3\2\2\2\u00b6\u03d7\3\2\2\2\u00b8\u03db\3\2\2\2\u00ba"+
		"\u03dd\3\2\2\2\u00bc\u00bd\5\4\3\2\u00bd\3\3\2\2\2\u00be\u00bf\b\3\1\2"+
		"\u00bf\u00c0\5\f\7\2\u00c0\u00c1\5\u00b4[\2\u00c1\u00c4\3\2\2\2\u00c2"+
		"\u00c4\5\u00b4[\2\u00c3\u00be\3\2\2\2\u00c3\u00c2\3\2\2\2\u00c4\u00cb"+
		"\3\2\2\2\u00c5\u00c6\f\4\2\2\u00c6\u00c7\5\f\7\2\u00c7\u00c8\5\u00b4["+
		"\2\u00c8\u00ca\3\2\2\2\u00c9\u00c5\3\2\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00c9"+
		"\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\5\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce"+
		"\u00d1\5\4\3\2\u00cf\u00d1\5\u00ba^\2\u00d0\u00ce\3\2\2\2\u00d0\u00cf"+
		"\3\2\2\2\u00d1\7\3\2\2\2\u00d2\u00d3\5\n\6\2\u00d3\u00d4\5\f\7\2\u00d4"+
		"\t\3\2\2\2\u00d5\u00d6\5\f\7\2\u00d6\u00d7\5\u00b4[\2\u00d7\u00de\3\2"+
		"\2\2\u00d8\u00d9\5\f\7\2\u00d9\u00da\5\u00b4[\2\u00da\u00db\5\n\6\2\u00db"+
		"\u00de\3\2\2\2\u00dc\u00de\5\u00ba^\2\u00dd\u00d5\3\2\2\2\u00dd\u00d8"+
		"\3\2\2\2\u00dd\u00dc\3\2\2\2\u00de\13\3\2\2\2\u00df\u00e6\5\26\f\2\u00e0"+
		"\u00e6\5\30\r\2\u00e1\u00e6\5\22\n\2\u00e2\u00e6\5\16\b\2\u00e3\u00e6"+
		"\7\20\2\2\u00e4\u00e6\58\35\2\u00e5\u00df\3\2\2\2\u00e5\u00e0\3\2\2\2"+
		"\u00e5\u00e1\3\2\2\2\u00e5\u00e2\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e5\u00e4"+
		"\3\2\2\2\u00e6\r\3\2\2\2\u00e7\u00e8\7\"\2\2\u00e8\u00e9\5\24\13\2\u00e9"+
		"\u00ea\5\20\t\2\u00ea\17\3\2\2\2\u00eb\u00ec\7Q\2\2\u00ec\u00f3\5\24\13"+
		"\2\u00ed\u00ee\7Q\2\2\u00ee\u00ef\5\24\13\2\u00ef\u00f0\5\20\t\2\u00f0"+
		"\u00f3\3\2\2\2\u00f1\u00f3\5\u00ba^\2\u00f2\u00eb\3\2\2\2\u00f2\u00ed"+
		"\3\2\2\2\u00f2\u00f1\3\2\2\2\u00f3\21\3\2\2\2\u00f4\u00f5\7\3\2\2\u00f5"+
		"\u00f6\5\24\13\2\u00f6\u00f7\5\24\13\2\u00f7\23\3\2\2\2\u00f8\u00fb\5"+
		"*\26\2\u00f9\u00fb\5\u0088E\2\u00fa\u00f8\3\2\2\2\u00fa\u00f9\3\2\2\2"+
		"\u00fb\25\3\2\2\2\u00fc\u00fd\7\5\2\2\u00fd\u00fe\7A\2\2\u00fe\u00ff\5"+
		"\4\3\2\u00ff\u0100\7@\2\2\u0100\27\3\2\2\2\u0101\u0102\7\16\2\2\u0102"+
		"\u0103\7A\2\2\u0103\u0104\5\4\3\2\u0104\u0105\7@\2\2\u0105\31\3\2\2\2"+
		"\u0106\u0107\7\25\2\2\u0107\u0108\5\u008eH\2\u0108\u0109\5\4\3\2\u0109"+
		"\u010a\7\17\2\2\u010a\u0110\3\2\2\2\u010b\u010c\7\25\2\2\u010c\u010d\5"+
		"\u008eH\2\u010d\u010e\7\17\2\2\u010e\u0110\3\2\2\2\u010f\u0106\3\2\2\2"+
		"\u010f\u010b\3\2\2\2\u0110\33\3\2\2\2\u0111\u0112\5 \21\2\u0112\u0113"+
		"\5\4\3\2\u0113\u0114\7\17\2\2\u0114\u0119\3\2\2\2\u0115\u0116\5 \21\2"+
		"\u0116\u0117\7\17\2\2\u0117\u0119\3\2\2\2\u0118\u0111\3\2\2\2\u0118\u0115"+
		"\3\2\2\2\u0119\35\3\2\2\2\u011a\u011b\7`\2\2\u011b\u011c\5\u0086D\2\u011c"+
		"\u011d\5\u00b4[\2\u011d\37\3\2\2\2\u011e\u011f\7\b\2\2\u011f\u0120\5\u008e"+
		"H\2\u0120\u0121\5\36\20\2\u0121\u0128\3\2\2\2\u0122\u0123\7\b\2\2\u0123"+
		"\u0128\5\u008eH\2\u0124\u0125\7\b\2\2\u0125\u0126\7b\2\2\u0126\u0128\5"+
		"\u0098M\2\u0127\u011e\3\2\2\2\u0127\u0122\3\2\2\2\u0127\u0124\3\2\2\2"+
		"\u0128!\3\2\2\2\u0129\u012a\5$\23\2\u012a\u012b\5\4\3\2\u012b\u012c\7"+
		"\17\2\2\u012c\u0131\3\2\2\2\u012d\u012e\5$\23\2\u012e\u012f\7\17\2\2\u012f"+
		"\u0131\3\2\2\2\u0130\u0129\3\2\2\2\u0130\u012d\3\2\2\2\u0131#\3\2\2\2"+
		"\u0132\u0133\7\t\2\2\u0133\u0134\5*\26\2\u0134\u0135\5&\24\2\u0135\u0136"+
		"\5(\25\2\u0136\u0137\5\u00b4[\2\u0137%\3\2\2\2\u0138\u013b\5.\30\2\u0139"+
		"\u013b\5\u00ba^\2\u013a\u0138\3\2\2\2\u013a\u0139\3\2\2\2\u013b\'\3\2"+
		"\2\2\u013c\u013f\58\35\2\u013d\u013f\5\u00ba^\2\u013e\u013c\3\2\2\2\u013e"+
		"\u013d\3\2\2\2\u013f)\3\2\2\2\u0140\u0141\5\u008eH\2\u0141\u0142\5,\27"+
		"\2\u0142\u014d\3\2\2\2\u0143\u014d\5\u0090I\2\u0144\u014d\5x=\2\u0145"+
		"\u0146\5v<\2\u0146\u0147\7?\2\2\u0147\u014d\3\2\2\2\u0148\u014d\5t;\2"+
		"\u0149\u014d\5r:\2\u014a\u014d\5p9\2\u014b\u014d\5n8\2\u014c\u0140\3\2"+
		"\2\2\u014c\u0143\3\2\2\2\u014c\u0144\3\2\2\2\u014c\u0145\3\2\2\2\u014c"+
		"\u0148\3\2\2\2\u014c\u0149\3\2\2\2\u014c\u014a\3\2\2\2\u014c\u014b\3\2"+
		"\2\2\u014d+\3\2\2\2\u014e\u0153\7K\2\2\u014f\u0153\7M\2\2\u0150\u0153"+
		"\7Z\2\2\u0151\u0153\5\u00ba^\2\u0152\u014e\3\2\2\2\u0152\u014f\3\2\2\2"+
		"\u0152\u0150\3\2\2\2\u0152\u0151\3\2\2\2\u0153-\3\2\2\2\u0154\u0155\7"+
		"E\2\2\u0155\u0156\5\60\31\2\u0156\u0157\7D\2\2\u0157\u015c\3\2\2\2\u0158"+
		"\u0159\7E\2\2\u0159\u015c\7D\2\2\u015a\u015c\5\60\31\2\u015b\u0154\3\2"+
		"\2\2\u015b\u0158\3\2\2\2\u015b\u015a\3\2\2\2\u015c/\3\2\2\2\u015d\u015e"+
		"\5\64\33\2\u015e\u015f\5\62\32\2\u015f\61\3\2\2\2\u0160\u0161\7Q\2\2\u0161"+
		"\u0162\5\u00aeX\2\u0162\u0163\5\64\33\2\u0163\u0166\3\2\2\2\u0164\u0166"+
		"\5\u00ba^\2\u0165\u0160\3\2\2\2\u0165\u0164\3\2\2\2\u0166\63\3\2\2\2\u0167"+
		"\u0174\5\u0098M\2\u0168\u0169\7<\2\2\u0169\u0174\5\u0098M\2\u016a\u016b"+
		"\7;\2\2\u016b\u0174\5\u0098M\2\u016c\u016d\79\2\2\u016d\u0174\5\u0098"+
		"M\2\u016e\u0174\5\u008aF\2\u016f\u0170\5\u0098M\2\u0170\u0171\7Z\2\2\u0171"+
		"\u0172\58\35\2\u0172\u0174\3\2\2\2\u0173\u0167\3\2\2\2\u0173\u0168\3\2"+
		"\2\2\u0173\u016a\3\2\2\2\u0173\u016c\3\2\2\2\u0173\u016e\3\2\2\2\u0173"+
		"\u016f\3\2\2\2\u0174\65\3\2\2\2\u0175\u0178\58\35\2\u0176\u0178\5\u008a"+
		"F\2\u0177\u0175\3\2\2\2\u0177\u0176\3\2\2\2\u0178\67\3\2\2\2\u0179\u017a"+
		"\b\35\1\2\u017a\u018a\5D#\2\u017b\u017c\t\2\2\2\u017c\u018a\58\35\27\u017d"+
		"\u017e\7\n\2\2\u017e\u018a\58\35\25\u017f\u0180\5<\37\2\u0180\u0181\5"+
		"8\35\16\u0181\u018a\3\2\2\2\u0182\u0183\5*\26\2\u0183\u0184\5R*\2\u0184"+
		"\u0185\5B\"\2\u0185\u018a\3\2\2\2\u0186\u0187\5\u008eH\2\u0187\u0188\5"+
		"> \2\u0188\u018a\3\2\2\2\u0189\u0179\3\2\2\2\u0189\u017b\3\2\2\2\u0189"+
		"\u017d\3\2\2\2\u0189\u017f\3\2\2\2\u0189\u0182\3\2\2\2\u0189\u0186\3\2"+
		"\2\2\u018a\u01df\3\2\2\2\u018b\u018c\f\32\2\2\u018c\u018d\7Q\2\2\u018d"+
		"\u018e\5\u00aeX\2\u018e\u018f\58\35\33\u018f\u01de\3\2\2\2\u0190\u0191"+
		"\f\31\2\2\u0191\u0192\5l\67\2\u0192\u0193\5\u00aeX\2\u0193\u0194\58\35"+
		"\32\u0194\u01de\3\2\2\2\u0195\u0196\f\23\2\2\u0196\u0197\5\u00b0Y\2\u0197"+
		"\u0198\5\u00b2Z\2\u0198\u0199\7Z\2\2\u0199\u019a\5\u00aeX\2\u019a\u019b"+
		"\58\35\24\u019b\u01de\3\2\2\2\u019c\u019d\f\22\2\2\u019d\u019e\5x=\2\u019e"+
		"\u019f\5\u00aeX\2\u019f\u01a0\58\35\23\u01a0\u01de\3\2\2\2\u01a1\u01a2"+
		"\f\21\2\2\u01a2\u01a3\7^\2\2\u01a3\u01de\58\35\22\u01a4\u01a5\f\20\2\2"+
		"\u01a5\u01a6\7d\2\2\u01a6\u01de\58\35\21\u01a7\u01a8\f\17\2\2\u01a8\u01a9"+
		"\7M\2\2\u01a9\u01aa\7d\2\2\u01aa\u01de\58\35\20\u01ab\u01ac\f\r\2\2\u01ac"+
		"\u01ad\5r:\2\u01ad\u01ae\5\u00aeX\2\u01ae\u01af\58\35\16\u01af\u01de\3"+
		"\2\2\2\u01b0\u01b1\f\f\2\2\u01b1\u01b2\5n8\2\u01b2\u01b3\5\u00aeX\2\u01b3"+
		"\u01b4\58\35\r\u01b4\u01de\3\2\2\2\u01b5\u01b6\f\13\2\2\u01b6\u01b7\5"+
		"p9\2\u01b7\u01b8\5\u00aeX\2\u01b8\u01b9\58\35\f\u01b9\u01de\3\2\2\2\u01ba"+
		"\u01bb\f\n\2\2\u01bb\u01bc\5:\36\2\u01bc\u01bd\5\u00aeX\2\u01bd\u01be"+
		"\58\35\13\u01be\u01de\3\2\2\2\u01bf\u01c0\f\t\2\2\u01c0\u01c1\7K\2\2\u01c1"+
		"\u01c2\58\35\2\u01c2\u01c3\7N\2\2\u01c3\u01c4\58\35\n\u01c4\u01de\3\2"+
		"\2\2\u01c5\u01c6\f\30\2\2\u01c6\u01de\7K\2\2\u01c7\u01c8\f\26\2\2\u01c8"+
		"\u01c9\7C\2\2\u01c9\u01ca\58\35\2\u01ca\u01cb\7B\2\2\u01cb\u01de\3\2\2"+
		"\2\u01cc\u01cd\f\24\2\2\u01cd\u01ce\t\3\2\2\u01ce\u01de\5(\25\2\u01cf"+
		"\u01d0\f\b\2\2\u01d0\u01de\5|?\2\u01d1\u01d2\f\7\2\2\u01d2\u01de\5@!\2"+
		"\u01d3\u01d4\f\6\2\2\u01d4\u01d5\5l\67\2\u01d5\u01d6\7\b\2\2\u01d6\u01de"+
		"\3\2\2\2\u01d7\u01d8\f\4\2\2\u01d8\u01d9\5l\67\2\u01d9\u01da\5*\26\2\u01da"+
		"\u01db\5R*\2\u01db\u01dc\5B\"\2\u01dc\u01de\3\2\2\2\u01dd\u018b\3\2\2"+
		"\2\u01dd\u0190\3\2\2\2\u01dd\u0195\3\2\2\2\u01dd\u019c\3\2\2\2\u01dd\u01a1"+
		"\3\2\2\2\u01dd\u01a4\3\2\2\2\u01dd\u01a7\3\2\2\2\u01dd\u01ab\3\2\2\2\u01dd"+
		"\u01b0\3\2\2\2\u01dd\u01b5\3\2\2\2\u01dd\u01ba\3\2\2\2\u01dd\u01bf\3\2"+
		"\2\2\u01dd\u01c5\3\2\2\2\u01dd\u01c7\3\2\2\2\u01dd\u01cc\3\2\2\2\u01dd"+
		"\u01cf\3\2\2\2\u01dd\u01d1\3\2\2\2\u01dd\u01d3\3\2\2\2\u01dd\u01d7\3\2"+
		"\2\2\u01de\u01e1\3\2\2\2\u01df\u01dd\3\2\2\2\u01df\u01e0\3\2\2\2\u01e0"+
		"9\3\2\2\2\u01e1\u01df\3\2\2\2\u01e2\u01e5\5v<\2\u01e3\u01e5\5t;\2\u01e4"+
		"\u01e2\3\2\2\2\u01e4\u01e3\3\2\2\2\u01e5;\3\2\2\2\u01e6\u01e9\5z>\2\u01e7"+
		"\u01e9\7d\2\2\u01e8\u01e6\3\2\2\2\u01e8\u01e7\3\2\2\2\u01e9=\3\2\2\2\u01ea"+
		"\u01eb\t\4\2\2\u01eb?\3\2\2\2\u01ec\u01ed\7\23\2\2\u01ed\u01ee\5\u00ae"+
		"X\2\u01ee\u01ef\58\35\2\u01ef\u0205\3\2\2\2\u01f0\u01f1\7#\2\2\u01f1\u01f2"+
		"\5\u00aeX\2\u01f2\u01f3\58\35\2\u01f3\u0205\3\2\2\2\u01f4\u01f5\7&\2\2"+
		"\u01f5\u01f6\5\u00aeX\2\u01f6\u01f7\58\35\2\u01f7\u0205\3\2\2\2\u01f8"+
		"\u01f9\7$\2\2\u01f9\u01fa\5\u00aeX\2\u01fa\u01fb\58\35\2\u01fb\u0205\3"+
		"\2\2\2\u01fc\u01fd\7\33\2\2\u01fd\u0205\5\f\7\2\u01fe\u01ff\7\13\2\2\u01ff"+
		"\u0200\5\u0082B\2\u0200\u0201\5\u00b8]\2\u0201\u0202\5\6\4\2\u0202\u0203"+
		"\7\17\2\2\u0203\u0205\3\2\2\2\u0204\u01ec\3\2\2\2\u0204\u01f0\3\2\2\2"+
		"\u0204\u01f4\3\2\2\2\u0204\u01f8\3\2\2\2\u0204\u01fc\3\2\2\2\u0204\u01fe"+
		"\3\2\2\2\u0205A\3\2\2\2\u0206\u0209\5@!\2\u0207\u0209\5\u00ba^\2\u0208"+
		"\u0206\3\2\2\2\u0208\u0207\3\2\2\2\u0209C\3\2\2\2\u020a\u0288\5\u008c"+
		"G\2\u020b\u0288\7*\2\2\u020c\u0288\5\u0088E\2\u020d\u020e\7E\2\2\u020e"+
		"\u020f\58\35\2\u020f\u0210\7D\2\2\u0210\u0288\3\2\2\2\u0211\u0288\5|?"+
		"\2\u0212\u0213\7\6\2\2\u0213\u0288\5(\25\2\u0214\u0215\7\35\2\2\u0215"+
		"\u0288\5(\25\2\u0216\u0217\7\31\2\2\u0217\u0288\58\35\2\u0218\u0219\7"+
		"\33\2\2\u0219\u021a\5X-\2\u021a\u021b\5j\66\2\u021b\u0288\3\2\2\2\u021c"+
		"\u021d\7\'\2\2\u021d\u0288\5(\25\2\u021e\u021f\7\4\2\2\u021f\u0220\5\u00b4"+
		"[\2\u0220\u0221\5\6\4\2\u0221\u0222\7\17\2\2\u0222\u0288\3\2\2\2\u0223"+
		"\u0224\7\23\2\2\u0224\u0225\58\35\2\u0225\u0226\5\u00a6T\2\u0226\u0227"+
		"\5\f\7\2\u0227\u0228\5L\'\2\u0228\u0229\5N(\2\u0229\u022a\7\17\2\2\u022a"+
		"\u0288\3\2\2\2\u022b\u022c\7\23\2\2\u022c\u022d\5\u00aeX\2\u022d\u022e"+
		"\58\35\2\u022e\u022f\5\u00a6T\2\u022f\u0230\5\6\4\2\u0230\u0231\5f\64"+
		"\2\u0231\u0232\7\17\2\2\u0232\u0288\3\2\2\2\u0233\u0234\7\23\2\2\u0234"+
		"\u0235\5\u00aeX\2\u0235\u0236\58\35\2\u0236\u0237\5\u00a6T\2\u0237\u0238"+
		"\5\6\4\2\u0238\u0239\7\17\2\2\u0239\u0288\3\2\2\2\u023a\u023b\7%\2\2\u023b"+
		"\u023c\58\35\2\u023c\u023d\5\u00a6T\2\u023d\u023e\5\b\5\2\u023e\u023f"+
		"\7\17\2\2\u023f\u0288\3\2\2\2\u0240\u0241\7#\2\2\u0241\u0242\5\u00aeX"+
		"\2\u0242\u0243\58\35\2\u0243\u0244\5\u00a6T\2\u0244\u0245\5\6\4\2\u0245"+
		"\u0246\5j\66\2\u0246\u0247\7\17\2\2\u0247\u0288\3\2\2\2\u0248\u0249\7"+
		"\7\2\2\u0249\u024a\5\6\4\2\u024a\u024b\5`\61\2\u024b\u024c\7\17\2\2\u024c"+
		"\u0288\3\2\2\2\u024d\u024e\7\7\2\2\u024e\u024f\5\u00b4[\2\u024f\u0250"+
		"\5`\61\2\u0250\u0251\7\17\2\2\u0251\u0288\3\2\2\2\u0252\u0253\7&\2\2\u0253"+
		"\u0254\5\u00aeX\2\u0254\u0255\58\35\2\u0255\u0256\5\u00a4S\2\u0256\u0257"+
		"\5\6\4\2\u0257\u0258\7\17\2\2\u0258\u0288\3\2\2\2\u0259\u025a\7$\2\2\u025a"+
		"\u025b\5\u00aeX\2\u025b\u025c\58\35\2\u025c\u025d\5\u00a4S\2\u025d\u025e"+
		"\5\6\4\2\u025e\u025f\7\17\2\2\u025f\u0288\3\2\2\2\u0260\u0261\7\22\2\2"+
		"\u0261\u0262\5\u00aeX\2\u0262\u0263\58\35\2\u0263\u0264\7\24\2\2\u0264"+
		"\u0265\5b\62\2\u0265\u0266\5\u00b8]\2\u0266\u0267\5\6\4\2\u0267\u0268"+
		"\7\17\2\2\u0268\u0288\3\2\2\2\u0269\u0288\5\34\17\2\u026a\u0288\5\"\22"+
		"\2\u026b\u0288\5\32\16\2\u026c\u026d\5*\26\2\u026d\u026e\5P)\2\u026e\u0288"+
		"\3\2\2\2\u026f\u0270\7A\2\2\u0270\u0271\5\u00aeX\2\u0271\u0272\5\u008a"+
		"F\2\u0272\u0273\5\u00aeX\2\u0273\u0274\5F$\2\u0274\u0275\5\u00b0Y\2\u0275"+
		"\u0276\5\u00aeX\2\u0276\u0277\7@\2\2\u0277\u0288\3\2\2\2\u0278\u0279\7"+
		"C\2\2\u0279\u027a\5\u00aeX\2\u027a\u027b\58\35\2\u027b\u027c\5\u00aeX"+
		"\2\u027c\u027d\5H%\2\u027d\u027e\5\u00b0Y\2\u027e\u027f\5\u00aeX\2\u027f"+
		"\u0280\7B\2\2\u0280\u0288\3\2\2\2\u0281\u0282\7E\2\2\u0282\u0283\58\35"+
		"\2\u0283\u0284\5J&\2\u0284\u0285\5(\25\2\u0285\u0286\7D\2\2\u0286\u0288"+
		"\3\2\2\2\u0287\u020a\3\2\2\2\u0287\u020b\3\2\2\2\u0287\u020c\3\2\2\2\u0287"+
		"\u020d\3\2\2\2\u0287\u0211\3\2\2\2\u0287\u0212\3\2\2\2\u0287\u0214\3\2"+
		"\2\2\u0287\u0216\3\2\2\2\u0287\u0218\3\2\2\2\u0287\u021c\3\2\2\2\u0287"+
		"\u021e\3\2\2\2\u0287\u0223\3\2\2\2\u0287\u022b\3\2\2\2\u0287\u0233\3\2"+
		"\2\2\u0287\u023a\3\2\2\2\u0287\u0240\3\2\2\2\u0287\u0248\3\2\2\2\u0287"+
		"\u024d\3\2\2\2\u0287\u0252\3\2\2\2\u0287\u0259\3\2\2\2\u0287\u0260\3\2"+
		"\2\2\u0287\u0269\3\2\2\2\u0287\u026a\3\2\2\2\u0287\u026b\3\2\2\2\u0287"+
		"\u026c\3\2\2\2\u0287\u026f\3\2\2\2\u0287\u0278\3\2\2\2\u0287\u0281\3\2"+
		"\2\2\u0288E\3\2\2\2\u0289\u028a\7Q\2\2\u028a\u028b\5\u00aeX\2\u028b\u028c"+
		"\5\u008aF\2\u028c\u0294\3\2\2\2\u028d\u028e\7Q\2\2\u028e\u028f\5\u00ae"+
		"X\2\u028f\u0290\5\u008aF\2\u0290\u0291\5F$\2\u0291\u0294\3\2\2\2\u0292"+
		"\u0294\5\u00ba^\2\u0293\u0289\3\2\2\2\u0293\u028d\3\2\2\2\u0293\u0292"+
		"\3\2\2\2\u0294G\3\2\2\2\u0295\u0296\7Q\2\2\u0296\u0297\5\u00aeX\2\u0297"+
		"\u0298\58\35\2\u0298\u02a0\3\2\2\2\u0299\u029a\7Q\2\2\u029a\u029b\5\u00ae"+
		"X\2\u029b\u029c\58\35\2\u029c\u029d\5H%\2\u029d\u02a0\3\2\2\2\u029e\u02a0"+
		"\5\u00ba^\2\u029f\u0295\3\2\2\2\u029f\u0299\3\2\2\2\u029f\u029e\3\2\2"+
		"\2\u02a0I\3\2\2\2\u02a1\u02a2\t\3\2\2\u02a2K\3\2\2\2\u02a3\u02a4\7\r\2"+
		"\2\u02a4\u02a5\58\35\2\u02a5\u02a6\5\u00a6T\2\u02a6\u02a7\5\f\7\2\u02a7"+
		"\u02aa\3\2\2\2\u02a8\u02aa\5\u00ba^\2\u02a9\u02a3\3\2\2\2\u02a9\u02a8"+
		"\3\2\2\2\u02aaM\3\2\2\2\u02ab\u02ac\7\f\2\2\u02ac\u02af\58\35\2\u02ad"+
		"\u02af\5\u00ba^\2\u02ae\u02ab\3\2\2\2\u02ae\u02ad\3\2\2\2\u02afO\3\2\2"+
		"\2\u02b0\u02b1\7E\2\2\u02b1\u02b2\5\u00aeX\2\u02b2\u02b3\5\66\34\2\u02b3"+
		"\u02b4\5T+\2\u02b4\u02b5\5\u00aeX\2\u02b5\u02b6\7D\2\2\u02b6\u02bc\3\2"+
		"\2\2\u02b7\u02b8\7E\2\2\u02b8\u02b9\5\u00aeX\2\u02b9\u02ba\7D\2\2\u02ba"+
		"\u02bc\3\2\2\2\u02bb\u02b0\3\2\2\2\u02bb\u02b7\3\2\2\2\u02bcQ\3\2\2\2"+
		"\u02bd\u02be\5\66\34\2\u02be\u02bf\5T+\2\u02bfS\3\2\2\2\u02c0\u02c1\7"+
		"Q\2\2\u02c1\u02c2\5\u00aeX\2\u02c2\u02c3\5\66\34\2\u02c3\u02cb\3\2\2\2"+
		"\u02c4\u02c5\7Q\2\2\u02c5\u02c6\5\u00aeX\2\u02c6\u02c7\5\66\34\2\u02c7"+
		"\u02c8\5T+\2\u02c8\u02cb\3\2\2\2\u02c9\u02cb\5\u00ba^\2\u02ca\u02c0\3"+
		"\2\2\2\u02ca\u02c4\3\2\2\2\u02ca\u02c9\3\2\2\2\u02cbU\3\2\2\2\u02cc\u02d1"+
		"\5\u0086D\2\u02cd\u02d1\5\u008aF\2\u02ce\u02cf\7[\2\2\u02cf\u02d1\5\u0098"+
		"M\2\u02d0\u02cc\3\2\2\2\u02d0\u02cd\3\2\2\2\u02d0\u02ce\3\2\2\2\u02d1"+
		"W\3\2\2\2\u02d2\u02d5\5V,\2\u02d3\u02d5\5\u00ba^\2\u02d4\u02d2\3\2\2\2"+
		"\u02d4\u02d3\3\2\2\2\u02d5Y\3\2\2\2\u02d6\u02d7\7%\2\2\u02d7\u02d8\5b"+
		"\62\2\u02d8\u02d9\5\\/\2\u02d9\u02da\5\u00a6T\2\u02da\u02db\5\4\3\2\u02db"+
		"\u02de\3\2\2\2\u02dc\u02de\5h\65\2\u02dd\u02d6\3\2\2\2\u02dd\u02dc\3\2"+
		"\2\2\u02de[\3\2\2\2\u02df\u02e0\7Q\2\2\u02e0\u02e7\5b\62\2\u02e1\u02e2"+
		"\7Q\2\2\u02e2\u02e3\5b\62\2\u02e3\u02e4\5\\/\2\u02e4\u02e7\3\2\2\2\u02e5"+
		"\u02e7\5\u00ba^\2\u02e6\u02df\3\2\2\2\u02e6\u02e1\3\2\2\2\u02e6\u02e5"+
		"\3\2\2\2\u02e7]\3\2\2\2\u02e8\u02ed\5Z.\2\u02e9\u02ea\5Z.\2\u02ea\u02eb"+
		"\5^\60\2\u02eb\u02ed\3\2\2\2\u02ec\u02e8\3\2\2\2\u02ec\u02e9\3\2\2\2\u02ed"+
		"_\3\2\2\2\u02ee\u02f1\5^\60\2\u02ef\u02f1\5\u00ba^\2\u02f0\u02ee\3\2\2"+
		"\2\u02f0\u02ef\3\2\2\2\u02f1a\3\2\2\2\u02f2\u02f3\58\35\2\u02f3c\3\2\2"+
		"\2\u02f4\u02f5\7\r\2\2\u02f5\u02f6\5\u00aeX\2\u02f6\u02f7\58\35\2\u02f7"+
		"\u02f8\5\u00a6T\2\u02f8\u02f9\5\4\3\2\u02f9\u02fc\3\2\2\2\u02fa\u02fc"+
		"\5h\65\2\u02fb\u02f4\3\2\2\2\u02fb\u02fa\3\2\2\2\u02fce\3\2\2\2\u02fd"+
		"\u0302\5d\63\2\u02fe\u02ff\5d\63\2\u02ff\u0300\5f\64\2\u0300\u0302\3\2"+
		"\2\2\u0301\u02fd\3\2\2\2\u0301\u02fe\3\2\2\2\u0302g\3\2\2\2\u0303\u0304"+
		"\7\f\2\2\u0304\u0305\5\u00aeX\2\u0305\u0306\5\4\3\2\u0306i\3\2\2\2\u0307"+
		"\u030a\5h\65\2\u0308\u030a\5\u00ba^\2\u0309\u0307\3\2\2\2\u0309\u0308"+
		"\3\2\2\2\u030ak\3\2\2\2\u030b\u030c\t\5\2\2\u030cm\3\2\2\2\u030d\u030e"+
		"\t\6\2\2\u030eo\3\2\2\2\u030f\u0310\t\7\2\2\u0310q\3\2\2\2\u0311\u0312"+
		"\t\b\2\2\u0312s\3\2\2\2\u0313\u0314\t\t\2\2\u0314u\3\2\2\2\u0315\u0316"+
		"\t\n\2\2\u0316w\3\2\2\2\u0317\u0318\t\13\2\2\u0318y\3\2\2\2\u0319\u031a"+
		"\t\f\2\2\u031a{\3\2\2\2\u031b\u031c\7A\2\2\u031c\u031d\5\u00aeX\2\u031d"+
		"\u031e\5\u0082B\2\u031e\u031f\5\b\5\2\u031f\u0320\5\u00aeX\2\u0320\u0321"+
		"\5~@\2\u0321\u0322\7@\2\2\u0322}\3\2\2\2\u0323\u0324\7Q\2\2\u0324\u0325"+
		"\5\b\5\2\u0325\u0326\5\u00aeX\2\u0326\u032e\3\2\2\2\u0327\u0328\7Q\2\2"+
		"\u0328\u0329\5\b\5\2\u0329\u032a\5\u00aeX\2\u032a\u032b\5~@\2\u032b\u032e"+
		"\3\2\2\2\u032c\u032e\5\u00ba^\2\u032d\u0323\3\2\2\2\u032d\u0327\3\2\2"+
		"\2\u032d\u032c\3\2\2\2\u032e\177\3\2\2\2\u032f\u0330\7U\2\2\u0330\u0331"+
		"\58\35\2\u0331\u0332\5\u0084C\2\u0332\u0333\7U\2\2\u0333\u0081\3\2\2\2"+
		"\u0334\u0337\5\u0080A\2\u0335\u0337\5\u00ba^\2\u0336\u0334\3\2\2\2\u0336"+
		"\u0335\3\2\2\2\u0337\u0083\3\2\2\2\u0338\u0339\7Q\2\2\u0339\u0340\58\35"+
		"\2\u033a\u033b\7Q\2\2\u033b\u033c\58\35\2\u033c\u033d\5\u0084C\2\u033d"+
		"\u0340\3\2\2\2\u033e\u0340\5\u00ba^\2\u033f\u0338\3\2\2\2\u033f\u033a"+
		"\3\2\2\2\u033f\u033e\3\2\2\2\u0340\u0085\3\2\2\2\u0341\u0345\5\u008eH"+
		"\2\u0342\u0343\7N\2\2\u0343\u0345\5\u008eH\2\u0344\u0341\3\2\2\2\u0344"+
		"\u0342\3\2\2\2\u0345\u0087\3\2\2\2\u0346\u0347\7N\2\2\u0347\u034d\5\u0098"+
		"M\2\u0348\u0349\7N\2\2\u0349\u034d\5\u00a8U\2\u034a\u034b\7N\2\2\u034b"+
		"\u034d\5*\26\2\u034c\u0346\3\2\2\2\u034c\u0348\3\2\2\2\u034c\u034a\3\2"+
		"\2\2\u034d\u0089\3\2\2\2\u034e\u034f\58\35\2\u034f\u0350\7[\2\2\u0350"+
		"\u0351\58\35\2\u0351\u0357\3\2\2\2\u0352\u0353\58\35\2\u0353\u0354\7N"+
		"\2\2\u0354\u0355\58\35\2\u0355\u0357\3\2\2\2\u0356\u034e\3\2\2\2\u0356"+
		"\u0352\3\2\2\2\u0357\u008b\3\2\2\2\u0358\u0359\bG\1\2\u0359\u035e\5\u0098"+
		"M\2\u035a\u035e\5\u0090I\2\u035b\u035c\7O\2\2\u035c\u035e\5\u008cG\3\u035d"+
		"\u0358\3\2\2\2\u035d\u035a\3\2\2\2\u035d\u035b\3\2\2\2\u035e\u0364\3\2"+
		"\2\2\u035f\u0360\f\4\2\2\u0360\u0361\7O\2\2\u0361\u0363\5\u0092J\2\u0362"+
		"\u035f\3\2\2\2\u0363\u0366\3\2\2\2\u0364\u0362\3\2\2\2\u0364\u0365\3\2"+
		"\2\2\u0365\u008d\3\2\2\2\u0366\u0364\3\2\2\2\u0367\u0368\5\u0098M\2\u0368"+
		"\u0369\5\u0096L\2\u0369\u008f\3\2\2\2\u036a\u0374\7)\2\2\u036b\u0374\5"+
		"\u00a8U\2\u036c\u0374\7(\2\2\u036d\u0374\7!\2\2\u036e\u0374\7\21\2\2\u036f"+
		"\u0374\7\27\2\2\u0370\u0371\7)\2\2\u0371\u0374\7H\2\2\u0372\u0374\7k\2"+
		"\2\u0373\u036a\3\2\2\2\u0373\u036b\3\2\2\2\u0373\u036c\3\2\2\2\u0373\u036d"+
		"\3\2\2\2\u0373\u036e\3\2\2\2\u0373\u036f\3\2\2\2\u0373\u0370\3\2\2\2\u0373"+
		"\u0372\3\2\2\2\u0374\u0091\3\2\2\2\u0375\u0378\5\u0098M\2\u0376\u0378"+
		"\5\u0090I\2\u0377\u0375\3\2\2\2\u0377\u0376\3\2\2\2\u0378\u0093\3\2\2"+
		"\2\u0379\u037a\7O\2\2\u037a\u037e\5\u0098M\2\u037b\u037c\7H\2\2\u037c"+
		"\u037e\5\u0098M\2\u037d\u0379\3\2\2\2\u037d\u037b\3\2\2\2\u037e\u0095"+
		"\3\2\2\2\u037f\u0385\5\u0094K\2\u0380\u0381\5\u0094K\2\u0381\u0382\5\u0096"+
		"L\2\u0382\u0385\3\2\2\2\u0383\u0385\5\u00ba^\2\u0384\u037f\3\2\2\2\u0384"+
		"\u0380\3\2\2\2\u0384\u0383\3\2\2\2\u0385\u0097\3\2\2\2\u0386\u0394\7j"+
		"\2\2\u0387\u0394\5\u009cO\2\u0388\u0394\5\u009eP\2\u0389\u0394\5\u00a0"+
		"Q\2\u038a\u0394\5\u00a2R\2\u038b\u0394\7\26\2\2\u038c\u0394\7\32\2\2\u038d"+
		"\u0394\7\34\2\2\u038e\u0394\7\6\2\2\u038f\u0394\7\36\2\2\u0390\u0394\7"+
		"\37\2\2\u0391\u0394\7\27\2\2\u0392\u0394\5\u009aN\2\u0393\u0386\3\2\2"+
		"\2\u0393\u0387\3\2\2\2\u0393\u0388\3\2\2\2\u0393\u0389\3\2\2\2\u0393\u038a"+
		"\3\2\2\2\u0393\u038b\3\2\2\2\u0393\u038c\3\2\2\2\u0393\u038d\3\2\2\2\u0393"+
		"\u038e\3\2\2\2\u0393\u038f\3\2\2\2\u0393\u0390\3\2\2\2\u0393\u0391\3\2"+
		"\2\2\u0393\u0392\3\2\2\2\u0394\u0099\3\2\2\2\u0395\u0396\7A\2\2\u0396"+
		"\u039a\7@\2\2\u0397\u0398\7C\2\2\u0398\u039a\7B\2\2\u0399\u0395\3\2\2"+
		"\2\u0399\u0397\3\2\2\2\u039a\u009b\3\2\2\2\u039b\u039c\7>\2\2\u039c\u039d"+
		"\7j\2\2\u039d\u009d\3\2\2\2\u039e\u039f\7?\2\2\u039f\u03a0\7?\2\2\u03a0"+
		"\u03a1\7j\2\2\u03a1\u009f\3\2\2\2\u03a2\u03a3\7?\2\2\u03a3\u03a4\7j\2"+
		"\2\u03a4\u00a1\3\2\2\2\u03a5\u03a6\7>\2\2\u03a6\u03a9\7(\2\2\u03a7\u03a9"+
		"\7,\2\2\u03a8\u03a5\3\2\2\2\u03a8\u03a7\3\2\2\2\u03a9\u00a3\3\2\2\2\u03aa"+
		"\u03ab\7\13\2\2\u03ab\u03b0\5\u00b4[\2\u03ac\u03ad\7N\2\2\u03ad\u03b0"+
		"\5\u00b4[\2\u03ae\u03b0\5\u00b4[\2\u03af\u03aa\3\2\2\2\u03af\u03ac\3\2"+
		"\2\2\u03af\u03ae\3\2\2\2\u03b0\u00a5\3\2\2\2\u03b1\u03b2\7 \2\2\u03b2"+
		"\u03b7\5\u00b8]\2\u03b3\u03b4\7N\2\2\u03b4\u03b7\5\u00b8]\2\u03b5\u03b7"+
		"\5\u00b4[\2\u03b6\u03b1\3\2\2\2\u03b6\u03b3\3\2\2\2\u03b6\u03b5\3\2\2"+
		"\2\u03b7\u00a7\3\2\2\2\u03b8\u03bc\7+\2\2\u03b9\u03ba\7+\2\2\u03ba\u03bc"+
		"\5\u00a8U\2\u03bb\u03b8\3\2\2\2\u03bb\u03b9\3\2\2\2\u03bc\u00a9\3\2\2"+
		"\2\u03bd\u03be\t\r\2\2\u03be\u00ab\3\2\2\2\u03bf\u03c4\5\u00aaV\2\u03c0"+
		"\u03c1\5\u00aaV\2\u03c1\u03c2\5\u00acW\2\u03c2\u03c4\3\2\2\2\u03c3\u03bf"+
		"\3\2\2\2\u03c3\u03c0\3\2\2\2\u03c4\u00ad\3\2\2\2\u03c5\u03c8\5\u00acW"+
		"\2\u03c6\u03c8\5\u00ba^\2\u03c7\u03c5\3\2\2\2\u03c7\u03c6\3\2\2\2\u03c8"+
		"\u00af\3\2\2\2\u03c9\u03cc\7Q\2\2\u03ca\u03cc\5\u00ba^\2\u03cb\u03c9\3"+
		"\2\2\2\u03cb\u03ca\3\2\2\2\u03cc\u00b1\3\2\2\2\u03cd\u03d0\7<\2\2\u03ce"+
		"\u03d0\5\u00ba^\2\u03cf\u03cd\3\2\2\2\u03cf\u03ce\3\2\2\2\u03d0\u00b3"+
		"\3\2\2\2\u03d1\u03d6\5\u00b6\\\2\u03d2\u03d3\5\u00b6\\\2\u03d3\u03d4\5"+
		"\u00b4[\2\u03d4\u03d6\3\2\2\2\u03d5\u03d1\3\2\2\2\u03d5\u03d2\3\2\2\2"+
		"\u03d6\u00b5\3\2\2\2\u03d7\u03d8\t\16\2\2\u03d8\u00b7\3\2\2\2\u03d9\u03dc"+
		"\5\u00b4[\2\u03da\u03dc\5\u00ba^\2\u03db\u03d9\3\2\2\2\u03db\u03da\3\2"+
		"\2\2\u03dc\u00b9\3\2\2\2\u03dd\u03de\3\2\2\2\u03de\u00bb\3\2\2\2D\u00c3"+
		"\u00cb\u00d0\u00dd\u00e5\u00f2\u00fa\u010f\u0118\u0127\u0130\u013a\u013e"+
		"\u014c\u0152\u015b\u0165\u0173\u0177\u0189\u01dd\u01df\u01e4\u01e8\u0204"+
		"\u0208\u0287\u0293\u029f\u02a9\u02ae\u02bb\u02ca\u02d0\u02d4\u02dd\u02e6"+
		"\u02ec\u02f0\u02fb\u0301\u0309\u032d\u0336\u033f\u0344\u034c\u0356\u035d"+
		"\u0364\u0373\u0377\u037d\u0384\u0393\u0399\u03a8\u03af\u03b6\u03bb\u03c3"+
		"\u03c7\u03cb\u03cf\u03d5\u03db";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}