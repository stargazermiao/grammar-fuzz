/*
MIT License
Copyright (c) 2019 Gang ZHANG (gangz@emergentdesign.cn)
(Contributors are welcome!)
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
History: 2019/01/06 Initial version
*/

parser grammar RubyParser;

options { tokenVocab= RubyLexer; }

compilation_unit
:
	statement_list_terms
;

statement_list_terms
:
	statement terms
	| statement_list_terms statement terms
	| terms
;

opt_statement_list_terms: statement_list_terms | none;

statement_list_noterms
:
	opt_statement_terms statement
;

opt_statement_terms
:
   statement terms
   | statement terms opt_statement_terms
   | none
;

statement
:
	 begin_block
	| end_block
	| alias_statement
	| undef_statement
	| ENSURE
	| expr
;

undef_statement: UNDEF function_name_or_symbol opt_function_name_or_symbol;

opt_function_name_or_symbol
:
    ',' function_name_or_symbol
    | ',' function_name_or_symbol opt_function_name_or_symbol
    | none
;

alias_statement
:
	ALIAS function_name_or_symbol function_name_or_symbol
;

function_name_or_symbol:
	function_name
	|symbol
;

begin_block
:
	BEGIN_BLOCK LEFT_PAREN statement_list_terms RIGHT_PAREN
;

end_block
:
	END_BLOCK LEFT_PAREN statement_list_terms RIGHT_PAREN
;

module_definition
:
	MODULE cpath statement_list_terms END
	| MODULE cpath END
;

class_definition:
   	class_header statement_list_terms END
   	| class_header END
;

superclass: '<' id_symbol terms;

class_header:
	CLASS cpath superclass
	| CLASS cpath
	| CLASS BIT_SHL identifier
;

function_definition
:
	function_definition_header statement_list_terms END
	| function_definition_header END
;

function_definition_header
:
	DEF function_name opt_function_definition_params opt_expr terms
;

opt_function_definition_params
:
    function_definition_params
    | none
;

opt_expr : expr | none;

function_name
:   cpath opt_cpath_func_suffix
    | literal
	| assignOperator
	| mathOperator AT
	| bitOperator
	| compareOperator
	| equalsOperator
	| logicalOperator
;

opt_cpath_func_suffix : QUESTION | SIGH | ASSIGN | none;

function_definition_params
:
	LEFT_RBRACKET func_def_body RIGHT_RBRACKET
	| LEFT_RBRACKET RIGHT_RBRACKET
	| func_def_body
;

func_def_body:
    function_definition_param opt_function_definition_param_list
;

opt_function_definition_param_list:
    ',' opt_crlfs function_definition_param
    | none
;

function_definition_param:
	identifier
	| MUL identifier
	| EXP identifier
	| BIT_AND identifier
	| hash_asso
	| identifier ASSIGN expr
;

function_call_param:
	expr
	| hash_asso
;


expr
:
	primary                                              #ExprPrimary
	| expr ',' opt_crlfs expr                               #ExprList
	| expr dot_ref opt_crlfs expr                           #ExprDotRef
	| expr postfix=QUESTION                              #ExprQuestion
	| prefix=(PLUS| MINUS|MUL|MOD|BIT_AND) expr          #ExprPrefixCalc
	| expr LEFT_SBRACKET expr RIGHT_SBRACKET 			 #ExprArrayAccess					/* array access */
	| prefix=DEFINED expr                                #ExprDefineTest        	        /* identifier definition test */
	| expr bop=(DOT2|DOT3) opt_expr 						 #ExprRange		                  	/* range */
	| expr opt_comma opt_mul ASSIGN opt_crlfs expr                  #ExprBatchAssign          			/* batch assign */
	| expr assignOperator opt_crlfs expr					 #ExprAssign						/* assign */
	| expr bop=PATTERN_MATCH expr                        #ExprPatternMatch1                 /* pattern match */
	| expr bop=BIT_NOT expr                              #ExprPatternMatch2                 /* pattern match */
	| expr SIGH BIT_NOT expr                             #ExprPatternMatch3                 /* pattern match */
	| not_or_bitnot expr                                 #ExprLogicalNot	             	/* logical not */
	| expr (compareOperator) opt_crlfs expr                 #ExprCompareLogical               	/* compare logical  */
	| expr (logicalOperator) opt_crlfs expr                 #ExprLogicalJoin	             	/* logical join */
	| expr (equalsOperator) opt_crlfs expr                  #ExprEqualTest		              	/* equal test */
	| expr (math_or_bit_op) opt_crlfs expr        #ExprCalcuation                  	/* calcuation */
	| expr QUESTION expr COLON expr                      #ExprConditionStatement   			/* cond?true_part:false_part */
	| expr block                                         #ExprBlock
	| expr expr_statement_suffix                         #ExprWitStatementSuffix
	| expr dot_ref CLASS                                 #ExprDotClass
	| function_name func_call_parameters_no_bracket opt_expr_statement_suffix                      #ExprFunctionCall1
	| expr dot_ref function_name func_call_parameters_no_bracket opt_expr_statement_suffix         #ExprFunctionCall2
	| cpath cpath_expr_suffix                                                                      #ExprFunctionCall3
	;

math_or_bit_op: mathOperator | bitOperator;

not_or_bitnot: notSym | BIT_NOT;

cpath_expr_suffix: QUESTION | SIGH;

expr_statement_suffix:
	IF opt_crlfs expr
	| UNLESS opt_crlfs expr
	| WHILE opt_crlfs expr
	| UNTIL opt_crlfs expr
	| RESCUE statement
    | DO opt_block_params opt_terms opt_statement_list_terms END
;

opt_expr_statement_suffix : expr_statement_suffix | none;

primary:
	variable_path                                                              #PrimaryVarPath
	| Regex														               #PrimaryRegex
	| symbol                                                                   #PrimarySymbol
	| LEFT_RBRACKET expr RIGHT_RBRACKET                                        #PrimaryBracket
	| block                                                                    #PrimaryBlock
	| BREAK opt_expr     										               #PrimaryStatementBreak
	| RETURN opt_expr                                                          #PrimaryStatementReturn
	| RAISE expr                                                               #PrimaryStatementRaise
	| RESCUE opt_rescure_param  opt_else_tail                                  #PrimaryStatementRescue
	| YIELD opt_expr												           #PrimaryStatementYield
	| BEGIN terms opt_statement_list_terms END						           #PrimaryBeginBlock
    | IF expr then_keyword statement opt_else_if opt_else END                  #PrimaryBlockIf3
	| IF opt_crlfs expr then_keyword opt_statement_list_terms if_tails END     #PrimaryBlockIf2
	| IF opt_crlfs expr then_keyword opt_statement_list_terms END              #PrimaryBlockIf
	| WHEN expr then_keyword statement_list_noterms END                        #PrimaryBlockWhen
	| UNLESS opt_crlfs expr then_keyword  opt_statement_list_terms opt_else_tail END  #PrimaryBlockUnless
	| CASE opt_statement_list_terms opt_case_bodys  END                               #PrimaryBlockCase1
	| CASE terms opt_case_bodys  END                                               #PrimaryBlockCase2
	| WHILE opt_crlfs expr do_keyword  opt_statement_list_terms END                  #PrimaryBlockWhile
	| UNTIL opt_crlfs expr do_keyword  opt_statement_list_terms  END                 #PrimaryBlockUntil
	| FOR opt_crlfs expr IN when_cond opt_terms  opt_statement_list_terms END           #PrimaryBlockFor
	| class_definition                                                         #PrimaryBlockClassDef
	| function_definition                                                      #PrimaryBlockFunctionDef
	| module_definition                                                        #PrimaryBlockModelDef
	| function_name func_call_parameters                                       #PrimaryFunctionCall0
	| LEFT_PAREN opt_crlfs hash_asso opt_crlfs comma_hash_asssos opt_comma opt_crlfs RIGHT_PAREN   	#PrimaryListHash
	| LEFT_SBRACKET  opt_crlfs expr opt_crlfs comma_exprs opt_comma opt_crlfs  RIGHT_SBRACKET   	#PrimaryListExpr
	| LEFT_RBRACKET expr dot2_or_dot3 opt_expr RIGHT_RBRACKET                               	#PrimaryRange
	;

comma_hash_asssos:
    ',' opt_crlfs hash_asso
    | ',' opt_crlfs hash_asso comma_hash_asssos
    | none
;

comma_exprs:
    ',' opt_crlfs expr
    | ',' opt_crlfs expr comma_exprs
    | none
;

dot2_or_dot3 : DOT2 | DOT3;

opt_else_if: ELSIF expr then_keyword statement | none;

opt_else: ELSE expr | none;

func_call_parameters
:
	LEFT_RBRACKET opt_crlfs function_call_param  function_call_param_rest opt_crlfs RIGHT_RBRACKET
	| LEFT_RBRACKET opt_crlfs RIGHT_RBRACKET
;
func_call_parameters_no_bracket:
	function_call_param function_call_param_rest
;

function_call_param_rest:
    ',' opt_crlfs function_call_param
    | ',' opt_crlfs function_call_param function_call_param_rest
    | none
;

rescure_param: id_symbol | hash_asso |ASSOC identifier;

opt_rescure_param: rescure_param | none;

case_body:
	WHEN when_cond when_cond_rest then_keyword statement_list_terms
	|else_tail;

when_cond_rest:
    ',' when_cond
    | ',' when_cond when_cond_rest
    | none
;

case_bodys: case_body | case_body case_bodys;

opt_case_bodys: case_bodys | none;

when_cond:
	expr
	;

if_tail:
	ELSIF opt_crlfs expr then_keyword statement_list_terms
	| else_tail
;

if_tails : if_tail | if_tail if_tails;

else_tail:
	ELSE opt_crlfs statement_list_terms;

opt_else_tail: else_tail | none;

dot_ref: DOT | ANDDOT ;


logicalOperator: OR| AND;

equalsOperator: EQUAL| NOT_EQUAL | EQUAL3;

compareOperator: LESS|GREATER|LESS_EQUAL| GREATER_EQUAL;

bitOperator: BIT_SHL|BIT_SHR|BIT_AND|BIT_OR|BIT_XOR;

mathOperator: MUL|DIV|MOD|PLUS|MINUS|EXP;

assignOperator:
	PLUS_ASSIGN | MINUS_ASSIGN |MUL_ASSIGN|DIV_ASSIGN|MOD_ASSIGN
	| EXP_ASSIGN  |BIT_OR_ASSIGN|BIT_AND_ASSIGN|OR_ASSIGN|AND_ASSIGN
	| BIT_XOR_ASSIGN | BIT_NOT_ASSIGN |BIT_SHL_ASSIGN | BIT_SHR_ASSIGN
	| ASSIGN;

notSym: NOT | SIGH;

block: LEFT_PAREN opt_crlfs opt_block_params statement_list_noterms opt_crlfs block_stmt_list RIGHT_PAREN;

block_stmt_list :
    ',' statement_list_noterms opt_crlfs
    | ',' statement_list_noterms opt_crlfs block_stmt_list
    | none
;

block_params: BIT_OR expr expr_block_params_list  BIT_OR;

opt_block_params: block_params | none;

expr_block_params_list : ',' expr | ',' expr expr_block_params_list | none;

id_symbol:
	cpath
	| 	COLON cpath
	;

symbol:
	COLON identifier
	|  COLON string
	|  COLON function_name
;

hash_asso:
	expr ASSOC expr
	| expr COLON expr
;

variable_path:
	identifier                                 			#VarPathId
    |literal                                    		#VarPathLiteral
	| variable_path COLON2 id_or_literal             	#VarPathJoin
	| COLON2 variable_path                      		#VarPathGlobalPrefix
	;
cpath:
    identifier cpath_suffixs
    ;
literal:
	Float
	| string
	| Integer
	| TRUE
	| FALSE
	| NIL
	| Float DOT
	| ShellCommand
;

id_or_literal: identifier|literal;

cpath_suffix:
    COLON2 identifier | DOT identifier
;

cpath_suffixs: cpath_suffix | cpath_suffix cpath_suffixs | none;

identifier: Identifier | globalVar | classVar |instanceVar | idArg | NEXT  |REDO |RETRY | BREAK |SELF | SUPER  |NIL | empty;

empty:
	LEFT_PAREN RIGHT_PAREN																/* empty block  */
	| LEFT_SBRACKET  RIGHT_SBRACKET                                                    	/* empty array */
	;

globalVar:	DOLLAR Identifier;
classVar: AT AT Identifier;
instanceVar: AT Identifier;
idArg: DOLLAR Integer | DollarSpecial;

do_keyword: DO terms | COLON terms | terms ;
then_keyword: THEN opt_terms | COLON opt_terms | terms;
string: String | String string;  /* many string can be concatenated  */

crlf: SL_COMMENT|CRLF;

crlfs : crlf | crlf crlfs;

opt_crlfs: crlfs | none;

opt_comma : ',' | none;

opt_mul : MUL | none;

terms:
	term | term terms
;

term: SEMICOLON | CRLF | SL_COMMENT;

opt_terms: terms | none;

none: ;