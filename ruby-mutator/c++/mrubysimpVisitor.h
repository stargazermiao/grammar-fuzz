
// Generated from mrubysimp.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "mrubysimpParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by mrubysimpParser.
 */
class  mrubysimpVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by mrubysimpParser.
   */
    virtual antlrcpp::Any visitStart(mrubysimpParser::StartContext *context) = 0;

    virtual antlrcpp::Any visitProgram(mrubysimpParser::ProgramContext *context) = 0;

    virtual antlrcpp::Any visitStatement(mrubysimpParser::StatementContext *context) = 0;

    virtual antlrcpp::Any visitVar(mrubysimpParser::VarContext *context) = 0;

    virtual antlrcpp::Any visitRef(mrubysimpParser::RefContext *context) = 0;

    virtual antlrcpp::Any visitArgs(mrubysimpParser::ArgsContext *context) = 0;

    virtual antlrcpp::Any visitVal(mrubysimpParser::ValContext *context) = 0;

    virtual antlrcpp::Any visitIdentifier(mrubysimpParser::IdentifierContext *context) = 0;


};

