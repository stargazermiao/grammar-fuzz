
// Generated from mrubysimp.g4 by ANTLR 4.8


#include "mrubysimpVisitor.h"

#include "mrubysimpParser.h"


using namespace antlrcpp;
using namespace antlr4;

mrubysimpParser::mrubysimpParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

mrubysimpParser::~mrubysimpParser() {
  delete _interpreter;
}

std::string mrubysimpParser::getGrammarFileName() const {
  return "mrubysimp.g4";
}

const std::vector<std::string>& mrubysimpParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& mrubysimpParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- StartContext ------------------------------------------------------------------

mrubysimpParser::StartContext::StartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* mrubysimpParser::StartContext::START_HEAD() {
  return getToken(mrubysimpParser::START_HEAD, 0);
}

mrubysimpParser::ProgramContext* mrubysimpParser::StartContext::program() {
  return getRuleContext<mrubysimpParser::ProgramContext>(0);
}

tree::TerminalNode* mrubysimpParser::StartContext::EOF() {
  return getToken(mrubysimpParser::EOF, 0);
}


size_t mrubysimpParser::StartContext::getRuleIndex() const {
  return mrubysimpParser::RuleStart;
}


antlrcpp::Any mrubysimpParser::StartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitStart(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::StartContext* mrubysimpParser::start() {
  StartContext *_localctx = _tracker.createInstance<StartContext>(_ctx, getState());
  enterRule(_localctx, 0, mrubysimpParser::RuleStart);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(16);
    match(mrubysimpParser::START_HEAD);
    setState(17);
    program();
    setState(18);
    match(mrubysimpParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgramContext ------------------------------------------------------------------

mrubysimpParser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<mrubysimpParser::StatementContext *> mrubysimpParser::ProgramContext::statement() {
  return getRuleContexts<mrubysimpParser::StatementContext>();
}

mrubysimpParser::StatementContext* mrubysimpParser::ProgramContext::statement(size_t i) {
  return getRuleContext<mrubysimpParser::StatementContext>(i);
}


size_t mrubysimpParser::ProgramContext::getRuleIndex() const {
  return mrubysimpParser::RuleProgram;
}


antlrcpp::Any mrubysimpParser::ProgramContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitProgram(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::ProgramContext* mrubysimpParser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 2, mrubysimpParser::RuleProgram);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(21); 
    _errHandler->sync(this);
    alt = 1;
    do {
      switch (alt) {
        case 1: {
              setState(20);
              statement();
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(23); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 0, _ctx);
    } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

mrubysimpParser::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* mrubysimpParser::StatementContext::DEF() {
  return getToken(mrubysimpParser::DEF, 0);
}

std::vector<mrubysimpParser::VarContext *> mrubysimpParser::StatementContext::var() {
  return getRuleContexts<mrubysimpParser::VarContext>();
}

mrubysimpParser::VarContext* mrubysimpParser::StatementContext::var(size_t i) {
  return getRuleContext<mrubysimpParser::VarContext>(i);
}

tree::TerminalNode* mrubysimpParser::StatementContext::DOT() {
  return getToken(mrubysimpParser::DOT, 0);
}

std::vector<mrubysimpParser::IdentifierContext *> mrubysimpParser::StatementContext::identifier() {
  return getRuleContexts<mrubysimpParser::IdentifierContext>();
}

mrubysimpParser::IdentifierContext* mrubysimpParser::StatementContext::identifier(size_t i) {
  return getRuleContext<mrubysimpParser::IdentifierContext>(i);
}

tree::TerminalNode* mrubysimpParser::StatementContext::LP() {
  return getToken(mrubysimpParser::LP, 0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::RP() {
  return getToken(mrubysimpParser::RP, 0);
}

std::vector<mrubysimpParser::ArgsContext *> mrubysimpParser::StatementContext::args() {
  return getRuleContexts<mrubysimpParser::ArgsContext>();
}

mrubysimpParser::ArgsContext* mrubysimpParser::StatementContext::args(size_t i) {
  return getRuleContext<mrubysimpParser::ArgsContext>(i);
}

mrubysimpParser::ProgramContext* mrubysimpParser::StatementContext::program() {
  return getRuleContext<mrubysimpParser::ProgramContext>(0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::EQ() {
  return getToken(mrubysimpParser::EQ, 0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::LCB() {
  return getToken(mrubysimpParser::LCB, 0);
}

std::vector<tree::TerminalNode *> mrubysimpParser::StatementContext::MID() {
  return getTokens(mrubysimpParser::MID);
}

tree::TerminalNode* mrubysimpParser::StatementContext::MID(size_t i) {
  return getToken(mrubysimpParser::MID, i);
}

tree::TerminalNode* mrubysimpParser::StatementContext::RCB() {
  return getToken(mrubysimpParser::RCB, 0);
}

mrubysimpParser::ValContext* mrubysimpParser::StatementContext::val() {
  return getRuleContext<mrubysimpParser::ValContext>(0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::RETURN() {
  return getToken(mrubysimpParser::RETURN, 0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::BREAK() {
  return getToken(mrubysimpParser::BREAK, 0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::RAISE() {
  return getToken(mrubysimpParser::RAISE, 0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::YIELD() {
  return getToken(mrubysimpParser::YIELD, 0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::CONTINUE() {
  return getToken(mrubysimpParser::CONTINUE, 0);
}

tree::TerminalNode* mrubysimpParser::StatementContext::NEXT() {
  return getToken(mrubysimpParser::NEXT, 0);
}


size_t mrubysimpParser::StatementContext::getRuleIndex() const {
  return mrubysimpParser::RuleStatement;
}


antlrcpp::Any mrubysimpParser::StatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitStatement(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::StatementContext* mrubysimpParser::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 4, mrubysimpParser::RuleStatement);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(99);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(25);
      match(mrubysimpParser::DEF);
      setState(26);
      var();
      setState(27);
      match(mrubysimpParser::DOT);
      setState(28);
      identifier();
      setState(29);
      match(mrubysimpParser::LP);
      setState(31);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == mrubysimpParser::LP

      || _la == mrubysimpParser::NAME) {
        setState(30);
        args();
      }
      setState(33);
      match(mrubysimpParser::RP);
      setState(35);
      _errHandler->sync(this);

      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx)) {
      case 1: {
        setState(34);
        program();
        break;
      }

      }
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(37);
      var();
      setState(38);
      match(mrubysimpParser::EQ);
      setState(39);
      var();
      setState(40);
      match(mrubysimpParser::DOT);
      setState(41);
      identifier();
      setState(42);
      match(mrubysimpParser::LP);
      setState(44);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == mrubysimpParser::LP

      || _la == mrubysimpParser::NAME) {
        setState(43);
        args();
      }
      setState(46);
      match(mrubysimpParser::RP);
      setState(47);
      match(mrubysimpParser::LCB);
      setState(48);
      match(mrubysimpParser::MID);
      setState(50);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == mrubysimpParser::LP

      || _la == mrubysimpParser::NAME) {
        setState(49);
        args();
      }
      setState(52);
      match(mrubysimpParser::MID);
      setState(54);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << mrubysimpParser::DEF)
        | (1ULL << mrubysimpParser::LP)
        | (1ULL << mrubysimpParser::RETURN)
        | (1ULL << mrubysimpParser::RAISE)
        | (1ULL << mrubysimpParser::YIELD)
        | (1ULL << mrubysimpParser::CONTINUE)
        | (1ULL << mrubysimpParser::BREAK)
        | (1ULL << mrubysimpParser::NEXT)
        | (1ULL << mrubysimpParser::NAME))) != 0)) {
        setState(53);
        program();
      }
      setState(56);
      match(mrubysimpParser::RCB);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(58);
      var();
      setState(59);
      match(mrubysimpParser::EQ);
      setState(60);
      identifier();
      setState(61);
      match(mrubysimpParser::DOT);
      setState(62);
      identifier();
      setState(63);
      match(mrubysimpParser::LP);
      setState(65);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == mrubysimpParser::LP

      || _la == mrubysimpParser::NAME) {
        setState(64);
        args();
      }
      setState(67);
      match(mrubysimpParser::RP);
      setState(68);
      match(mrubysimpParser::LCB);
      setState(69);
      match(mrubysimpParser::MID);
      setState(71);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == mrubysimpParser::LP

      || _la == mrubysimpParser::NAME) {
        setState(70);
        args();
      }
      setState(73);
      match(mrubysimpParser::MID);
      setState(75);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << mrubysimpParser::DEF)
        | (1ULL << mrubysimpParser::LP)
        | (1ULL << mrubysimpParser::RETURN)
        | (1ULL << mrubysimpParser::RAISE)
        | (1ULL << mrubysimpParser::YIELD)
        | (1ULL << mrubysimpParser::CONTINUE)
        | (1ULL << mrubysimpParser::BREAK)
        | (1ULL << mrubysimpParser::NEXT)
        | (1ULL << mrubysimpParser::NAME))) != 0)) {
        setState(74);
        program();
      }
      setState(77);
      match(mrubysimpParser::RCB);
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(79);
      var();
      setState(80);
      match(mrubysimpParser::EQ);
      setState(81);
      var();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(83);
      var();
      setState(84);
      match(mrubysimpParser::EQ);
      setState(85);
      val();
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(87);
      match(mrubysimpParser::RETURN);
      setState(88);
      var();
      break;
    }

    case 7: {
      enterOuterAlt(_localctx, 7);
      setState(89);
      match(mrubysimpParser::BREAK);
      setState(90);
      var();
      break;
    }

    case 8: {
      enterOuterAlt(_localctx, 8);
      setState(91);
      match(mrubysimpParser::RAISE);
      setState(92);
      var();
      break;
    }

    case 9: {
      enterOuterAlt(_localctx, 9);
      setState(93);
      match(mrubysimpParser::YIELD);
      setState(94);
      var();
      break;
    }

    case 10: {
      enterOuterAlt(_localctx, 10);
      setState(95);
      match(mrubysimpParser::CONTINUE);
      setState(96);
      var();
      break;
    }

    case 11: {
      enterOuterAlt(_localctx, 11);
      setState(97);
      match(mrubysimpParser::NEXT);
      setState(98);
      var();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VarContext ------------------------------------------------------------------

mrubysimpParser::VarContext::VarContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* mrubysimpParser::VarContext::NAME() {
  return getToken(mrubysimpParser::NAME, 0);
}

mrubysimpParser::RefContext* mrubysimpParser::VarContext::ref() {
  return getRuleContext<mrubysimpParser::RefContext>(0);
}

tree::TerminalNode* mrubysimpParser::VarContext::LP() {
  return getToken(mrubysimpParser::LP, 0);
}

tree::TerminalNode* mrubysimpParser::VarContext::RP() {
  return getToken(mrubysimpParser::RP, 0);
}


size_t mrubysimpParser::VarContext::getRuleIndex() const {
  return mrubysimpParser::RuleVar;
}


antlrcpp::Any mrubysimpParser::VarContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitVar(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::VarContext* mrubysimpParser::var() {
  VarContext *_localctx = _tracker.createInstance<VarContext>(_ctx, getState());
  enterRule(_localctx, 6, mrubysimpParser::RuleVar);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(107);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(101);
      match(mrubysimpParser::NAME);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(102);
      ref();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(103);
      match(mrubysimpParser::LP);
      setState(104);
      ref();
      setState(105);
      match(mrubysimpParser::RP);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RefContext ------------------------------------------------------------------

mrubysimpParser::RefContext::RefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<mrubysimpParser::IdentifierContext *> mrubysimpParser::RefContext::identifier() {
  return getRuleContexts<mrubysimpParser::IdentifierContext>();
}

mrubysimpParser::IdentifierContext* mrubysimpParser::RefContext::identifier(size_t i) {
  return getRuleContext<mrubysimpParser::IdentifierContext>(i);
}

tree::TerminalNode* mrubysimpParser::RefContext::DUO_COL() {
  return getToken(mrubysimpParser::DUO_COL, 0);
}


size_t mrubysimpParser::RefContext::getRuleIndex() const {
  return mrubysimpParser::RuleRef;
}


antlrcpp::Any mrubysimpParser::RefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitRef(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::RefContext* mrubysimpParser::ref() {
  RefContext *_localctx = _tracker.createInstance<RefContext>(_ctx, getState());
  enterRule(_localctx, 8, mrubysimpParser::RuleRef);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(109);
    identifier();
    setState(110);
    match(mrubysimpParser::DUO_COL);
    setState(111);
    identifier();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArgsContext ------------------------------------------------------------------

mrubysimpParser::ArgsContext::ArgsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

mrubysimpParser::VarContext* mrubysimpParser::ArgsContext::var() {
  return getRuleContext<mrubysimpParser::VarContext>(0);
}

tree::TerminalNode* mrubysimpParser::ArgsContext::COMMA() {
  return getToken(mrubysimpParser::COMMA, 0);
}

mrubysimpParser::ArgsContext* mrubysimpParser::ArgsContext::args() {
  return getRuleContext<mrubysimpParser::ArgsContext>(0);
}


size_t mrubysimpParser::ArgsContext::getRuleIndex() const {
  return mrubysimpParser::RuleArgs;
}


antlrcpp::Any mrubysimpParser::ArgsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitArgs(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::ArgsContext* mrubysimpParser::args() {
  ArgsContext *_localctx = _tracker.createInstance<ArgsContext>(_ctx, getState());
  enterRule(_localctx, 10, mrubysimpParser::RuleArgs);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(118);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(113);
      var();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(114);
      var();
      setState(115);
      match(mrubysimpParser::COMMA);
      setState(116);
      args();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ValContext ------------------------------------------------------------------

mrubysimpParser::ValContext::ValContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* mrubysimpParser::ValContext::NAME() {
  return getToken(mrubysimpParser::NAME, 0);
}

tree::TerminalNode* mrubysimpParser::ValContext::NUM() {
  return getToken(mrubysimpParser::NUM, 0);
}

std::vector<mrubysimpParser::VarContext *> mrubysimpParser::ValContext::var() {
  return getRuleContexts<mrubysimpParser::VarContext>();
}

mrubysimpParser::VarContext* mrubysimpParser::ValContext::var(size_t i) {
  return getRuleContext<mrubysimpParser::VarContext>(i);
}

tree::TerminalNode* mrubysimpParser::ValContext::DOT2() {
  return getToken(mrubysimpParser::DOT2, 0);
}


size_t mrubysimpParser::ValContext::getRuleIndex() const {
  return mrubysimpParser::RuleVal;
}


antlrcpp::Any mrubysimpParser::ValContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitVal(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::ValContext* mrubysimpParser::val() {
  ValContext *_localctx = _tracker.createInstance<ValContext>(_ctx, getState());
  enterRule(_localctx, 12, mrubysimpParser::RuleVal);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(135);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case mrubysimpParser::T__0: {
        enterOuterAlt(_localctx, 1);
        setState(120);
        match(mrubysimpParser::T__0);
        setState(121);
        match(mrubysimpParser::NAME);
        setState(122);
        match(mrubysimpParser::T__0);
        break;
      }

      case mrubysimpParser::NUM: {
        enterOuterAlt(_localctx, 2);
        setState(123);
        match(mrubysimpParser::NUM);
        break;
      }

      case mrubysimpParser::T__1: {
        enterOuterAlt(_localctx, 3);
        setState(124);
        match(mrubysimpParser::T__1);
        break;
      }

      case mrubysimpParser::T__2: {
        enterOuterAlt(_localctx, 4);
        setState(125);
        match(mrubysimpParser::T__2);
        break;
      }

      case mrubysimpParser::T__3: {
        enterOuterAlt(_localctx, 5);
        setState(126);
        match(mrubysimpParser::T__3);
        break;
      }

      case mrubysimpParser::T__4: {
        enterOuterAlt(_localctx, 6);
        setState(127);
        match(mrubysimpParser::T__4);
        break;
      }

      case mrubysimpParser::LP:
      case mrubysimpParser::NAME: {
        enterOuterAlt(_localctx, 7);
        setState(128);
        var();
        setState(129);
        match(mrubysimpParser::DOT2);
        setState(130);
        var();
        break;
      }

      case mrubysimpParser::T__5: {
        enterOuterAlt(_localctx, 8);
        setState(132);
        match(mrubysimpParser::T__5);
        break;
      }

      case mrubysimpParser::T__6: {
        enterOuterAlt(_localctx, 9);
        setState(133);
        match(mrubysimpParser::T__6);
        break;
      }

      case mrubysimpParser::T__7: {
        enterOuterAlt(_localctx, 10);
        setState(134);
        match(mrubysimpParser::T__7);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IdentifierContext ------------------------------------------------------------------

mrubysimpParser::IdentifierContext::IdentifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* mrubysimpParser::IdentifierContext::NAME() {
  return getToken(mrubysimpParser::NAME, 0);
}


size_t mrubysimpParser::IdentifierContext::getRuleIndex() const {
  return mrubysimpParser::RuleIdentifier;
}


antlrcpp::Any mrubysimpParser::IdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<mrubysimpVisitor*>(visitor))
    return parserVisitor->visitIdentifier(this);
  else
    return visitor->visitChildren(this);
}

mrubysimpParser::IdentifierContext* mrubysimpParser::identifier() {
  IdentifierContext *_localctx = _tracker.createInstance<IdentifierContext>(_ctx, getState());
  enterRule(_localctx, 14, mrubysimpParser::RuleIdentifier);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(137);
    match(mrubysimpParser::NAME);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> mrubysimpParser::_decisionToDFA;
atn::PredictionContextCache mrubysimpParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN mrubysimpParser::_atn;
std::vector<uint16_t> mrubysimpParser::_serializedATN;

std::vector<std::string> mrubysimpParser::_ruleNames = {
  "start", "program", "statement", "var", "ref", "args", "val", "identifier"
};

std::vector<std::string> mrubysimpParser::_literalNames = {
  "", "'\"'", "'nil'", "'true'", "'false'", "'/foo/'", "'[]'", "'[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,nil]'", 
  "'{}'", "'a=0\nb=\"asdfasdfasdf adaf asdf asdfa sdf asdfasdfasdfa sdf\"\nc={1=>1, 2=>\"foo\", \"foo\"=>nil, nil=> nil }\nd=[1,nil,\" sdfg\"]\nsrand(1337)\n'", 
  "'\n'", "'::'", "'.'", "'def'", "'('", "')'", "'='", "'{'", "'}'", "'|'", 
  "'return'", "'raise'", "'yield'", "'continue'", "'break'", "'next'", "','", 
  "'..'"
};

std::vector<std::string> mrubysimpParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "START_HEAD", "NEWLINE", "DUO_COL", 
  "DOT", "DEF", "LP", "RP", "EQ", "LCB", "RCB", "MID", "RETURN", "RAISE", 
  "YIELD", "CONTINUE", "BREAK", "NEXT", "COMMA", "DOT2", "NUM", "NAME", 
  "WS", "ErrorChar"
};

dfa::Vocabulary mrubysimpParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> mrubysimpParser::_tokenNames;

mrubysimpParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x21, 0x8e, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x4, 
    0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 
    0x2, 0x3, 0x3, 0x6, 0x3, 0x18, 0xa, 0x3, 0xd, 0x3, 0xe, 0x3, 0x19, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x22, 
    0xa, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x26, 0xa, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x2f, 
    0xa, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x35, 0xa, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x39, 0xa, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x5, 0x4, 0x44, 0xa, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 
    0x4, 0x4a, 0xa, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x4e, 0xa, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x5, 0x4, 0x66, 0xa, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 
    0x3, 0x5, 0x3, 0x5, 0x5, 0x5, 0x6e, 0xa, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x5, 
    0x7, 0x79, 0xa, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 
    0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 
    0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x8a, 0xa, 0x8, 0x3, 0x9, 0x3, 
    0x9, 0x3, 0x9, 0x2, 0x2, 0xa, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 
    0x2, 0x2, 0x2, 0xa4, 0x2, 0x12, 0x3, 0x2, 0x2, 0x2, 0x4, 0x17, 0x3, 
    0x2, 0x2, 0x2, 0x6, 0x65, 0x3, 0x2, 0x2, 0x2, 0x8, 0x6d, 0x3, 0x2, 0x2, 
    0x2, 0xa, 0x6f, 0x3, 0x2, 0x2, 0x2, 0xc, 0x78, 0x3, 0x2, 0x2, 0x2, 0xe, 
    0x89, 0x3, 0x2, 0x2, 0x2, 0x10, 0x8b, 0x3, 0x2, 0x2, 0x2, 0x12, 0x13, 
    0x7, 0xb, 0x2, 0x2, 0x13, 0x14, 0x5, 0x4, 0x3, 0x2, 0x14, 0x15, 0x7, 
    0x2, 0x2, 0x3, 0x15, 0x3, 0x3, 0x2, 0x2, 0x2, 0x16, 0x18, 0x5, 0x6, 
    0x4, 0x2, 0x17, 0x16, 0x3, 0x2, 0x2, 0x2, 0x18, 0x19, 0x3, 0x2, 0x2, 
    0x2, 0x19, 0x17, 0x3, 0x2, 0x2, 0x2, 0x19, 0x1a, 0x3, 0x2, 0x2, 0x2, 
    0x1a, 0x5, 0x3, 0x2, 0x2, 0x2, 0x1b, 0x1c, 0x7, 0xf, 0x2, 0x2, 0x1c, 
    0x1d, 0x5, 0x8, 0x5, 0x2, 0x1d, 0x1e, 0x7, 0xe, 0x2, 0x2, 0x1e, 0x1f, 
    0x5, 0x10, 0x9, 0x2, 0x1f, 0x21, 0x7, 0x10, 0x2, 0x2, 0x20, 0x22, 0x5, 
    0xc, 0x7, 0x2, 0x21, 0x20, 0x3, 0x2, 0x2, 0x2, 0x21, 0x22, 0x3, 0x2, 
    0x2, 0x2, 0x22, 0x23, 0x3, 0x2, 0x2, 0x2, 0x23, 0x25, 0x7, 0x11, 0x2, 
    0x2, 0x24, 0x26, 0x5, 0x4, 0x3, 0x2, 0x25, 0x24, 0x3, 0x2, 0x2, 0x2, 
    0x25, 0x26, 0x3, 0x2, 0x2, 0x2, 0x26, 0x66, 0x3, 0x2, 0x2, 0x2, 0x27, 
    0x28, 0x5, 0x8, 0x5, 0x2, 0x28, 0x29, 0x7, 0x12, 0x2, 0x2, 0x29, 0x2a, 
    0x5, 0x8, 0x5, 0x2, 0x2a, 0x2b, 0x7, 0xe, 0x2, 0x2, 0x2b, 0x2c, 0x5, 
    0x10, 0x9, 0x2, 0x2c, 0x2e, 0x7, 0x10, 0x2, 0x2, 0x2d, 0x2f, 0x5, 0xc, 
    0x7, 0x2, 0x2e, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x2f, 0x3, 0x2, 0x2, 
    0x2, 0x2f, 0x30, 0x3, 0x2, 0x2, 0x2, 0x30, 0x31, 0x7, 0x11, 0x2, 0x2, 
    0x31, 0x32, 0x7, 0x13, 0x2, 0x2, 0x32, 0x34, 0x7, 0x15, 0x2, 0x2, 0x33, 
    0x35, 0x5, 0xc, 0x7, 0x2, 0x34, 0x33, 0x3, 0x2, 0x2, 0x2, 0x34, 0x35, 
    0x3, 0x2, 0x2, 0x2, 0x35, 0x36, 0x3, 0x2, 0x2, 0x2, 0x36, 0x38, 0x7, 
    0x15, 0x2, 0x2, 0x37, 0x39, 0x5, 0x4, 0x3, 0x2, 0x38, 0x37, 0x3, 0x2, 
    0x2, 0x2, 0x38, 0x39, 0x3, 0x2, 0x2, 0x2, 0x39, 0x3a, 0x3, 0x2, 0x2, 
    0x2, 0x3a, 0x3b, 0x7, 0x14, 0x2, 0x2, 0x3b, 0x66, 0x3, 0x2, 0x2, 0x2, 
    0x3c, 0x3d, 0x5, 0x8, 0x5, 0x2, 0x3d, 0x3e, 0x7, 0x12, 0x2, 0x2, 0x3e, 
    0x3f, 0x5, 0x10, 0x9, 0x2, 0x3f, 0x40, 0x7, 0xe, 0x2, 0x2, 0x40, 0x41, 
    0x5, 0x10, 0x9, 0x2, 0x41, 0x43, 0x7, 0x10, 0x2, 0x2, 0x42, 0x44, 0x5, 
    0xc, 0x7, 0x2, 0x43, 0x42, 0x3, 0x2, 0x2, 0x2, 0x43, 0x44, 0x3, 0x2, 
    0x2, 0x2, 0x44, 0x45, 0x3, 0x2, 0x2, 0x2, 0x45, 0x46, 0x7, 0x11, 0x2, 
    0x2, 0x46, 0x47, 0x7, 0x13, 0x2, 0x2, 0x47, 0x49, 0x7, 0x15, 0x2, 0x2, 
    0x48, 0x4a, 0x5, 0xc, 0x7, 0x2, 0x49, 0x48, 0x3, 0x2, 0x2, 0x2, 0x49, 
    0x4a, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x4b, 0x3, 0x2, 0x2, 0x2, 0x4b, 0x4d, 
    0x7, 0x15, 0x2, 0x2, 0x4c, 0x4e, 0x5, 0x4, 0x3, 0x2, 0x4d, 0x4c, 0x3, 
    0x2, 0x2, 0x2, 0x4d, 0x4e, 0x3, 0x2, 0x2, 0x2, 0x4e, 0x4f, 0x3, 0x2, 
    0x2, 0x2, 0x4f, 0x50, 0x7, 0x14, 0x2, 0x2, 0x50, 0x66, 0x3, 0x2, 0x2, 
    0x2, 0x51, 0x52, 0x5, 0x8, 0x5, 0x2, 0x52, 0x53, 0x7, 0x12, 0x2, 0x2, 
    0x53, 0x54, 0x5, 0x8, 0x5, 0x2, 0x54, 0x66, 0x3, 0x2, 0x2, 0x2, 0x55, 
    0x56, 0x5, 0x8, 0x5, 0x2, 0x56, 0x57, 0x7, 0x12, 0x2, 0x2, 0x57, 0x58, 
    0x5, 0xe, 0x8, 0x2, 0x58, 0x66, 0x3, 0x2, 0x2, 0x2, 0x59, 0x5a, 0x7, 
    0x16, 0x2, 0x2, 0x5a, 0x66, 0x5, 0x8, 0x5, 0x2, 0x5b, 0x5c, 0x7, 0x1a, 
    0x2, 0x2, 0x5c, 0x66, 0x5, 0x8, 0x5, 0x2, 0x5d, 0x5e, 0x7, 0x17, 0x2, 
    0x2, 0x5e, 0x66, 0x5, 0x8, 0x5, 0x2, 0x5f, 0x60, 0x7, 0x18, 0x2, 0x2, 
    0x60, 0x66, 0x5, 0x8, 0x5, 0x2, 0x61, 0x62, 0x7, 0x19, 0x2, 0x2, 0x62, 
    0x66, 0x5, 0x8, 0x5, 0x2, 0x63, 0x64, 0x7, 0x1b, 0x2, 0x2, 0x64, 0x66, 
    0x5, 0x8, 0x5, 0x2, 0x65, 0x1b, 0x3, 0x2, 0x2, 0x2, 0x65, 0x27, 0x3, 
    0x2, 0x2, 0x2, 0x65, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x65, 0x51, 0x3, 0x2, 
    0x2, 0x2, 0x65, 0x55, 0x3, 0x2, 0x2, 0x2, 0x65, 0x59, 0x3, 0x2, 0x2, 
    0x2, 0x65, 0x5b, 0x3, 0x2, 0x2, 0x2, 0x65, 0x5d, 0x3, 0x2, 0x2, 0x2, 
    0x65, 0x5f, 0x3, 0x2, 0x2, 0x2, 0x65, 0x61, 0x3, 0x2, 0x2, 0x2, 0x65, 
    0x63, 0x3, 0x2, 0x2, 0x2, 0x66, 0x7, 0x3, 0x2, 0x2, 0x2, 0x67, 0x6e, 
    0x7, 0x1f, 0x2, 0x2, 0x68, 0x6e, 0x5, 0xa, 0x6, 0x2, 0x69, 0x6a, 0x7, 
    0x10, 0x2, 0x2, 0x6a, 0x6b, 0x5, 0xa, 0x6, 0x2, 0x6b, 0x6c, 0x7, 0x11, 
    0x2, 0x2, 0x6c, 0x6e, 0x3, 0x2, 0x2, 0x2, 0x6d, 0x67, 0x3, 0x2, 0x2, 
    0x2, 0x6d, 0x68, 0x3, 0x2, 0x2, 0x2, 0x6d, 0x69, 0x3, 0x2, 0x2, 0x2, 
    0x6e, 0x9, 0x3, 0x2, 0x2, 0x2, 0x6f, 0x70, 0x5, 0x10, 0x9, 0x2, 0x70, 
    0x71, 0x7, 0xd, 0x2, 0x2, 0x71, 0x72, 0x5, 0x10, 0x9, 0x2, 0x72, 0xb, 
    0x3, 0x2, 0x2, 0x2, 0x73, 0x79, 0x5, 0x8, 0x5, 0x2, 0x74, 0x75, 0x5, 
    0x8, 0x5, 0x2, 0x75, 0x76, 0x7, 0x1c, 0x2, 0x2, 0x76, 0x77, 0x5, 0xc, 
    0x7, 0x2, 0x77, 0x79, 0x3, 0x2, 0x2, 0x2, 0x78, 0x73, 0x3, 0x2, 0x2, 
    0x2, 0x78, 0x74, 0x3, 0x2, 0x2, 0x2, 0x79, 0xd, 0x3, 0x2, 0x2, 0x2, 
    0x7a, 0x7b, 0x7, 0x3, 0x2, 0x2, 0x7b, 0x7c, 0x7, 0x1f, 0x2, 0x2, 0x7c, 
    0x8a, 0x7, 0x3, 0x2, 0x2, 0x7d, 0x8a, 0x7, 0x1e, 0x2, 0x2, 0x7e, 0x8a, 
    0x7, 0x4, 0x2, 0x2, 0x7f, 0x8a, 0x7, 0x5, 0x2, 0x2, 0x80, 0x8a, 0x7, 
    0x6, 0x2, 0x2, 0x81, 0x8a, 0x7, 0x7, 0x2, 0x2, 0x82, 0x83, 0x5, 0x8, 
    0x5, 0x2, 0x83, 0x84, 0x7, 0x1d, 0x2, 0x2, 0x84, 0x85, 0x5, 0x8, 0x5, 
    0x2, 0x85, 0x8a, 0x3, 0x2, 0x2, 0x2, 0x86, 0x8a, 0x7, 0x8, 0x2, 0x2, 
    0x87, 0x8a, 0x7, 0x9, 0x2, 0x2, 0x88, 0x8a, 0x7, 0xa, 0x2, 0x2, 0x89, 
    0x7a, 0x3, 0x2, 0x2, 0x2, 0x89, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x89, 0x7e, 
    0x3, 0x2, 0x2, 0x2, 0x89, 0x7f, 0x3, 0x2, 0x2, 0x2, 0x89, 0x80, 0x3, 
    0x2, 0x2, 0x2, 0x89, 0x81, 0x3, 0x2, 0x2, 0x2, 0x89, 0x82, 0x3, 0x2, 
    0x2, 0x2, 0x89, 0x86, 0x3, 0x2, 0x2, 0x2, 0x89, 0x87, 0x3, 0x2, 0x2, 
    0x2, 0x89, 0x88, 0x3, 0x2, 0x2, 0x2, 0x8a, 0xf, 0x3, 0x2, 0x2, 0x2, 
    0x8b, 0x8c, 0x7, 0x1f, 0x2, 0x2, 0x8c, 0x11, 0x3, 0x2, 0x2, 0x2, 0xf, 
    0x19, 0x21, 0x25, 0x2e, 0x34, 0x38, 0x43, 0x49, 0x4d, 0x65, 0x6d, 0x78, 
    0x89, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

mrubysimpParser::Initializer mrubysimpParser::_init;
