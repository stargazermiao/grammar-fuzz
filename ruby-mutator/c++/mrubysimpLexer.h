
// Generated from mrubysimp.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"




class  mrubysimpLexer : public antlr4::Lexer {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, START_HEAD = 9, NEWLINE = 10, DUO_COL = 11, DOT = 12, DEF = 13, 
    LP = 14, RP = 15, EQ = 16, LCB = 17, RCB = 18, MID = 19, RETURN = 20, 
    RAISE = 21, YIELD = 22, CONTINUE = 23, BREAK = 24, NEXT = 25, COMMA = 26, 
    DOT2 = 27, NUM = 28, NAME = 29, WS = 30, ErrorChar = 31
  };

  mrubysimpLexer(antlr4::CharStream *input);
  ~mrubysimpLexer();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

