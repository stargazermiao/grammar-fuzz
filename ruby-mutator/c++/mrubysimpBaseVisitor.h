
// Generated from mrubysimp.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "mrubysimpVisitor.h"


/**
 * This class provides an empty implementation of mrubysimpVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  mrubysimpBaseVisitor : public mrubysimpVisitor {
public:

  virtual antlrcpp::Any visitStart(mrubysimpParser::StartContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProgram(mrubysimpParser::ProgramContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStatement(mrubysimpParser::StatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVar(mrubysimpParser::VarContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRef(mrubysimpParser::RefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitArgs(mrubysimpParser::ArgsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVal(mrubysimpParser::ValContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIdentifier(mrubysimpParser::IdentifierContext *ctx) override {
    return visitChildren(ctx);
  }


};

