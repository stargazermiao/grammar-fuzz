;; a random mutator will randomly select a node from AST and mutate it into
;; some  AST node with same type

;; Yihao Sun
;; 2020 Syracuse

#lang racket

(require parser-tools/lex)

(require "ts-parser.rkt"
         "tiny-scheme.rkt")

(provide random-mutate-tiny-scheme)

(define seed-prog
  "(begin (define foo (lambda (x) x)) (foo (lambda (y) 1)))")

;; parse a input program into  AST with label
(define (parse parser lexer input)
  (init-counter)
  (define (next-nonempty)
    (define next (lexer input))
    (cond
      [(equal? (position-token-token next) 'EMPTY)
       (next-nonempty)]
      [else next]))
  (parser (λ ()
            (define next (next-nonempty))
            ;;(displayln ">>>>>>>>>>>>>>>>>>>>>>>>")
            ;;(displayln next)
            next)))

;; find the type and position of a node inside a AST with given ID
(define (find-node/id ast target-id)
  ;;(displayln ">>>>>>>>>>>>>>>")
  ;;(displayln ast)
  (match ast
    [`(,type ,id ,pos ,children ...)
     (cond
       [(equal? id target-id) (list type pos)]
       [else
        (foldl (λ (c res)
                 ;; (displayln ">>>>>>>>>>>>>")
                 ;; (displayln c)
                 (or res (find-node/id c target-id)))
               #f
               children)])]
    [else
     #f]))


(define (random-mutate-tiny-scheme input-str)
  (let/ec return
    (begin
      ;;(init-counter)
      (define ast
        (with-handlers ([exn:fail?
                         (λ (e)
                           #;(parse tiny-scheme-parser tiny-scheme-lexer (open-input-string seed-prog))
                           (return seed-prog))])
          (parse tiny-scheme-parser tiny-scheme-lexer (open-input-string input-str))))
      (define depth (random 10))
      (define target-id (add1 (random (current-counter))))
      ;; (displayln (format "target id is ~a" target-id))
      (define-values (target-type target-pos)
        (match (find-node/id ast target-id)
          [`(,t ,p) (values t p)]))
      ;;(define before-time (current-milliseconds))
      (define new-node-str (gen-tiny-scheme target-type depth))
      ;;(define after-time (current-milliseconds))
      ;;(displayln (format "generate code cost ~a ms" (- after-time before-time)))
      ;; (displayln (format "target node position at ~a" target-pos))
      ;; (displayln (format "original program is divided into two part ~a \n ~a"
      ;;                    (substring input-str 0 (sub1 (position-offset (first target-pos))))
      ;;                    (substring input-str (sub1 (position-offset (second target-pos))))))
      ;; (displayln (format "generated node is:"))
      ;; (displayln new-node-str)
      ;; ;; (displayln "<<<<<<<<<<<<<<<<<<<<<<<<<<")
      (define res
        (string-append
         (substring input-str 0 (sub1 (position-offset (first target-pos))))
         new-node-str
         (substring input-str (sub1 (position-offset (second target-pos)))))))
    (parse tiny-scheme-parser tiny-scheme-lexer (open-input-string res))
    res))


;;;;;;;;;;; test
;; (define (loop prog)
;;   (define next (random-mutate-tiny-scheme prog))
;;   (print next)
;;   (displayln " ")
;;   (loop next))
